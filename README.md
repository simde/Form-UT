# Form'UT
**Form-UT est un service souhaitant satisfaire les besoins en questionnaires (en ligne) de la communauté UTCéenne.**

Retrouver la documentation complète (en anglais) [ici](./Documentation/toc.md).

## Fonctionnalités
- Application placée derrière l'authentification offerte par le CAS : garantie *théorique* **d'une réponse = une personne physique**.


- Création **simple** de formulaire via une interface graphique.
- Possibilité de mettre les mains dans le cambouis et de *coder* les formulaires directement (aussi).
- Possibilité de partager la source d'un formulaire que l'on a créé.
- Validation serveur des formulaires créés, pour s'assurer d'un minimum de cohérence.


- Restriction d'accès à des groupes d'utilisateurs⋅rices (soit ceux du CAS soit ceux créés par les utilisateurs⋅rices eux⋅elles-même). Cela concerne distinctement :
 - La possibilité de répondre à un formulaire,
 - La possibilité d'accéder aux réponses d'un formulaire (cet accès peut être partagé - par le⋅a créateur⋅rice - avec d'autres).


- Le contenu des réponses peut être **techniquement** anonyme selon la volonté de la personne ayant créé le formulaire (plus d'informations sur l'anonymat [ici](./Documentation/doc_files/architecture.md)).
- Validation client **et** validation serveur des réponses formulées.
- Possibilité d'éditer ses réponses après la *confirmation*, même pour les réponses transmises de manière anonyme (**et ce en conservant l'anonymat**).


- Le tout dans une interface pas trop sale.
