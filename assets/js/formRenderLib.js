/**
 * Js lib for easy form rendering
 */
(function(window) {
	// You can enable the strict mode commenting the following line
	'use strict';

	function myLibrary() {
		var _myLibraryObject = {};

		var self;

		var dataToRender = [];

		_myLibraryObject.setDataToRender = function(data) {
			dataToRender = data;
		};

		/**
		 * Store the final html output
		 * @type {String}
		 */
		var finalHtmlOutput = '';

		/**
		 * Stores the current html (used when building the form)
		 * @type {String}
		 */
		var currentHtmlOutput = '';

		/**
		 * Id of the div where rendering is needed
		 * THIS IS IMPORTANT FOR NOT GETTING MESSY WHEN THERE ARE MULTIPLE rendering on a page
		 * @type {String}
		 */
		var renderingDiv = "preview";

		/**
		 * Holds the number of page for all forms based on their div id
		 * @type {Array}
		 */
		var nbOfPages = [];
		nbOfPages[renderingDiv] = -1;

		/**
		 * Holds the current page number for all forms based on their div id
		 * @type {Array}
		 */
		var currentPage = [];
		currentPage[renderingDiv] = 1

		/**
		 * Array to hold the elements that will be needed for parsing in saving form
		 * @type {Array}
		 */
		var elementsToParse = [];

		_myLibraryObject.getElementsToParse = function() {
			return elementsToParse[renderingDiv];
		}

		/**
		 * For escaping html
		 * @type {Object}
		 */
		var entityMap = {
			'&': '&amp;',
			'<': '&lt;',
			'>': '&gt;',
			'"': '&quot;',
			"'": '&#39;',
			'/': '&#x2F;',
			'`': '&#x60;',
			'=': '&#x3D;'
		};

		_myLibraryObject.escapeHtml = function(string) {
			return String(string).replace(/[&<>"'`=\/]/g, function(s) {
				return entityMap[s];
			});
		};


		/**
		 * Function simple html decoding
		 * @param  {string} input
		 * @return {string}
		 */
		_myLibraryObject.decodeHtml = function(input) {
			input = input.replace(/<br>/g, '\n');
			var parser = new DOMParser;
			var dom = parser.parseFromString(
				'<!doctype html><body>' + input,
				'text/html');
			var decodedString = dom.body.textContent;
			return decodedString;
		}

		_myLibraryObject.alphaNumericCleaner = function(string) {
			return string.replace(/[^0-9a-zA-Z-]/gi, ''); // it keeps -
		};

		_myLibraryObject.numericCleaner = function(string) {
			return string.replace(/[^0-9]/gi, ''); // it keeps -
		};

		_myLibraryObject.wrapIt = function(string, object) {
			return object.open + "\n" + string + "\n" + object.close;
		}

		/**
		 * Function for adding a page in the rendered form
		 * @return {[type]} [description]
		 */
		_myLibraryObject.addPage = function() {
			finalHtmlOutput += self.wrapIt(currentHtmlOutput, formSectionHtml);
			currentHtmlOutput = '';
			nbOfPages[renderingDiv]++;
			currentPage[renderingDiv]++;
		};


		/**
		 * Function for adding a ready element to the form
		 */
		_myLibraryObject.saveRenderedElement = function(element) {
			currentHtmlOutput += "\n \n \n" + element;
			return;
		};



		/**
		 * function for checking a placeholders
		 * @param  {object} data fomrElement
		 * @return {string}
		 */
		_myLibraryObject.checkPlaceholder = function(data) {
			if (data.hasOwnProperty("placeholder")) {
				return data.placeholder;
			} else {
				return '';
			}
		};



		/**
		 * Simple checks before rendering
		 * Each fields need a type and correct characters
		 * @return {bool}
		 */
		_myLibraryObject.checkTypes = function() {
			var expr = /[^0-9a-zA-Z-]/;
			for (var i = 0; i < dataToRender.length; i++) {
				if (dataToRender[i].hasOwnProperty("type") === false) return false;
				if (expr.test(dataToRender[i].type) === true) return false;
			}
			return true;
		}




		/**
		 * Function for adding the maxlength constraint
		 * @param  {object} data formelement
		 * @return {object|string}
		 */
		_myLibraryObject.buildMaxLength = function(data) {
			if (!data.hasOwnProperty("maxlength")) return {
				'string': ''
			};
			var maxLength = parseInt(data.maxlength);
			if (maxLength > 0) {
				var string = 'maxlength="' + maxLength + '" data-parsley-maxlength="' + maxLength + '" ';
				return {
					'string': string,
					value: maxLength
				};
			}
			return {
				'string': ''
			};
		}



		/**
		 * Function for adding the max min constraint on number field
		 * @param  {object} data formelement
		 * @return {string}
		 */
		_myLibraryObject.buildMiniMaxi = function(data) {
			var output = '';
			if (data.hasOwnProperty("min")) {
				var min = parseInt(data.min);
				output += ' min="' + min + '" data-parsley-min="' + min + '" ';
			};

			if (data.hasOwnProperty("max")) {
				var max = parseInt(data.max);
				output += ' max="' + max + '" data-parsley-max="' + max + '" ';
			}
			return output;
		}



		/**
		 * Function for adding a parsley pattern verification
		 * @param  {string} type type of field
		 * @return {string}
		 */
		_myLibraryObject.buildPattern = function(type) {
			var pattern;
			if (type === "date") {
				pattern = "^(?:(?:31(\\\/|-|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\\/|-|\\.)(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\\/|-|\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\\/|-|\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$";
			}
			return ' pattern="' + pattern + '" data-parsley-pattern="' + pattern + '" ';

		}


		/**
		 * function for rendering fields label
		 * @param  {string} forID    id of the field
		 * @param  {string} label    label of the field
		 * @param  {bool} required    is the field required
		 * @return {string}          html label
		 */
		_myLibraryObject.renderLabel = function(forID, label, required) {
			if (typeof(required) === 'undefined') required = false;
			label = label.replace('<br>', '');
			label = self.escapeHtml(label);
			var tmp1 = 1;
			var label = {
				'open': '<label for="' + forID + '" class="control-label">',
				'close': label + '</label>'
			};

			if (required === false) return label.open + label.close;
			return self.wrapIt(requiredHtml, label);
		}



		/**
		 * Function for rendering the input html element
		 * It is not working for all form elements !!
		 * @param  {string} input       What kind of input
		 * @param  {string} type        What type of input
		 * @param  {string} name        name & id of the field
		 * @param  {string} placeholder
		 * @param  {string} classes     classes to add to the element
		 * @param  {string} extras      extra stuff to put in the header
		 * @param  {bool} required
		 * @return {string}
		 */
		_myLibraryObject.inputRender = function(input, type, name, placeholder, classes, extras, required) {
			placeholder = self.escapeHtml(placeholder);
			if (type !== '') type = ' type="' + type + '" ';
			if (required === true) extras += requiredText;
			return '<' + input + type + ' id="' + name + '" name="' + name + '" placeholder="' + placeholder + '" class="' + classes + '" ' + extras + ' >';
		}

		var formDivId = "OnArienMis";
		var progressbarDivId = "progressbar";

		/**
		 * Function for setting the rendering div id
		 * @param  {string} divId
		 */
		_myLibraryObject.setRenderingDiv = function(divId) {
			renderingDiv = divId;
			formDivId = 'form-' + divId;
			progressbarDivId = 'progressbar-' + divId;

			mainFormHtml = {
				'open': '<div id="' + progressbarDivId + '"></div> <form id="' + formDivId + '"> ',
				'close': '</form>'
			};
		}


		/////////////////////////////////
		/////////////////////////////////
		/////////////////////////////////
		///////////////////////////////// Sets of prepared elements for wrapping
		var mainFormHtml = {
			'open': '<div id="progressbar"></div> <form id="OnArienMis"> ',
			'close': '</form>'
		};
		var formElementHtml = {
			'open': '<div class="formElement">',
			'close': '</div> '
		};
		var textElementHtml = {
			'open': '<div class="form-group form-group-lg Markdown">',
			'close': '</div>'
		};
		var requiredHtml = '<span style="color:red">* </span>';

		var requiredText = ' required data-parsley-required ';

		var parsleyTrigger = ' data-parsley-trigger="keyup" ';

		var formSectionHtml = {
			'open': '<div class="form-section">',
			'close': '</div>'
		}
		/////////////////////////
		/////////////////////////
		/////////////////////////


		/**
		 * Array for storing the page index of each form rendered in the page
		 * Structure : pagesIndex[renderingDiv][page]{formElement}
		 * @type {Array}
		 */
		var pagesIndex = [];
		_myLibraryObject.getPagesIndex = function() {
			return pagesIndex[renderingDiv];
		}


		/**
		 * Function for adding to page index
		 * @param  {string} id   id of the field/subfield
		 * @param  {string} type stype of field
		 */
		_myLibraryObject.addIdToPagesIndex = function(id, type) {
			if (typeof(pagesIndex[renderingDiv][currentPage]) == 'undefined') pagesIndex[renderingDiv][currentPage[renderingDiv]] = [];
			pagesIndex[renderingDiv][currentPage[renderingDiv]].push({
				'id': id,
				'type': type
			}); // we store the type so that we can easily reset fields on a sepectific page
		}

		/**
		 * Generating the correct parsleyGroup for page validation
		 * @return {string}
		 */
		_myLibraryObject.parsleyGroup = function() {
			return ' data-parsley-group="block-' + currentPage[renderingDiv] + '" ';
		}

		///////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////

		/**
		 * Main library function for generating a form
		 * @param  {bool} validate do we activate validation (and reset)
		 * @return {array}         The elemens that will parsing for populate/reset/save
		 */
		_myLibraryObject.renderNow = function(validate) {
			self = this;
			finalHtmlOutput = '';
			currentHtmlOutput = '';
			nbOfPages[renderingDiv] = 1;
			currentPage[renderingDiv] = 1;
			elementsToParse[renderingDiv] = [];
			pagesIndex[renderingDiv] = [];

			if (self.checkTypes() === false) return;

			for (var i = 0; i < dataToRender.length; i++) {
				self.renderSwticher(dataToRender[i]);
			}

			self.addPage();
			finalHtmlOutput += self.insertFormInteractions(validate);
			finalHtmlOutput = self.wrapIt(finalHtmlOutput, mainFormHtml);
			$(document.getElementById(renderingDiv)).html(finalHtmlOutput);
			self.activateFormsInteractions(validate);
			self.activateSelectUi();
			if (validate !== false){
				populateFormLib.setPagesIndex(pagesIndex[renderingDiv]);
			}

			return elementsToParse[renderingDiv];
		};


		/**
		 * Same function for quick render
		 * TODO check if it is usefull
		 * @return {array}         The elemens that will parsing for populate/reset/save
		 */
		_myLibraryObject.quickRender = function() {
			self = this;
			finalHtmlOutput = '';
			currentHtmlOutput = '';
			nbOfPages[renderingDiv] = 0;
			currentPage[renderingDiv] = 1;
			elementsToParse[renderingDiv] = [];
			pagesIndex[renderingDiv] = [];

			if (self.checkTypes() === false) return;

			for (var i = 0; i < dataToRender.length; i++) {
				self.renderSwticher(dataToRender[i]);
			}

			self.addPage();
			finalHtmlOutput = self.wrapIt(finalHtmlOutput, mainFormHtml);
			$(document.getElementById(renderingDiv)).html(finalHtmlOutput);
			self.activateSelectUi();
			return elementsToParse[renderingDiv];
		};




		/**
		 * Switch for rendering the form elements depending on their type
		 * @param  {object} data formelement
		 */
		_myLibraryObject.renderSwticher = function(data) {
			var required = false;

			if (data.hasOwnProperty("required") && data.required === true) required = true;

			data.name = self.alphaNumericCleaner(data.name);

			//We clean the options values to make sure their are no mis Understanding
			if (data.hasOwnProperty('values')) {
				for (var i = 0; i < data.values.length; i++) {
					data.values[i].value.replace('|', '/');
				}
			}

			if (data.type === "text") {
				return self.saveRenderedElement(self.renderText(data, required));
			} else if (data.type === "textarea") {
				return self.saveRenderedElement(self.renderTextArea(data, required));
			} else if (data.type === "date") {
				return self.saveRenderedElement(self.renderDateField(data, required));
			} else if (data.type === "number") {
				return self.saveRenderedElement(self.renderNumberField(data, required));
			} else if (data.type === "likertScale") {
				return self.saveRenderedElement(self.renderLikertScale(data, required));
			} else if (data.type === "newPageDelimiter") {
				return self.addPage();
			} else if (data.type === "select") {
				return self.saveRenderedElement(self.renderSelect(data, required));
			} else if (data.type === "adminSelect") {
				return self.saveRenderedElement(self.renderAdminSelect(data, required));
			} else if (data.type === "paragraph") {
				return self.saveRenderedElement(self.renderParagraph(data, required));
			} else if (data.type === "checkbox-group") {
				return self.saveRenderedElement(self.renderCheckboxGroup(data, required));
			} else if (data.type === "radio-group") {
				return self.saveRenderedElement(self.renderRadioGroup(data, required));
			} else return;
		};



//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
// the functions untils the next block a very similar to each others
// They aim at create the correct html
//
// All generated inputs are added to the page index and elementsToParse



		_myLibraryObject.renderText = function(data, required) {
			if (typeof(required) === 'undefined') required = false;

			if (data.hasOwnProperty("name") === false) return;
			var label = self.renderLabel(data.name, data.label, required);
			var placeholder = self.checkPlaceholder(data);

			var classes = 'form-control ';
			var maxLength = self.buildMaxLength(data)
			var extras = parsleyTrigger + maxLength.string + self.parsleyGroup();

			if (maxLength.string !== '') {
				label += '<div id="' + data.name + '-counter" class="charCounter">0/' + maxLength.value + '</div>';
				extras += ' onkeyup="formRenderLib.countChars(this.id)" ';
			}


			var input = self.inputRender('input', data.type, data.name, placeholder, classes, extras, required);

			elementsToParse[renderingDiv].push({
				'type': data.type,
				'id': data.name
			});
			self.addIdToPagesIndex(data.name, data.type);
			return self.wrapIt(label + "\n" + input, formElementHtml);
		}


		_myLibraryObject.renderTextArea = function(data, required) {
			if (typeof(required) === 'undefined') required = false;

			if (data.hasOwnProperty("name") === false) return;
			var label = self.renderLabel(data.name, data.label, required);
			var placeholder = self.checkPlaceholder(data);

			var classes = 'form-control ';
			var maxLength = self.buildMaxLength(data);
			var extras = parsleyTrigger + maxLength.string + self.parsleyGroup();

			if (maxLength.string !== '') {
				label += '<div id="' + data.name + '-counter" class="charCounter">0/' + maxLength.value + '</div>';
				extras += ' onkeyup="formRenderLib.countChars(this.id)" ';
			}

			if (data.hasOwnProperty("rows") && data.rows != "") {
				extras += ' rows="' + parseInt(data.rows) + '" ';
			}

			var input = self.inputRender('textarea', '', data.name, placeholder, classes, extras, required) + '</textarea>';

			elementsToParse[renderingDiv].push({
				'type': data.type,
				'id': data.name
			});

			self.addIdToPagesIndex(data.name, data.type);


			return self.wrapIt(label + "\n" + input, formElementHtml);
		}



		_myLibraryObject.renderDateField = function(data, required) {
			if (typeof(required) === 'undefined') required = false;

			if (data.hasOwnProperty("name") === false) return;
			var label = self.renderLabel(data.name, data.label, required);
			var placeholder = self.checkPlaceholder(data);


			var classes = 'form-control datepicker ';

			var extras = ' data-parsley-trigger="change" style="max-width : 100px;" ' + self.buildPattern("date") + ' onclick="$(' + "'#" + data.name + "' ).datepicker({ dateFormat: 'dd/mm/yy' });$(" + "'#" + data.name + "' ).datepicker('show');" + '"' + self.parsleyGroup();

			var input = self.inputRender('input', 'text', data.name, placeholder, classes, extras, required);

			elementsToParse[renderingDiv].push({
				'type': data.type,
				'id': data.name
			});
			self.addIdToPagesIndex(data.name, data.type);

			var infos = '<details class="remark remark-date"><summary><i class="fa fa-hand-o-right" aria-hidden="true"></i> Remarque</summary><p class="remark remark-date">Seules les dates ayant le format "jj/mm/aaaa" seront acceptées.</p></details>';

			return self.wrapIt(label + "\n" + input + infos, formElementHtml);
		}


		_myLibraryObject.renderNumberField = function(data, required) {
			if (typeof(required) === 'undefined') required = false;

			if (data.hasOwnProperty("name") === false) return;
			var label = self.renderLabel(data.name, data.label, required);
			var placeholder = self.checkPlaceholder(data);

			var classes = 'form-control  numberPicker';

			var extras = parsleyTrigger + self.buildMiniMaxi(data) + ' data-parsley-type="number" ' + self.parsleyGroup();


			var input = self.inputRender('input', 'text', data.name, placeholder, classes, extras, required);

			elementsToParse[renderingDiv].push({
				'type': data.type,
				'id': data.name
			});
			self.addIdToPagesIndex(data.name, data.type);

			return self.wrapIt(label + "\n" + input, formElementHtml);
		}

		_myLibraryObject.renderLikertScale = function(data, required) {
			if (typeof(required) === 'undefined') required = false;

			if (data.hasOwnProperty("name") === false) return;
			var label = self.renderLabel(data.name, data.label, required);

			var classes = 'form-control ';

			var extras = parsleyTrigger + self.buildMiniMaxi(data);
			var requiredString;
			if (required === true) {
				requiredString = ' required ';
			} else {
				requiredString = '';
			}
			var j = 4;
			if (data.subtype === "withNeutral") j = 5;
			var ids = [];

			for (var i = 0; i < j; i++) {
				ids[i] = data.name + '-' + i;
				self.addIdToPagesIndex(ids[i], data.type);
			}

			var k = 0;

			var input = '';
			input += '<div class="likertScale "><div><table><tr><td class="radio radio-success radio-single">';
			input += '<input type="radio" name="' + data.name + '" ' + self.parsleyGroup() + ' id="' + ids[k++] + '" value="++" ' + requiredString + '/><label></label></td></tr><tr><td><label for="' + data.name + 'stronglyAgree">'+"Tout à fait d'accord"+'</label></td></tr></table></div><div><table><tr><td class="radio radio-success radio-single"><input type="radio" name="' + data.name + '" id="' + ids[k++] + '" ' + self.parsleyGroup() + ' value="+" ' + requiredString + ' /><label></label></td></tr><tr><td><label for="' + data.name + 'agree">'+"Plutôt d'accord"+'</label></td></tr></table></div>';

			if (data.subtype === "withNeutral") {
				input += '<div><table><tr><td class="radio radio-success radio-single"><input type="radio" name="' + data.name + '" id="' + ids[k++] + '" ' + self.parsleyGroup() + ' value="0" ' + requiredString + '/><label></label></td></tr><tr><td><label for="' + data.name + 'neutral">Neutre</label></td></tr></table></div>';
			}
			input += '<div><table><tr><td class="radio radio-success radio-single"><input type="radio" name="' + data.name + '" id="' + ids[k++] + '"' + self.parsleyGroup() + ' value="-" ' + requiredString + '/><label></label></td></tr><tr><td><label for="' + data.name + 'disagree">'+"Plutôt pas d'accord"+'</label></td></tr></table></div><div><table><tr><td class="radio radio-success radio-single"><input type="radio" name="' + data.name + '" id="' + ids[k++] + '"' + self.parsleyGroup() + ' value="--"' + requiredString + '/><label></label></td></tr><tr><td><label for="' + data.name + 'stronglyDisagree">'+"Pas du tout d'accord"+'</label></td></tr></table></div></div>';


			elementsToParse[renderingDiv].push({
				'type': data.type,
				'id': data.name,
				'subIds': ids
			});



			return self.wrapIt(label + "\n" + input, formElementHtml);
		}



		_myLibraryObject.renderSelect = function(data, required) {
			if (typeof(required) === 'undefined') required = false;

			var label = self.renderLabel(data.name, data.label, required);
			var placeholder = self.escapeHtml(self.checkPlaceholder(data));

			var classes = 'form-control ';
			var header = '<select value="" id="' + data.name + '" class="' + classes + '" data-selected-text-format="count > 3" data-parsley-trigger="click" ' + self.parsleyGroup();
			if (required === true) header += ' required ';
			if (placeholder !== '') header += 'title="' + placeholder + '" ';
			if (data.hasOwnProperty("multiple") && data.multiple === true) header += " multiple ";
			header += '>';

			var options = '';
			if (data.hasOwnProperty("values") === false) return;
			var option;
			var optionValue;
			var optionLabel;
			var optionId = '';
			var ids = [];
			for (var i = 0; i < data.values.length; i++) {
				option = data.values[i];
				optionValue = self.escapeHtml(option.value);
				optionLabel = self.escapeHtml(option.label);
				optionId = data.name + '-' + i;
				options += '<option id="' + optionId + '" value="' + optionValue + '" ' + self.parsleyGroup() + ' data-parsley-trigger="click">' + optionLabel + '</option>' + "\n";
				ids.push(optionId);
				self.addIdToPagesIndex(optionId, data.type);

			}
			var closing = '</select>';

			elementsToParse[renderingDiv].push({
				'type': data.type,
				'id': data.name,
				'subIds': ids
			});

			return self.wrapIt(label + "\n" + header + options + closing, formElementHtml);
		}



		_myLibraryObject.renderAdminSelect = function(data, required) {
			if (typeof(required) === 'undefined') required = false;

			var label = self.renderLabel(data.name, data.label, required);
			var placeholder = self.escapeHtml(self.checkPlaceholder(data));

			var classes = 'form-control ';
			var header = '<select data-live-search="true" id="' + data.name + '" class="' + classes + '" data-selected-text-format="count > 3" data-parsley-trigger="click" ' + self.parsleyGroup();
			if (required === true) header += ' required ';
			if (placeholder !== '') header += 'title="' + placeholder + '" ';
			if (data.hasOwnProperty("multiple") && data.multiple === true) header += " multiple ";
			header += '>';

			if (data.hasOwnProperty("groups") === false) return;
			var groupHtml = {
				'open': '',
				'close': '</optgroup>'
			};
			var groupLabel = '';
			var group;
			var options = '';
			var option;
			var optionValue;
			var optionLabel;
			var optionId = '';
			var ids = [];
			var tmp = '';

			for (var j = 0; j < data.groups.length; j++) {

				group = data.groups[j];
				if (group.hasOwnProperty('label') && group.label !== '') {
					groupHtml.open = '<optgroup label="' + group.label + '">';
				} else {
					groupHtml.open = '<optgroup>';
				}

				options = '';
				if (typeof(group.values) !== 'undefined') {
					for (var i = 0; i < group.values.length; i++) {
						option = group.values[i];
						optionValue = self.escapeHtml(option.value);
						optionLabel = self.escapeHtml(option.label);
						optionId = data.name + '-' + j + '-' + i;
						options += '<option id="' + optionId + '" value="' + optionValue + '" ' + self.parsleyGroup() + ' data-parsley-trigger="click">' + optionLabel + '</option>' + "\n";
						ids.push(optionId);
						self.addIdToPagesIndex(optionId, 'select');
					}
				}
				tmp += self.wrapIt(options, groupHtml);
			}


			var closing = '</select>';

			elementsToParse[renderingDiv].push({
				'type': 'select',
				'id': data.name,
				'subIds': ids
			});

			return self.wrapIt(label + "\n" + header + tmp + closing, formElementHtml);
		}




		_myLibraryObject.renderParagraph = function(data, required) {
			var text = self.decodeHtml(data.label);
			text = markdown.toHTML(text);
			text = filterXSS(text);

			return self.wrapIt(text, textElementHtml);
		}

		_myLibraryObject.renderCheckboxGroup = function(data, required) {
			if (typeof(required) === 'undefined') required = false;

			var mainLabel = self.renderLabel(data.name, data.label, required) + '<br>';
			var output = '';
			var classes = 'form-control  '
			var extraInline = '';
			if (data.hasOwnProperty('inline') && data.inline === true) extraInline = ' checkbox-inline ';
			var checkBoxesHtml = {
				"open": '<div class="checkbox checkbox-info ' + extraInline + '" >',
				"close": '</div>'
			};
			var option;
			var optionValue;
			var optionLabel;
			var optionId;
			var tmp;
			var ids = [];
			for (var i = 0; i < data.values.length; i++) {
				tmp = '\n';
				option = data.values[i];
				optionValue = self.escapeHtml(option.value);
				optionLabel = self.escapeHtml(option.label);
				optionId = data.name + '-' + i;
				tmp += '<input type="checkbox" id="' + optionId + '" name="' + data.name + '" ' + self.parsleyGroup() + ' value="' + optionValue + '" data-parsley-multiple="' + data.name + '" ';
				if (required === true && i == 0) tmp += ' data-parsley-mincheck="1" required';
				tmp += ">\n";
				tmp += '<label for="' + optionId + '">' + optionLabel + '</label>';
				output += self.wrapIt(tmp, checkBoxesHtml);
				ids.push(optionId);
				self.addIdToPagesIndex(optionId, data.type);
			}

			output = mainLabel + output;

			if (data.hasOwnProperty("other") && data.other === true) {
				var idOther = data.name + '-other';
				tmp = '<input type="checkbox" id="' + idOther + '" name="' + data.name + '" ' + self.parsleyGroup() + ' onclick="formRenderLib.checkboxOtherText(this.name,this.id)"  data-parsley-multiple="' + data.name + '"  >';
				tmp += '<label for="' + data.name + '-other' + '"> Autre</label>';
				output += self.wrapIt(tmp, checkBoxesHtml);
				output = self.wrapIt(output, formElementHtml);
				self.addIdToPagesIndex(idOther, data.type);


				var idOtherText = idOther + '-text';
				tmp = '<div data-parsley-class-handler="' + idOtherText + '" class="alternativeInput"> <label for="' + idOtherText + '" class="control-label"><span style="color:red">* </span>Merci de remplir ce champ</label>';
				tmp += '<input type="text" id="' + idOtherText + '" name="' + idOtherText + '" ' + self.parsleyGroup() + ' placeholder="Text de remplacement..." class="form-control "></div>';
				var formElHtmlOpen = formElementHtml.open.substring(0, formElementHtml.open.length - 2);
				tmp = formElHtmlOpen + ' displayNone"  id="' + data.name + '-other-div" >' + tmp + formElementHtml.close;
				self.addIdToPagesIndex(idOtherText, 'text');
				self.addIdToPagesIndex(data.name + '-other-div', 'alternativeInput');

				elementsToParse[renderingDiv].push({
					'type': data.type,
					'id': data.name,
					'other': true,
					'subIds': ids,
					'otherId': idOther,
					'otherTextId': idOtherText
				});
				return output + tmp;
			} else {
				elementsToParse[renderingDiv].push({
					'type': data.type,
					'id': data.name,
					'other': false,
					'subIds': ids
				});
				return self.wrapIt(output, formElementHtml);
			}



		}


		_myLibraryObject.renderRadioGroup = function(data, required) {
			if (typeof(required) === 'undefined') required = false;

			var mainLabel = self.renderLabel(data.name, data.label, required) + '<br>';
			var output = '';
			var classes = 'form-control  '
			var extraInline = '';

			var radioHtml = {
				"open": '<div class="radio radio-info" >',
				"close": '</div>'
			};

			var radioInlineHtml = {
				"open": '<div class="radio radio-info radio-inline" >',
				"close": '</div>'
			};

			var elementHtml = radioHtml;
			if (data.hasOwnProperty('inline') && data.inline === true) elementHtml = radioInlineHtml;

			var option;
			var optionValue;
			var optionLabel;
			var optionId;
			var tmp;
			var func = '';
			var ids = [];
			if (data.hasOwnProperty("other") && data.other === true) func = ' onchange="formRenderLib.hideOtherText(this.name)" ';

			for (var i = 0; i < data.values.length; i++) {
				tmp = '\n';
				option = data.values[i];
				optionValue = self.escapeHtml(option.value);
				optionLabel = self.escapeHtml(option.label);
				optionId = data.name + '-' + i;
				tmp += '<input type="radio" id="' + optionId + '" name="' + data.name + '" ' + self.parsleyGroup() + ' value="' + optionValue + '" ' + func;
				if (required === true) tmp += 'required';
				tmp += ">\n";
				tmp += '<label for="' + optionId + '">' + optionLabel + '</label>';
				output += self.wrapIt(tmp, elementHtml);
				ids.push(optionId);
				self.addIdToPagesIndex(optionId, data.type);
			}
			output = mainLabel + output;


			if (data.hasOwnProperty("other") && data.other === true) {
				var idOther = data.name + '-other';

				tmp = '<input type="radio" id="' + idOther + '" name="' + data.name + '"  ' + self.parsleyGroup() + ' onchange="formRenderLib.showOtherText(this.name)">';
				tmp += '<label for="' + data.name + '-other' + '"> Autre</label>';
				tmp = self.wrapIt(tmp, elementHtml);
				output = self.wrapIt(output + tmp, formElementHtml);
				self.addIdToPagesIndex(idOther, data.type);

				var idOtherText = idOther + '-text';
				tmp = '<div data-parsley-class-handler="' + idOtherText + '" class="alternativeInput"> <label for="' + idOtherText + '" class="control-label"><span style="color:red">* </span>Merci de remplir ce champ</label>';
				tmp += '<input type="text" id="' + idOtherText + '" name="' + idOtherText + '" ' + self.parsleyGroup() + ' placeholder="Text de remplacement..." class="form-control "></div>';
				var formElHtmlOpen = formElementHtml.open.substring(0, formElementHtml.open.length - 2);
				tmp = formElHtmlOpen + ' displayNone"  id="' + data.name + '-other-div" >' + tmp + formElementHtml.close;
				self.addIdToPagesIndex(idOtherText, 'text');
				self.addIdToPagesIndex(data.name + '-other-div', 'alternativeInput');




				elementsToParse[renderingDiv].push({
					'type': data.type,
					'id': data.name,
					'other': true,
					'subIds': ids,
					'otherId': idOther,
					'otherTextId': idOtherText
				});
				return output + tmp;
			} else {
				elementsToParse[renderingDiv].push({
					'type': data.type,
					'id': data.name,
					'other': false,
					'subIds': ids
				});
				return self.wrapIt(output, formElementHtml);
			}

		}
		//////////////////////////////////////////////
		//////////////////////////////////////////////
		//////////////////////////////////////////////
		//////////////////////////////////////////////
		//////////////////////////////////////////////
		//////////////////////////////////////////////
		//////////////////////////////////////////////
		// scripts for handling others field

		_myLibraryObject.showOtherText = function(name) {
			var textDiv = $(document.getElementById(name + '-other-div'));
			textDiv.toggleClass("displayNone", false);
			var textInput = $(document.getElementById(name + '-other-text'));
			textInput.prop("required", true);
			textInput.prop("data-parsley-required ", true);
		}

		_myLibraryObject.hideOtherText = function(name) {
			var textDiv = $(document.getElementById(name + '-other-div'));
			textDiv.toggleClass("displayNone", true);
			var textInput = $(document.getElementById(name + '-other-text'));
			textInput.prop("required", false);
			textInput.prop("data-parsley-required ", false);
		}

		_myLibraryObject.checkboxOtherText = function(name, checkBoxId) {
			var checkbox = $(document.getElementById(checkBoxId));
			var textDiv = $(document.getElementById(name + '-other-div'));
			var textInput = $(document.getElementById(name + '-other-text'));

			if (checkbox.is(':checked') === false) {
				textDiv.toggleClass("displayNone", true);
				textInput.prop("required", false);
				textInput.prop("data-parsley-required ", false);
			} else {
				textDiv.toggleClass("displayNone", false);
				textInput.prop("required", true);
				textInput.prop("data-parsley-required", true);
			}
		}

		/**
		 * Function for activating character counting
		 */
		_myLibraryObject.countChars = function(id) {
			var field = $(document.getElementById(id));
			var maxNumberOfChars = field.prop("maxlength");
			if (typeof(maxNumberOfChars) === 'undefined') return;
			var numberOfChars = field.val().length;
			var maxNumberOfChars = field.prop("maxlength");
			$(document.getElementById(id + '-counter')).html(numberOfChars + '/' + maxNumberOfChars);
		}

		//////////////////////////////////////////////
		//////////////////////////////////////////////
		//////////////////////////////////////////////
		//////////////////////////////////////////////
		//////////////////////////////////////////////
		//////////////////////////////////////////////

		var boutonPrevId = '';
		var boutonNextId = '';
		var pageCounterId = '';
		var resetBoutonId = '';


		/**
		 * Function to activate different form interactions
		 * @param  {bool} validate do we need validation
		 */
		_myLibraryObject.insertFormInteractions = function(validate) {
			if (typeof(validate) === 'undefined') validate = true;
			nbOfPages[renderingDiv]--;

			var formNavigation = {
				'open': '<div class="form-navigation">',
				'close': '</div>'
			}

			boutonPrevId = formDivId + '-bouton-prev';
			boutonNextId = formDivId + '-bouton-next';
			pageCounterId = formDivId + '-pageCounter';
			resetBoutonId = formDivId + '-reset';
			var reset = '<div class="resetButton"><button id="' + resetBoutonId + '" type="button" class="btn btn-lg-xs btn-warning">Remettre à zéro <i class="fa fa-trash-o" aria-hidden="true"></i></button></div>'

			if (nbOfPages[renderingDiv] == 1) {
				formInterfaceLib.showNextButton();
				formInterfaceLib.showPreviousButton();
				if (validate === false) return '';
				if (elementsToParse[renderingDiv].length !== 0) return '<div>' + reset + '</div>';
				return '';
			}

			var boutonPrev = '<div style="float:left;"><button id="' + boutonPrevId + '" type="button" class="previous btn btn-lg-xs btn-info"><i class="fa fa-angle-left" aria-hidden="true"></i> Previous</button></div>';
			var pageCount = '<div id="' + pageCounterId + '" class="formPageCounter">Page 1 sur ' + nbOfPages[renderingDiv] + '</div>';
			var boutonNext = '<div style="float:right;"><button id="' + boutonNextId + '" type="button" class="next btn btn-lg-xs btn-info"> &nbsp;&nbsp;&nbsp;Next <i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;</button></div>';

			var main = '<div>' + formNavigation.open + '<hr>' + boutonPrev + boutonNext + pageCount + formNavigation.close + '</div>';
			if (validate === false) return main;
			return reset + main;
		}



		var selectUi;
		/**
		 * Function for activating SelectUi
		 */
		_myLibraryObject.activateSelectUi = function() {
			selectUi = $('select').selectpicker();
		}

		/**
		 * Function for desactivating SelectUi
		 */
		_myLibraryObject.deactivateSelectUi = function() {
			if (typeof(selectUi) === 'undefined') return;
			selectUi.selectpicker('destroy');
		}

		/**
		 * Function for activating form interactions
		 * @param  {bool} validate  Is validation active ?
		 */
		_myLibraryObject.activateFormsInteractions = function(validate) {
			if (typeof(validate) == 'undefined') validate = true;

			selectUi = $('select').selectpicker();

			currentPage[renderingDiv] = 1;

			var $sections = $('#' + formDivId + ' .form-section');


			if (nbOfPages[renderingDiv] > 1) {
				var options = {
					target: document.getElementById(progressbarDivId)
				};

				var nanobar = new Nanobar(options);
				nanobar.go(99 * (currentPage[renderingDiv]) / nbOfPages[renderingDiv]);
			}


			if (validate === true) {
				formInterfaceLib.setFormToValidateId(formDivId);
			}


			function navigateTo(page) {
				// Mark the current section with the class 'current'
				$sections.toggleClass('displayNone', true);
				$sections.eq(page - 1).toggleClass('displayNone', false);
				if (nbOfPages[renderingDiv] === 1) {
					return;
				}

				// Show only the navigation buttons that make sense for the current section:


				$(document.getElementById(boutonPrevId)).toggleClass('displayHidden', page <= 1);
				var atTheEnd = (page == nbOfPages[renderingDiv]);
				$(document.getElementById(boutonNextId)).toggleClass('displayHidden', atTheEnd);



				if (validate === true && atTheEnd === true) {
					formInterfaceLib.showNextButton();
				} else if (validate === true && atTheEnd === false) {
					formInterfaceLib.hideNextButton();
				}


				// update page counter
				$(document.getElementById(pageCounterId)).html("Page " + page + " sur " + nbOfPages[renderingDiv]);
				currentPage[renderingDiv] = page;
				if (nbOfPages[renderingDiv] > 1) nanobar.go(99 * (page) / nbOfPages[renderingDiv]);
			}

			if (nbOfPages[renderingDiv] > 1) {

				// Previous button is easy, just go back
				$(document.getElementById(boutonPrevId)).click(function() {
					var elementOffset = $('#'+formDivId).offset().bottom;
					$("html, body").animate({

						scrollTop: elementOffset
					}, 600);

				if (validate === true) formLib.simpleSave('error');
					navigateTo(currentPage[renderingDiv] - 1);
				});
				var form = $(document.getElementById(formDivId));

				// Next button goes forward iff current block validates
				$(document.getElementById(boutonNextId)).click(function() {
						var block = currentPage[renderingDiv];
						block = 'block-' + block;
						var elementOffset = $('#'+formDivId).offset().bottom;
						if (validate === false || form.parsley().validate({
								group: block
							})) {
							$('html, body').animate({
								scroll: elementOffset
							}, 600);

							if (validate === true) formLib.simpleSave('error');
							navigateTo(currentPage[renderingDiv] + 1);
						}
					}

				);
			};

			if (elementsToParse[renderingDiv].length !== 0) {
				$(document.getElementById(resetBoutonId)).click(function() {
					bootbox.confirm({
						message: "Êtes-vous sur de vouloir remettre à zéro cette page du formulaire ?",
						buttons: {
							confirm: {
								label: 'Oui',
								className: 'btn-warning'
							},
							cancel: {
								label: 'Non',
								className: 'btn-primary'
							}
						},
						callback: function(result) {
							if (result == true) {
								populateFormLib.resetPage(currentPage[renderingDiv]);
							}
						}
					});
				});
			}

			navigateTo(1); // Start at the beginning



		};


		return _myLibraryObject;
	}

	// We need that our library is globally accesible, then we save in the window
	if (typeof(window.formRenderLib) === 'undefined') {
		window.formRenderLib = myLibrary();
	}
})(window); // We send the window variable withing our function
