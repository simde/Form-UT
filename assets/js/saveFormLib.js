/**
 * Library for simple saving of a form
 */
(function(window) {
	// You can enable the strict mode commenting the following line
	'use strict';

	function myLibrary() {
		var _myLibraryObject = {};

		/**
		 * Simple var for holding this.
		 * @type {Object}
		 */
		var self;

		/**
		 * Object containing all the fields id and their associated value
		 * @type {Object}
		 */
		var elementsSerialized = new Object();

		/**
		 * Getter to the serialized elements
		 * @return {object}
		 */
		_myLibraryObject.getElementsSerialized = function() {
			return elementsSerialized;
		}

		/**
		 * Object containing all the main fields and the associated results
		 * @type {Object}
		 */
		var results = new Object();

		/**
		 * array containing all the fields id that needs to be parsed.
		 * It contains the name, type and id. TODO check this
		 * @type {array}
		 */
		var elementsToParse;

		/**
		 * Function for turning an array into string
		 * @param  {array} res
		 * @return {string}
		 */
		_myLibraryObject.concatResArray = function(res) {
			if (res.length > 0) {
				var tmp = '';
				for (var i = 0; i < res.length; i++) {
					tmp += '|' + res[i] + '|';
				}
				return tmp;

			}

			return '';
		}


		/**
		 * Main function for saving
		 * @param  {array} elementsToParse
		 * @return {object} returns the results and object serialized
		 */
		_myLibraryObject.saveNow = function(elementsToParse) {
			if (typeof(elementsToParse) === 'undefined') elementsToParse = formRenderLib.getElementsToParse();

			formRenderLib.deactivateSelectUi();
			self = this;
			elementsSerialized = new Object();
			results = new Object();

			for (var i = 0; i < elementsToParse.length; i++) {
				self.saveSwticher(elementsToParse[i]);
			}

			formRenderLib.activateSelectUi();
			return {
				'results': results,
				'elementsSerialized': elementsSerialized
			};
		};


		/**
		 * Switch for saving values
		 * @param  {object} data one element of the form
		 */
		_myLibraryObject.saveSwticher = function(data) {
			if (data.type === "text" || data.type === "textarea" || data.type === "date" || data.type === "number") {
				self.saveClassicInput(data);
			} else if (data.type === "likertScale" || data.type === "select" || data.type === "checkbox-group" || data.type === "radio-group") {
				self.saveGroup(data);
			} else return;
		};


		/**
		 * Function for saving classic inputs
		 * @param  {object} data one element of the form
		 */
		_myLibraryObject.saveClassicInput = function(data) {
			var field = $(document.getElementById(data.id));
			var val = field.val().replace('|', '/'); //for preventing errors when parsing group inputs.
			elementsSerialized[data.id] = val;
			results[data.id] = val;
		}


		/**
		 * Function for saving group inputs
		 * @param  {object} data one element of the form
		 */
		_myLibraryObject.saveGroup = function(data) {
			var fields = data.subIds;
			var checked;
			var fieldId;
			var field;
			var res = [];
			for (var i = 0; i < fields.length; i++) {
				fieldId = fields[i];
				field = $(document.getElementById(fieldId));
				checked = field.is(':checked');
				elementsSerialized[fieldId] = checked;
				if (checked === true) res.push(field.val());
			}

			if (data.hasOwnProperty('other') && data.other === true) {
				var textOtherId = data.otherTextId;
				var textOther = $(document.getElementById(textOtherId)).val().replace('|', '/');
				elementsSerialized[textOtherId] = textOther;

				if ($(document.getElementById(data.otherId)).is(":checked") === true) {
					elementsSerialized[data.otherId] = true;
					res.push(textOther);
				} else if (data.hasOwnProperty('other') && data.other === false) {
					elementsSerialized[data.otherId] = false;
				}

			}
			if (data.type === 'radio-group' || data.type ==='likertScale'){
				if (res.length === 0) {
					results[data.id] ='';
				} else {
					results[data.id] = res[0];
				}
			} else {
				results[data.id] = self.concatResArray(res);
			}
		}




		return _myLibraryObject;
	}

	// We need that our library is globally accesible, then we save in the window
	if (typeof(window.saveFormLib) === 'undefined') {
		window.saveFormLib = myLibrary();
	}
})(window); // We send the window variable withing our function
