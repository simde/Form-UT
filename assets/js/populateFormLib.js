/**
 * JS Library for populating a form
 */
(function(window) {
	// You can enable the strict mode commenting the following line
	'use strict';

	function myLibrary() {
		var _myLibraryObject = {};

		/**
		 * Var holding this
		 * @type {object}
		 */
		var self;

		/**
		 * Object containing all the fields id and their associated value
		 * @type {Object}
		 */
		var elementsSerialized;
		_myLibraryObject.setElementsSerialized = function(data) {
			elementsSerialized = data;
		}


		/**
		 * array containing all the fields id that needs to be parsed.
		 * It contains the name, type and id. TODO check this
		 * @type {array}
		 */
		var elementsToParse;


		/**
		 * Main function for form population
		 */
		_myLibraryObject.populateNow = function() {
			formRenderLib.deactivateSelectUi();
			elementsToParse = formRenderLib.getElementsToParse();

			if (jQuery.isEmptyObject(elementsSerialized) === true || elementsToParse == []) {
				return;
			}
			self = this;

			for (var i = 0; i < elementsToParse.length; i++) {
				self.populateSwticher(elementsToParse[i]);
			}
			formRenderLib.activateSelectUi();
		};

		/**
		 * Switch for populating the fields according to their types
		 */
		_myLibraryObject.populateSwticher = function(data) {
			if (data.type === "text" || data.type === "textarea" || data.type === "date" || data.type === "number") {
				self.populateClassicInputs(data);
			} else if (data.type === "likertScale" || data.type === "select" || data.type === "checkbox-group" || data.type === "radio-group") {
				self.populateGroupInputs(data);
			} else return;
		};


		/**
		 * For populating classic inputs
		 */
		_myLibraryObject.populateClassicInputs = function(data) {
			var field = $(document.getElementById(data.id));
			field.val(elementsSerialized[data.id]);
			formRenderLib.countChars(data.id); // this will update the counters that can be updated
		}


		/**
		 * For populating group inputs
		 */
		_myLibraryObject.populateGroupInputs = function(data) {
			var fields = data.subIds;
			var fieldId;
			var field;
			var val;
			for (var i = 0; i < fields.length; i++) {
				fieldId = fields[i];
				if (elementsSerialized[fieldId] === 'true') {
					elementsSerialized[fieldId] = true;
				} else if ((elementsSerialized[fieldId] === 'false')) {
					elementsSerialized[fieldId] = false;
				}
				if (elementsSerialized[fieldId] === false) continue;
				field = $(document.getElementById(fieldId));
				field.prop("checked", elementsSerialized[fieldId]);
				field.prop("selected", elementsSerialized[fieldId]);
			}

			if (data.hasOwnProperty('other')) {
				self.populateClassicInputs(data);
				var checked = elementsSerialized[data.otherId];
				$(document.getElementById(data.otherId)).prop("checked", checked);
				$(document.getElementById(data.otherId)).prop("selected", checked);

				var textDiv = $(document.getElementById(data.id + '-other-div'));
				textDiv.toggleClass("displayNone", !checked);
				var textInput = $(document.getElementById(data.id + '-other-text'));
				textInput.prop("required", checked);
				textInput.prop("data-parsley-required ", checked);
			}
		}



		/**
		 * Holds the page index array
		 * @type {Array}
		 */
		var pagesIndex = [];
		_myLibraryObject.setPagesIndex = function(index) {
			pagesIndex = index;
		}

		/**
		 * Function for reseting a page
		 * @param  {int} page page nimber
		 */
		_myLibraryObject.resetPage = function(page) {
			formRenderLib.deactivateSelectUi(); //we have to desactivate the stylish ui arround select for this to work fine !

			self = this;
			var pageElements = pagesIndex[page];

			for (var i = 0; i < pageElements.length; i++) {
				self.resetSwticher(pageElements[i]);
			}
			formRenderLib.activateSelectUi();

		};


		/**
		 * Reset switcher based on the type of the field
		 */
		_myLibraryObject.resetSwticher = function(data) {
			if (data.type === "text" || data.type === "textarea" || data.type === "date" || data.type === "number") {
				self.resetClassicInputs(data);
			} else if (data.type === "likertScale" || data.type === "select" || data.type === "checkbox-group" || data.type === "radio-group") {
				self.resetGroupInputs(data);
			} else if (data.type === 'alternativeInput') {
				$(document.getElementById(data.id)).toggleClass('displayNone', true);
			} else return;
		};


		/**
		 * For classic inputs :
		 */
		_myLibraryObject.resetClassicInputs = function(data) {
			var field = $(document.getElementById(data.id));
			field.val('');
			formRenderLib.countChars(data.id); // this will update the counters that can be updated
		}

		/**
		 * For group inputs :
		 */
		_myLibraryObject.resetGroupInputs = function(data) {
			$(document.getElementById(data.id)).prop("checked", false);
			$(document.getElementById(data.id)).prop("selected", false);
		}

		return _myLibraryObject;
	}

	// We need that our library is globally accesible, then we save in the window
	if (typeof(window.populateFormLib) === 'undefined') {
		window.populateFormLib = myLibrary();
	}
})(window); // We send the window variable withing our function
