
/**
 * Js lib for manipulating the form interface and also the page elements sometimes
 */

(function(window) {
	// You can enable the strict mode commenting the following line
	'use strict';

	function myLibrary() {
		var _myLibraryObject = {};

		var navigationDivId = 'mainNavigationDiv';
		var mainPreviousStepButtonDivId = 'mainPreviousStepButton';
		var mainNextStepButtonDivId = 'mainNextSetpButton';
		var mainSaveButtonDivId = 'mainSaveButton';


		var nextStepUrl = '';
		_myLibraryObject.setNextStepUrl = function(url) {
			nextStepUrl = url;
		}

		var previousStepUrl = '';
		_myLibraryObject.setPreviousStepUrl = function(url) {
			previousStepUrl = url;
		}

		var formToValidateId = '';
		_myLibraryObject.setFormToValidateId = function(id) {
			formToValidateId = id;
		}



		/**
		 * Function to show the main next step button on the page
		 */
		_myLibraryObject.showNextButton = function() {
			if (nextStepUrl == '') {
				return;
			}
			var html = '<button type="button" class="btn btn-success btn-lg nextButton" id="buttonNextStep">';
			html += " Passer à l'étape suivante ";
			html += ' <i class="fa fa-angle-double-right"></i>';
			html += '</button>';

			$(document.getElementById(mainNextStepButtonDivId)).html(html);

			$(document.getElementById('buttonNextStep')).click(function() {

				if ($(document.getElementById(formToValidateId)).parsley().validate() === true) {
					formLib.mainSave('error', nextStepUrl);
				} else {
				}
			})
		};


		/**
		 * Function to hide the main nexgt step button on the page
		 */
		_myLibraryObject.hideNextButton = function() {
			$(document.getElementById(mainNextStepButtonDivId)).html('');
		}

		/**
		 * Function to show the main previous step button on the page
		 */
		_myLibraryObject.showPreviousButton = function() {
			if (previousStepUrl == '') {
				return;
			}
			var html = '<button type="button" class="btn btn-secondary btn-lg previousButton" id="buttonPreviousStep">';
			html += '<i class="fa fa-angle-double-left"></i>';
			html += " Retour à l'étape précédente ";
			html += '</button>';

			$(document.getElementById(mainPreviousStepButtonDivId)).html(html);

			$(document.getElementById('buttonPreviousStep')).click(function() {
				if ($(document.getElementById(formToValidateId)).parsley().validate() === true) {
					formLib.simpleSave('error', previousStepUrl);
				} else {
				}
			})
		};

		/**
		 * Function to hide the main previous step button on the page
		 */
		_myLibraryObject.hidePreviousButton = function() {
			$(document.getElementById(mainPreviousStepButtonDivId)).html('');
		}

		/**
		 * Function to show the tsave button
		 */
		_myLibraryObject.showSaveButton = function() {
			var html = '<br> <hr><button type="button" class="btn btn-success btn-lg saveButton" id="buttonSave">';
			html += " Enregistrer ";
			html += ' <i class="fa fa-download"></i>';
			html += '</button>';

			$(document.getElementById(mainSaveButtonDivId)).html(html);

			$(document.getElementById('buttonSave')).click(function() {
				if ($(document.getElementById(formToValidateId)).parsley().validate() === true) {
					formLib.mainSave('all', 'none');
				} else {
				}
			})
		};

		return _myLibraryObject;
	}

	// We need that our library is globally accesible, then we save in the window
	if (typeof(window.formInterfaceLib) === 'undefined') {
		window.formInterfaceLib = myLibrary();
	}
})(window); // We send the window variable withing our function
