
/**
 * Library for simple notifications
 */
(function(window){
  // You can enable the strict mode commenting the following line
   'use strict';

  function myLibrary(){
    var _myLibraryObject = {};

		/**
		 * standard options for creating notifications
		 * @type {Object}
		 */
		var notifyOptions = {
			type: 'info',
			delay:6500,
			placement:
				{
					from: "top",
					align: "right"
				},
			animate:
				{
					enter: 'animated fadeInDown',
					exit: 'animated fadeOutRight'
				},
			showProgressbar: false,
			};

			var data = {
				title : 'NULL',
				message : 'NULL'
			};

    /**
     * Simple notification
     * @param  {string} title
     * @param  {string} message
     * @param  {string} type    type of notification
		 * @return {notification}
     */
    _myLibraryObject.show = function(title,message,type){
			notifyOptions.type=type;
			data.title = title;
			data.message = message;
			return $.notify(data,notifyOptions);
    };

		/**
		 * standard notification when saving
		 * @return {notification}
		 */
		_myLibraryObject.saving = function(){
			notifyOptions.type='info';
			data.title = "<i class='fa fa-circle-o-notch fa-spin color-orange'> </i> Enregistrement en cours...";
			data.message = "";
			return $.notify(data,notifyOptions);
		};


		/**
		 * standard notification when there is nothing to save
		 * @return {notification}
		 */
		_myLibraryObject.nothingToSave = function(){
			notifyOptions.type='info';
			data.title = "<i class='fa fa-flag' ></i>";
			data.message = "Il n'y a rien de nouveau à enregistrer.";
			return $.notify(data,notifyOptions);
		};

		/**
		 * standard notification when saving is successful
		 * @return {notification}
		 */
		_myLibraryObject.saved = function(){
			notifyOptions.type='success';
			data.title = "<i class='fa fa-check' ></i> Opération réussie !";
			data.message = "Les données ont bien été enregistrées.";
			return $.notify(data,notifyOptions);
		};


		/**
		* standard notification for success
		 * @param  {string} message
		 * @return {notification}
		 */
		_myLibraryObject.success = function(message){
			notifyOptions.type='success';
			data.title = "<i class='fa fa-check' ></i> Opération réussie !";
			data.message = message;
			return $.notify(data,notifyOptions);
		};

		/**
		 * standard notification when data have been successfully retreive
		 * @return {notification}
		 */
		_myLibraryObject.restoredSuccess = function(){
			notifyOptions.type='success';
			data.title = "<i class='fa fa-check' ></i> Opération réussie !";
			data.message = "Les données précédentes ont été synchronisées avec le serveur.";
			return $.notify(data,notifyOptions);
		};

		/**
		* standard notification for when there is an error
		 * @param  {string} message
		 * @return {notification}
		 */
		_myLibraryObject.error = function(message){
			notifyOptions.type='danger';
			data.title = "<i class='fa fa-exclamation-triangle' ></i> Erreur <i class='fa fa-exclamation-triangle' ></i>";
			data.message = message;
			return $.notify(data,notifyOptions);
		};


		/**
		* standard notification for warning
		 * @param  {string} message
		 * @return {notification}
		 */
		_myLibraryObject.warning = function(message){
			notifyOptions.type='warning';
			data.title = "<i class='fa fa-exclamation-triangle' ></i> Attention <i class='fa fa-exclamation-triangle' ></i>";
			data.message = message;
			return $.notify(data,notifyOptions);
		};

    return _myLibraryObject;
  }

  // We need that our library is globally accesible, then we save in the window
  if(typeof(window.notifLib) === 'undefined'){
    window.notifLib = myLibrary();
  }
})(window); // We send the window variable withing our function
