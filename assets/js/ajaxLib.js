
/**
 * library used for making standardized ajax query to the server.
 */
(function(window) {
	'use strict';

	function myLibrary() {
		var _myLibraryObject = {};


		/**
		 * standardized request
		 * @type {Object}
		 */
		var request = {
			type: 'POST',
			url: 'NULL',
			dataType: "json",
			data: {
				"token": 'NULL',
				"redirect": 'NULL',
				"data": 'NULL'
			},
		};

		/**
		 * Sets the location of the ajaw receiver
		 * @param  {[string]} newUrl
		 */
		_myLibraryObject.setURL = function(newUrl) {
			request.url = newUrl;
		};

		/**
		 * Array holding the ajax tockens available
		 * @type {Array}
		 */
		var ajaxTokens = [];


		/**
		 * Function for adding a tocken to the array
		 * @param  {[string]} newToken
		 */
		_myLibraryObject.addAjaxToken = function(newToken) {
			ajaxTokens.push({
				"token": newToken,
				'used': false
			});
		}

		/**
		 * Function for using an ajaxToken
		 * @return {string} token
		 */
		_myLibraryObject.useToken = function() {
			for (var i = 0; i < ajaxTokens.length; i++) {
				if (ajaxTokens[i].used === false) {
					ajaxTokens[i].used = true;
					return ajaxTokens[i].token;
				}
			}
			notifLib.error("There are no more tokens that can be used for connecting to the server, please refresh the page");
			return false;
		}

		/**
		 * Not sure this is very usefull
		 * TODO clean !
		 * @type {String}
		 */
		var dataToReturn = '';


		/**
		 * Main ajax interface
		 * @param  {string} redirect  Where to be be residrect if the request is successful (and the returned data mentionned that it really is successful)
		 * @param  {object} data      data to send to the server
		 * @param  {string} notifMode Do we want notification poping
		 * @return {ajaxCall}
		 */
		_myLibraryObject.doItMain = function(redirect, data, notifMode) {
			self = this;

			var ajaxToken = self.useToken();
			if (ajaxToken === false) return;
			request.data.token = ajaxToken;

			if (notifMode == 'all') {
				var notif = notifLib.saving();
			}
			request.data.redirect = redirect;

			/**
			 * Function for making sure that empty objects are sent
			 * @param       {object} obj
			 * @return      {object}
			 */
			function __validate(obj) {
				if (typeof(obj) !== 'object') return obj;
				for (var key in obj) {

					if (typeof(obj[key]) === 'object') {
						if(jQuery.isEmptyObject(obj[key])) obj[key] = [''];
						else obj[key] = __validate(obj[key]);
					} else {
						if (obj[key].length == 0) {
							obj[key] = '';
						}

					}

				}

				return obj;
			}

			request.data.data = __validate(data);
			request.success = function(returnedData) {
				if (returnedData.hasOwnProperty('newAjaxToken')) self.addAjaxToken(returnedData.newAjaxToken);

				if (returnedData.success == 'SUCCESS') {
					if (notifMode == 'all') {
						setTimeout(function() {
							notif.close();
						}, 1000);
						notifLib.saved();
					}

				} else {
					if (notifMode == 'all' || notifMode == 'error') {
						notifLib.error(returnedData.message);
					}
				}
			};

			request.error = function(returnedData) {
				if (notifMode == 'all' || notifMode == 'error') {
					notifLib.error("Impossible de conacter le server ou les données renvoyées sont illisibles, veillez recharger la page..." + JSON.stringify(returnedData));
				}
				dataToReturn = false;
				return;
			};

			return $.ajax(request);
		};
		// end doit Main


		/**
		 * Ajax interface when we want all notifications
		 * @param  {string} redirect  Where to be be residrect if the request is successful (and the returned data mentionned that it really is successful)
		 * @param  {object} data      data to send to the server
		 * @return {ajaxCall}
		 */
		_myLibraryObject.doIt = function(redirect, data) {
			return this.doItMain(redirect, data, 'all');
		};
		// end doit


		/**
		 * Ajax interface when we want only errors notifications
		 * @param  {string} redirect  Where to be be residrect if the request is successful (and the returned data mentionned that it really is successful)
		 * @param  {object} data      data to send to the server
		 * @return {ajaxCall}
		 */
		_myLibraryObject.doItWithErrors = function(redirect, data) {
			return this.doItMain(redirect, data, 'error');
		};
		// end silent



		return _myLibraryObject;

	}


	// We need that our library is globally accesible, then we save in the window
	if (typeof(window.ajaxLib) === 'undefined') {
		window.ajaxLib = myLibrary();
	};
})(window); // We send the window variable withing our function
