/**
 * Library built around formBuilder.online
 *
 * TODO remove checks on data on server...
 */

(function(window) {
	// You can enable the strict mode commenting the following line
	'use strict';

	function myLibrary() {
		var _myLibraryObject = {};

		var currentData = '[]';
		var currentDataWithGoodNames = '[]';
		var dataOnServer = '[]';
		var dataOnserverWithGoodNames = '[]'; //that's actualy what is on the server
		var pageRedirection;

		/**
		 * Var containing the form builder object
		 * @type {object}
		 */
		var formBuilder;

		var nextPage = 'none';

		_myLibraryObject.setNextPage = function(url) {
			nextPage = url;
		}

		// for ajax
		var populateFormHandler = '';
		_myLibraryObject.setPopulateFormHandler = function(handler) {
			populateFormHandler = handler;
		}

		// for ajax
		var mainSavingFormHandler = '';
		_myLibraryObject.setMainSavingFormHandler = function(handler) {
			mainSavingFormHandler = handler;
		}

		// for ajax
		var simpleSavingFormHandler = '';
		_myLibraryObject.setSimpleSavingFormHandler = function(handler) {
			simpleSavingFormHandler = handler;
		}


		/**
		 * Main function for initiating formBuilder
		 * @param  {JSONString} initialData
		 */
		_myLibraryObject.initFormBuilder = function(initialData) {
			if (typeof(initialData)==='undefined') initialData = '[]';
			var fbTemplate = document.getElementById("fb-editor");

			/**
			 * Big var containing all the options for the formbuilder object
			 * @type {Object}
			 */
			var options = {
				editOnAdd: true,
				showActionButtons: false,
				formData: initialData,
				disableFields: ['autocomplete', 'file', 'hidden', 'button', 'header'],
				disabledAttrs: ['value', 'toggle', 'className', 'access', 'role', 'name', 'Help Text', 'description', 'step', 'selected'],
				fields: [{
						label: 'Nouvelle page',
						attrs: {
							type: 'newPageDelimiter'
						},
						icon: '📄'
					},
					{
						label: 'Échelle de Likert',
						attrs: {
							type: 'likertScale'
						},
						icon: '📏'
					}
				],
				templates: {
					newPageDelimiter: function(fieldData) {
						return {
							field: '<span id="' + fieldData.name + '">',
							onRender: function() {
								$(document.getElementById(fieldData.name)).html("Le contenu en dessous de cet élément sera disposé sur une autre page du formulaire (se référer à la prévisualisation).<hr>");
							}
						};
					},
					likertScale: function(fieldData) {
						return {
							field: '<span id="' + fieldData.name + '">',
							onRender: function() {

								var tmp = '';
								tmp += '<div class="likertScale"><div><table><tr><td class="radio radio-info radio-single">';
								tmp += '<input type="radio" name="' + fieldData.name + '" id="' + fieldData.name + 'stronglyAgree"/><label></label></td></tr><tr><td><label for="' + fieldData.name + 'stronglyAgree">' + "Tout à fait d'accord" + '</label></td></tr></table></div><div><table><tr><td class="radio radio-info radio-single"><input type="radio" name="' + fieldData.name + '" id="' + fieldData.name + 'agree"/><label></label></td></tr><tr><td><label for="' + fieldData.name + 'agree">' + "Plutôt d'accord" + '</label></td></tr></table></div>';

								if (fieldData.subtype === "withNeutral") {
									tmp += '<div><table><tr><td class="radio radio-info radio-single"><input type="radio" name="' + fieldData.name + '" id="' + fieldData.name + 'neutral"/><label></label></td></tr><tr><td><label for="' + fieldData.name + 'neutral">Neutre</label></td></tr></table></div>';
								}
								tmp += '<div><table><tr><td class="radio radio-info radio-single"><input type="radio" name="' + fieldData.name + '" id="' + fieldData.name + 'disagree"/><label></label></td></tr><tr><td><label for="' + fieldData.name + 'disagree">' + "Plutôt pas d'accord" + '</label></td></tr></table></div><div><table><tr><td class="radio radio-info radio-single"><input type="radio" name="' + fieldData.name + '" id="' + fieldData.name + 'stronglyDisagree"/><label></label></td></tr><tr><td><label for="' + fieldData.name + 'stronglyDisagree">' + "Pas du tout d'accord" + '</label></td></tr></table></div></div>';
								$(document.getElementById(fieldData.name)).html(tmp);
							}
						};
					}
				}, // end of template
				typeUserDisabledAttrs: {
					'newPageDelimiter': ['required', 'label', 'placeholder'],
					'likertScale': ['placeholder'],
					'textarea': ['subtype'],
					'text': ['subtype'],
					'paragraph': ['subtype']
				},
				typeUserAttrs: {
					likertScale: {
						subtype: {
							label: 'Version',
							options: {
								'withNeutral': 'Avec neutre',
								'withoutNeutral': 'Sans neutre'
							}
						}
					}
				},
				controlOrder: [
					'paragraph',
					'newPageDelimiter',
					'text',
					'textarea',
					'likertScale',
					'select',
					'radio-group',
					'checkbox-group',
					'number',
					'date'
				],
				i18n: {
					locale: 'fr-FR',
					location: translationLocation,
					extension: '.lang'
				}
			};

			formBuilder = $(fbTemplate).formBuilder(options);
		}

		var translationLocation = '/form-UT/assets/lang/';

		/**
		 * Function for setteing the translation location on the server
		 * @param  {string} url
		 */
		_myLibraryObject.setTranslationLocationUrl = function(url){
			translationLocation = url;
		}


		var currentNames = [];

		/**
		 * Funciton to make sure that there names in each fields
		 */
		_myLibraryObject.makeSureThereAreNames = function() {
			currentData = JSON.parse(currentData);
			currentNames = [];
			currentDataWithGoodNames = currentData;

			for (var i = 0; i < currentData.length; i++) {
				currentDataWithGoodNames[i].name = self.checkName(currentDataWithGoodNames[i]);
			}
			currentDataWithGoodNames = JSON.stringify(currentDataWithGoodNames);
		}


		/**
		 * Function that checks if the name exists already, if yes a new one is generated
		 * @param  {object} data formelement
		 * @return {string}
		 */
		_myLibraryObject.checkName = function(data) {
			var newName;
			if (typeof(data.name) !== 'undefined') {
				newName = data.name;
			} else {
				newName = data.type + '-' + self.makeID(15);
			}

			while ($.inArray(newName, currentNames) !== -1) {
				newName = data.type + '-' + self.makeID(15);
			}

			currentNames.push(newName);
			return newName;
		}


		/**
		 * Function for generating an id
		 * @param  {int} lengthNeeded length of the id
		 * @return {string}
		 */
		_myLibraryObject.makeID = function(lengthNeeded) {

			var text = "";
			var possible = "abcdefghijklmnopqrstuvwxyz0123456789";

			for (var i = 0; i < lengthNeeded; i++)
				text += possible.charAt(Math.floor(Math.random() * possible.length));

			return text;
		}

		var self;


		/**
		 * Function for simple saving of the architecture
		 */
		_myLibraryObject.simpleSaveArchitecture = function(notifMode, redirect) {
			if (typeof(notifMode) === 'undefined') notifMode = 'error';
			if (typeof(redirect) === 'undefined') notifMode = 'none';

			self = this;
			return self.saveArchitecture(simpleSavingFormHandler, notifMode, redirect);
		}


		/**
		 * Function for main saving of the architecture
		 */
		_myLibraryObject.mainSaveArchitecture = function(notifMode) {
			if (typeof(notifMode) === 'undefined') notifMode = 'error';
			self = this;
			return self.saveArchitecture(mainSavingFormHandler, 'error', nextPage);
		}


		/**
		 * Function for saving the architecture
		 * @param  {string} handler        What ajax handler is used
		 * @param  {string} notifMode      Notifs ?
		 * @param  {string} masterRedirect Redirection if successful ?
		 */
		_myLibraryObject.saveArchitecture = function(handler, notifMode, masterRedirect) {
			if (typeof(notifMode) === 'undefined') notifMode = 'error';
			if (typeof(masterRedirect) === 'undefined') masterRedirect = 'none';
			pageRedirection = masterRedirect;
			currentData = formBuilder.actions.getData('json');

			self.makeSureThereAreNames();
			var data = {
				"id": id,
				"formBuilderData": currentDataWithGoodNames
			};
			var ajaxCall = ajaxLib.doItMain(handler, data, notifMode);

			ajaxCall.done(this.successSavingArchitecture);

		}

		/**
		 * Callback if the ajac call is successful
		 */
		_myLibraryObject.successSavingArchitecture = function(returnedData) {
			if (returnedData.success == 'SUCCESS') {
				dataOnServer = JSON.stringify(currentData);
				dataOnserverWithGoodNames = currentDataWithGoodNames;
				if (pageRedirection != 'none') {
					window.location.href = pageRedirection;
				}
			}
		};


		/**
		 * Function to get the data out of the formBuilder object
		 * @return {array}
		 */
		_myLibraryObject.preview = function() {
			currentData = formBuilder.actions.getData('json');
			self = this;
			self.makeSureThereAreNames();
			return currentDataWithGoodNames;
		}


		/**
		 * Function to update the formbuilder content
		 * @param  {string} formBuilderArchitecture TODO array ?
		 */
		_myLibraryObject.updateFormBuilderContent = function(formBuilderArchitecture) {
			if (typeof(formBuilder.actions.setData) !== 'function') {
				setTimeout(function() {
					formBuilder.actions.setData(formBuilderArchitecture);
				}, 1000); // this is added because sometime the object  wasn't build yet...
			} else {
				formBuilder.actions.setData(formBuilderArchitecture);
			}
		}

		var id = '';
		_myLibraryObject.setFormId = function(idddd) {
			id = idddd;
		}

		/**
		 * ajax function for retreiving data from server
		 */
		_myLibraryObject.retreiveFormArchitectureFromServer = function() {
			var data = {
				"id": id
			};
			var ajaxCall = ajaxLib.doItWithErrors(populateFormHandler, data);
			ajaxCall.done(this.successRetreivingArchitecture.bind(this));
		}

		/**
		 * Callback fro treating the returned data
		 */
		_myLibraryObject.successRetreivingArchitecture = function(returnedData) {
			if (returnedData.success == 'SUCCESS') {
				this.updateFormBuilderContent(JSON.stringify(returnedData.data.formBuilderData));
				notifLib.restoredSuccess();
			}
		};

		/**
		 * Simple hook for clearing data in the formBuilder
		 * @return {[type]} [description]
		 */
		_myLibraryObject.clearFields = function() {
			formBuilder.actions.clearFields();
		};


		return _myLibraryObject;
	}

	// We need that our library is globally accesible, then we save in the window
	if (typeof(window.formBuilderLib) === 'undefined') {
		window.formBuilderLib = myLibrary();
	}
})(window); // We send the window variable withing our function
