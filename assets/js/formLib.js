
/**
 * Js lib for easy manipulation of the other libs
 */
(function(window) {
	// You can enable the strict mode commenting the following line
	'use strict';

	function myLibrary() {
		var _myLibraryObject = {};

		/**
		 * Object stroring the data on the server
		 * @type {Object}
		 */
		var dataOnServer = {};

		/**
		 * var storing the data from the form
		 * @type {object}
		 */
		var dataFromForm;

		/**
		 * id of the form
		 * @type {String}
		 */
		var formId = '';

		/**
		 * Ajax handler for simple saving
		 * @type {String}
		 */
		var simpleSavingFormHandler = '';

		/**
		 * Main ajac handler for saving (usualy their will be server checks for this)
		 * @type {String}
		 */
		var mainSavingFormHandler = '';

		/**
		 * Ajax handler for populating the form
		 * @type {String}
		 */
		var populateFormHandler = '';

		/**
		 * Is there page redirection after click on next button ?
		 * @type {String}
		 */
		var pageRedirection = 'none';

		/**
		 * TODO USELESS ?
		 * @type {String}
		 */
		var quickGetterHandler = '';

		/**
		 * TODO USELESS ?
		 * @type {String}
		 */
		var quickSetterHandler = '';



		_myLibraryObject.setFormId = function(id) {
			formId = id;
		}

		_myLibraryObject.setSimpleSavingFormHandler = function(handler) {
			simpleSavingFormHandler = handler;
		}

		_myLibraryObject.setMainSavingFormHandler = function(handler) {
			mainSavingFormHandler = handler;
		}

		_myLibraryObject.setPopulateFormHandler = function(handler) {
			populateFormHandler = handler;
		}

		_myLibraryObject.setQuickGetterHandler = function(handler) {
			quickGetterHandler = handler;
		}

		_myLibraryObject.setQuickSetterHandler = function(handler) {
			quickSetterHandler = handler;
		}

		var quickRenderingDiv = '';
		_myLibraryObject.setQuickRenderingDiv = function(divid) {
			quickRenderingDiv = divid;
		}

		/**
		 * Token associated with the form for server side validaion, this has to be sent with the results
		 * @type {string}
		 */
		var tokenForFormValidation;

		var dataFromForm;

		/**
		 * Object holding the elementsToParse
		 * @type {object}
		 */
		var elementsToParse;

		var self;


		/**
		 * Function for simple save (using simple ajax handler)
		 */
		_myLibraryObject.simpleSave = function(notifMode, redirect) {
			if (typeof(notifMode) == 'undefined') notifMode = 'error';
			if (typeof(redirect) == 'undefined') redirect = 'none';
			self = this;

			return self.save(notifMode, redirect, simpleSavingFormHandler);
		}


		/**
		 * Function for main save (using main ajax handler)
		 */
		_myLibraryObject.mainSave = function(notifMode, redirect) {
			if (typeof(notifMode) == 'undefined') notifMode = 'error';
			if (typeof(redirect) == 'undefined') redirect = 'none';
			self = this;
			console.log("mainSave");
			return self.save(notifMode, redirect, mainSavingFormHandler);
		}

		/**
		 * Interface for saving
		 * @param  {string} notifMode         What kind of notif do we want ?
		 * @param  {string} redirect          Redirection afterwards ?
		 * @param  {string} savingFormHandler What kind of handler to use
		 */
		_myLibraryObject.save = function(notifMode, redirect, savingFormHandler) {
			if (typeof(notifMode) == 'undefined') notifMode = 'error';
			if (typeof(redirect) == 'undefined') redirect = 'none';


			pageRedirection = redirect;
			dataFromForm = saveFormLib.saveNow(elementsToParse);
			var data = {
				"id": formId,
				"dataFromForm": dataFromForm,
				"tokenForFormValidation": tokenForFormValidation
			};
			var ajaxCall = ajaxLib.doItMain(savingFormHandler, data, notifMode);
			ajaxCall.done(self.successSavingData);

		};

		/**
		 * callback for treating the returning data from the ajax call
		 * @param  {object} returnedData [description]
		 */
		_myLibraryObject.successSavingData = function(returnedData) {
			if (returnedData.success == 'SUCCESS') {
				dataOnServer = dataFromForm;
				if (pageRedirection != 'none') {
					window.location.href = pageRedirection;
				}
			}
		};

		/////////////////////////////////////////////////////////////////////:
		/////////////////////////////////////////////////////////////////////:
		/////////////////////////////////////////////////////////////////////:
		/////////////////////////////////////////////////////////////////////:
		/////////////////////////////////////////////////////////////////////:
		/////////////////////////////////////////////////////////////////////:
		/////////////////////////////////////////////////////////////////////:

		/**
		 * Function use for managing an entire form !
		 */
		_myLibraryObject.setEverythingUp = function() {

			self = this;

			var data = {
				"id": formId,
			}
			var ajaxCall = ajaxLib.doItMain(populateFormHandler, data, 'error');

			ajaxCall.done(self.successRetreivingData);
		}


		/**
		 * Callback for treating the ajax call
		 * @param  {object} returnedData [description]
		 */
		_myLibraryObject.successRetreivingData = function(returnedData) {
			if (returnedData.success == 'SUCCESS') {
				formRenderLib.setDataToRender(returnedData.data.formToBuild);
				elementsToParse = formRenderLib.renderNow();
				populateFormLib.setElementsSerialized(returnedData.data.populate);
				populateFormLib.populateNow();
				tokenForFormValidation = returnedData.data.tokenForFormValidation;
				formRenderLib.activateSelectUi();
			}
		};




		return _myLibraryObject;
	}

	// We need that our library is globally accesible, then we save in the window
	if (typeof(window.formLib) === 'undefined') {
		window.formLib = myLibrary();
	}
})(window); // We send the window variable withing our function
