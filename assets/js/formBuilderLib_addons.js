
/**
 * Library used for builing arround the formBuilder Library
 * It was mainly to simplify the elements management on the building page.
 */

(function(window) {
	// You can enable the strict mode commenting the following line
	'use strict';

	function myLibrary() {
		var _myLibraryObject = {};

		var currentData = '[]';
		var currentDataWithGoodNames = '[]';
		var dataOnServer = '[]';
		var dataOnserverWithGoodNames = '[]'; //that's actualy what is on the server
		var pageRedirection;
		var formBuilder;
		var self;
		var selectInspirationRenderingDiv;
		var inspirationGetHandler = '';
		var inspirationPreviewHandler = '';
		var checkInspirationHandler = '';
		var tokenForFormValidation = '';
		var dataBeingPreviewed;

		_myLibraryObject.setInspirationGetHandler = function(handler) {
			inspirationGetHandler = handler;
		}

		_myLibraryObject.setInspirationPreviewHandler = function(handler) {
			inspirationPreviewHandler = handler;
		}

		_myLibraryObject.setSelectInspirationRenderingDiv = function(divId) {
			selectInspirationRenderingDiv = divId;
		}

		_myLibraryObject.setCheckInspirationHandler = function(handler) {
			checkInspirationHandler = handler;
		}

		var dataFromTextArea;



		/**
		 * Ajax call for loading the select for inspiration
		 */
		_myLibraryObject.loadGetInspired = function() {
			self = this;
			var data = {
				"id": "on s'en fiche!",
			}
			var ajaxCall = ajaxLib.doItMain(inspirationGetHandler, data, 'error');

			ajaxCall.done(self.successLoadingGetInspired);
		}


		var elementsToParseForSelect;

		/**
		 * Callback for treating the previous function on success
		 */
		_myLibraryObject.successLoadingGetInspired = function(returnedData) {
			if (returnedData.success == 'SUCCESS') {
				formRenderLib.setRenderingDiv(selectInspirationRenderingDiv);
				formRenderLib.setDataToRender(returnedData.data.formToBuild);
				elementsToParseForSelect = formRenderLib.quickRender();
				populateFormLib.setElementsSerialized(returnedData.data.populate);
				populateFormLib.populateNow();
				tokenForFormValidation = returnedData.data.tokenForFormValidation;
			}
		};


		/**
		 * Function for retreiving the form data for preview from the select
		 * @param  {object} saveResults results of the form containing the select
		 */
		_myLibraryObject.getJsonFromSelect = function(saveResults) {
			self = this;
			var data = {
				"id": 'blabal',
				"dataFromForm": saveResults,
				"tokenForFormValidation": tokenForFormValidation
			};

			var ajaxCall = ajaxLib.doItMain(inspirationPreviewHandler, data, 'error');

			ajaxCall.done(self.successGettingJsonFromSelect);
		}

		/**
		 * Callback for treating the server response from above ajax call
		 * @param  {array} returnedData [description]
		 */
		_myLibraryObject.successGettingJsonFromSelect = function(returnedData) {
			if (returnedData.success == 'SUCCESS') {
				dataBeingPreviewed = returnedData.data.formBuilderData;
				formRenderLib.setDataToRender(dataBeingPreviewed);
				formRenderLib.renderNow(false);
				self.activatePreviewButtons();
			}
		};



		/**
		 * Function for serverCheck the json inputed in the inspiration area
		 */
		_myLibraryObject.checkJSONinspiration = function($data) {
			self = this;
			var data = {
				"json": dataFromTextArea,
			}
			var ajaxCall = ajaxLib.doItMain(checkInspirationHandler, data, 'error');

			ajaxCall.done(self.successLoadingCheckInspiration);
		}

		/**
		 * Callback for treating the server response from above ajax call
		 * @param  {array} returnedData [description]
		 */
		_myLibraryObject.successLoadingCheckInspiration = function(returnedData) {
			if (returnedData.success == 'SUCCESS') {
				formRenderLib.setDataToRender(dataFromTextArea);
				formRenderLib.renderNow(false);
				dataBeingPreviewed = dataFromTextArea;
				self.activatePreviewButtons();
			}
		};


		/**
		 * For activating preview buttons (for adding the element to the form)
		 */
		var activated = false;
		_myLibraryObject.activatePreviewButtons = function() {
			if (activated === false) {
				$('#btns-preview').toggleClass("displayNone", false);
				activated = true;
			}
		}


		///////////////////////////////:
		///////////////////////////////:
		///////////////////////////////: Block of interactions rules

		_myLibraryObject.setInteractions = function() {
			self = this;

			document.getElementById('getInspired').addEventListener('click', function() {
				formRenderLib.setRenderingDiv('previewInspiration');
				$('#builderDiv').toggleClass("displayNone", true);
				$('#inspiration').toggleClass("displayNone", false);
				$('#goBackToEdition').toggleClass("displayNone", false);
			}); //End click on save button



			document.getElementById('previewFromSelect').addEventListener('click', function() {
				var tmp;
				if ($('#form-quickPreview').parsley().validate()) {
					console.log("elementsToParseavant", elementsToParseForSelect);

					tmp = saveFormLib.saveNow(elementsToParseForSelect);
					console.log("elementsToParseaprès", elementsToParseForSelect);
					self.getJsonFromSelect(tmp);
				}
			}); //End click on save button

			document.getElementById('previewFromTextArea').addEventListener('click', function() {
				dataFromTextArea = $('#inputJSON').val();
				try {
					dataFromTextArea = JSON.parse(dataFromTextArea);
				} catch (e) { // non-standard
					notifLib.error("Impossible de déchiffrer le texte JSON");
				} finally {
					self.checkJSONinspiration(dataFromTextArea);
				}


			}); //End click on save button




			document.getElementById('getPreview').addEventListener('click', function() {
				formRenderLib.setRenderingDiv('previewBuilder');
				var dataFromBuilder = formBuilderLib.preview();
				formRenderLib.setDataToRender(JSON.parse(dataFromBuilder));
				formRenderLib.renderNow(false);

				$('#builderDiv').toggleClass("displayNone", true);
				$('#previewBuilder').toggleClass("displayNone", false);
				$('#goBackToEdition').toggleClass("displayNone", false);

				//	formBuilderLib.saveArchitecture('ajaxSaveFormArchitecture','all','none'); // no redirect yet
			}); //End click on save button

			document.getElementById('buttonGoBackToEdition').addEventListener('click', function() {
				$('#previewBuilder').toggleClass("displayNone", true);
				$('#goBackToEdition').toggleClass("displayNone", true);
				$('#inspiration').toggleClass("displayNone", true);
				$('#builderDiv').toggleClass("displayNone", false);
			});


			document.getElementById('loadFormBuilderData').addEventListener('click', function() {
				$('#inputJSON').val(formBuilderLib.preview());
			});

			document.getElementById('addBegining').addEventListener('click', function() {
				askconfirmation("Vous vous apprêtez à ajouter la totalité du formulaire ci-dessous au début du formulaire en construction, confirmez-vous ?", 'begin')
			});

			document.getElementById('addEnd').addEventListener('click', function() {
				askconfirmation("Vous vous apprêtez à ajouter la totalité du formulaire ci-dessous à la fin du formulaire en construction, confirmez-vous ?", 'end')
			});

			document.getElementById('addReplace').addEventListener('click', function() {
				askconfirmation("Vous vous apprêtez à remplacer la totalité du formulaire en construction par celui-ci, confirmez-vous ?", 'reset')
			});

			function askconfirmation(messageInfo, action) {
				bootbox.confirm({
					message: messageInfo,
					buttons: {
						confirm: {
							label: 'Oui',
							className: 'btn-warning'
						},
						cancel: {
							label: 'Non',
							className: 'btn-primary'
						}
					},
					callback: function(result) {
						if (result == true) {
							modifyFormBuilderData(action);
						}
					}
				});
			};

			function modifyFormBuilderData(action) {
				var currentData = JSON.parse(formBuilderLib.preview());
				var tmp = dataBeingPreviewed;
				if (action === 'begin') {
					for (var i = 0; i < currentData.length; i++) {
						tmp.push(currentData[i]);
					}
				} else if (action === 'end') {
					for (var i = 0; i < tmp.length; i++) {
						currentData.push(tmp[i]);
					}
					tmp = currentData;
				} else if (action === 'reset') {
					//rien
				}
				formBuilderLib.updateFormBuilderContent(JSON.stringify(tmp));
				notifLib.success("");
			}

		}




		return _myLibraryObject;
	}

	// We need that our library is globally accesible, then we save in the window
	if (typeof(window.formBuilderLib_addons) === 'undefined') {
		window.formBuilderLib_addons = myLibrary();
	}
})(window); // We send the window variable withing our function
