# Changelog


# Beta
v1.0.0    |		Fri  1 Sep, 2017 		|		Service is out of beta, moved to SIMDE 
v0.9c 		|   Mon 28 Aug, 2017 		|   Translation corrections and slight modifications
v0.9b			| 	Thu 17 Aug, 2017 		| 	Bug correction in the Administrate controller
v0.9 	   	| 	Wed 16 Aug, 2017 		| 	**Opening the beta of this service.**
