# Documentation : Table of Content

---
## The file and folder organization is explained [here](./doc_files/folderOrganisation.md).

## A description of the architecture is available [here](./doc_files/architecture.md)

## Some talk about the database is available [here](./doc_files/database.md).

## A simple tutorial for installation is available [here](./doc_files/install.md).

## Most of the *outside* projects used are listed [here](./doc_files/otherProjectUsed.md).

---
### A todo list is available [here](./todo.md).
