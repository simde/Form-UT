# Install tutorial

1. Copy the whole repo on your server (PHP ≥ 7.0 is needed).
- Setup the database and run `crea_database.sql` file.
- Configure the config files of Codeigniter (/application/config). This concerns the `*.sample` files.
- Mess up with `.htaccess` files and *stackoverflow* for making it works.
