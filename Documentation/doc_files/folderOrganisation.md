# Description of how the (important) files are organized for this project

This description follows the structure of the repo.

## `application`

This is the folder containing all the Codeigniter and backend related stuff.
- `assets` : this is the folder for all *outside* PHP libraries
- `config` : this folder contains all the Codeigniter/libraries realted config files. __All the `.expample` files have to be changed depending on your installation !__
- `controllers` : this folder contains all the Codeigniter controllers, they are the main entry from the web.
- `core` : this folder contains *overloaded* Codeigniter PHP class : mainly for easy handling of restrective access, Ajax and SQL queries.
- `libraries` : this folder contains all the PHP libraries developped for this project and few others copied from the web (`CAS` and a bit of `PGPlib`).
- `models` : this folder contains every database related functions.
- `views` : this folder contains all the views used for this project, they are "template ready". If you want to create a new template, duplicate the default folder and change the template name in the `layouts.php` library.

## `assets`
This folder holds the assets for the frontend:
- `css` : main css files for the site.
- `js` : all the js libs developped for this project.
- `lang` : some language files for `node_modules` translation.

## `Codeigniter_stuff`
Well some stuff related to Codeigniter.

## `Documentation`
This folder contains all the documentaiton related files.

## `node_modules`
This is the folder storing the **NUMEROUS** js libs tacken from the open-source world for building this awesome project.
