# Element used in the form-UT project


## Core backend elements

### Main elements
- `CodeIgniter` v3.1.5
 - https://codeigniter.com/
 - MIT License
 - Reason for this choice : really simple PHP framework to use.


- `postgreSQL` v9.6
 - https://www.postgresql.org/
 - PostgreSQL License
 - Reason for this choice : ...


### Securtity related
- `phpCAS` v1.3.5
 - https://wiki.jasig.org/display/CASC/phpCAS
 - Apache License 2.0
 - Reason for this choice : essential library for **CAS** authentication.


- `codeIgniter cas library` (11th commit)
 - https://github.com/eliasdorneles/code-igniter-cas-library
 - MIT License
 - Reason for this choice : easy implementation for making `phpCAS` working with codeIngniter.
 - *Modifications* : Modification of file `Cas.php`.

From :

```PHP
phpCAS::client(CAS_VERSION_2_0, $cas_url['host'],
	$cas_url['port'], $cas_url['path']);
```

To :

```PHP
	if(isset($_SESSION))
				phpCAS::client(CAS_VERSION_2_0, $cas_url['host'],
					$cas_url['port'], $cas_url['path'],false);
		    else
					phpCAS::client(CAS_VERSION_2_0, $cas_url['host'],
						$cas_url['port'], $cas_url['path']);
```

For better session handling.


- `WP PGP Encrypted Emails` v0.6.3
 - https://wordpress.org/plugins/wp-pgp-encrypted-emails/
 - GNU General Public License v3.0
 - Reason for this choice : easy to understand lib implementing PGP manipulations. Few files were taken and modify from it for this project.


- `openpgp-php` v0.3.0
 - https://github.com/singpolyma/openpgp-php
 - The Unlicense
 - Reason for this choice : it is used by the above project. It's also a full implementation of PGP in PHP no need for console calls !




## Core frontend elements
- `formBuilder` v2.5.3
 - https://formbuilder.online/
 - LIT License
 - Reason for this choice : easy to configure form creation interface. A growing community and a very easy to understand form data structure.


- `parsleyjs` v2.7.2
 - http://parsleyjs.org/
 - MIT License
 - Reason for this choice : best in class client side form validation and easy to use.


- `bootstrap` v3.3.7
 - http://getbootstrap.com/
 - MIT License
 - Reason for this choice : easy to use and appealing results !


## Other frontend modules
You can have a look to the folder `node_modules` for a complete list, I won't cite the dependances here.

- `jquery` v3.2.1
 - https://jquery.com/
 - MIT License
 - Reason for this choice : needed for many other extensions used...


- `bootstrap-select` v1.12.4
 - https://silviomoreto.github.io/bootstrap-select/
 - MIT License
 - Reason for this choice : easy to use, good render, and more fonctionnality than with traditionnal selects.


- `bootstrap-table` v1.11.1
 - https://github.com/wenzhixin/bootstrap-table
 - MIT License
 - Reason for this choice : easy to use, a lot of plugins for cool fonctionnalities (**export**).


- `bootstrap-notify` v3.1.3
 - https://github.com/mouse0270/bootstrap-notify
 - MIT License
 - Reason for this choice : simple to use, good render.



- `font-awesome` v4.7
 - http://fontawesome.io/
 - MIT License
 - Reason for this choice : ...


- `markdown-js` v0.5.0
 - https://github.com/evilstreak/markdown-js
 - MIT License
 - Reason for this choice : one of the few lib that enables markdown on the client side, easy to use !


- `js-xss` v0.3.3
 - https://github.com/leizongmin/js-xss
 - MIT License
 - Reason for this choice : one of the few lib that enables xss-filtering on the client side. This is used when dealing with markdown data.


- `nanobar` v0.4.2
 - https://github.com/jacoborus/nanobar
 - MIT License
 - Reason for this choice : simple to use, light-weigth and good render.
