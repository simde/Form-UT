<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Controller for the homepage of the site
 */
class Welcome extends CI_Controller
{

    /**
     * Index Page for this controller. Nothing crazy.
     */
    public function index()
    {
        if (!$this->session->userdata('loggedIn')) {
            $this->load->view('template/default/views/welcome_NotLoggedIn');
        } else {
					$this->layouts->setTitle('Accueil')
												->addView('welcome_message')
												->display();
        }
        //$this->load->view('welcome_message');
    }

		/**
		 * Simple contact page
		 */
		public function contact()
		{
			$this->layouts->setTitle("Nous contacter")
										->addView('general/contact')
										->display();
		}


		/**
		 * simple error page info when missing temp data
		 */
		public function errorTempDataMissing()
		{
			$this->layouts->setTitle("Erreure...")
										->addView('errors/tempDataMissing')
										->display();
		}



		/**
		 * simple error page when the user is trying to do something forbidden
		 */
		public function errorNoSupport()
		{
			$this->layouts->setTitle("Erreure...")
										->addView('errors/noSupport')
										->display();
		}


		/**
		 * simple error page info when sql erros (erros can be displayed)
		 */
		public function errorSQL()
		{
			$params = array("message" => $this->session->SQLerror);
			$this->layouts->setTitle("Erreure...")
										->addView('errors/sql',$params)
										->display();
			$this->session->unset_userdata('SQLerror');
		}

		public function errorFormAccess()
		{
			$this->layouts->setTitle("Erreure d'accès...")
										->addView('errors/cantAccessForm')
										->display();
		}



}
