<?php


		/**
		 * Controller used as an interface for login and logout.
		 * File inspired by : https://github.com/bradtraversy/ciblog
		 */
    class Users extends CI_Controller
    {


        /**
         * Function to log the user
         * If it is the first time a user login, it is created in the database.
         * Few user-datas are stored in a session.
         *
         * @return void
         */
        public function login()
        {
						// Login using CAS
            $this->load->library('cas');
            $this->cas->force_auth();
            $user = $this->cas->user();


						// Some cas account may not be "real"
						if (!isset($user->attributes['mail'])){
							redirect('Welcome/errorTempDataMissing','location');
							return;
						}

						// Gathering of relevant information
            $userName = $user->userlogin;
            $userDisplayName = $user->attributes['displayName'];
            $userAccountProfile = $user->attributes['accountProfile'];
						$userEmail = $user->attributes['mail'];

						// We generate a random string on logging so we can easily log errors.
						$userErrorCode = self::generateRandomString(50);
            // Create session
            $userData = array(
                'userName' => $userName,
                'userDisplayName' => $userDisplayName,
                'userEmail' => $userEmail,
								'userAccountProfile' => $userAccountProfile,
                'loggedIn' => true,
								'userErrorCode' => $userErrorCode
            );
            $this->session->set_userdata($userData);


            // Set message
            $this->session->set_flashdata('user_loggedin', 'You are now logged in');

						// Databese manipulation
						$this->load->model('User_model');
						$this->User_model->loginOrUpdate();

						$this->load->model('Form_model');
						$result = $this->Form_model->cleanTmpForms();

						$this->load->model('Group_model');
						$this->Group_model->cleanGroups();
						// redirect to a specific page if needed :
						if ($this->session->has_userdata('pageRedirect')) {
							$pageRedirect = $this->session->pageRedirect;
							$this->session->unset_userdata->pageRedirect;
						} else {
							$pageRedirect = 'welcome/index';
						}

            redirect($pageRedirect,'location');
        }


        /**
         * log user out
         * @return void
         */
        public function logout()
        {
						$userIDinDB = $this->session->userIDinDB;
						$this->load->model('Form_model');
						$result = $this->Form_model->cleanTmpForms();

						$this->load->model('Group_model');
						$this->Group_model->cleanGroups();

            $this->session->sess_destroy();

            $this->session->set_userdata(array('logged_in' => false));

            // Set message
            $this->session->set_flashdata('user_loggedout', 'You are now logged out');

            self::casLogout();
        }

				/**
				 * Log out from the CAS.
				 * @return void
				 */
				public function casLogout()
				{
					$this->load->library('cas');
					$this->cas->logout();
				}

				/**
				 * Generates a random string
				 *
				 * @param integer $length length of the string ti generate

				 * @return string
				 */
				private function generateRandomString($length = 10)
				{
						$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
						$charactersLength = strlen($characters);
						$randomString = '';
						for ($i = 0; $i < $length; $i++) {
								$randomString .= $characters[rand(0, $charactersLength - 1)];
						}
						return $randomString;
				}
}
