<?php

include_once(APPPATH . 'core/My_Controller_with_login.php');

/**
 * This the controller used for groups manipulations.
 * It's access is limited to connected user.
 */
class Groups extends My_Controller_with_login
{
	/**
	 * This paramater has to be redefined so that ajax works correctly
	 */
		protected $className ="Groups";

		/**
		 * Ajax redirector for this controller
		 * @param  string $redirect What function should treat the request.
		 * @param  JSONarray $data   The data sent to the server
		 * @return function call
		 */
		protected function ajaxRedirector($redirect,$data)
		// override the function in My controller with login
		{
			if ($redirect == 'ajaxSaveGroup'){
				return self::ajaxSaveGroup($data);
			} elseif ($redirect == 'ajaxLoadGroup'){
				return self::ajaxLoadGroup($data);
			}
			return $this->JSONerror("No function fond for treating the request");
		}


		/**
		 * Checks if the user is the owner of the group
		 * @param  int  $groupId  groupid
		 * @param  boolean $redirect [description]
		 * @return array | bool       group info is access is granted, false if not
		 */
		private function checkOwnerGroup($groupId, $redirect = false)
		{
			$groupId = intval($groupId);

			$this->load->model('Group_model');

			// We have to check if the form is a draft and belongs to the user
			$groupTitleAndActive = $this->Group_model->isUserOwner($groupId);
			if($groupTitleAndActive === $this->session->userErrorCode){
				if ($redirect === true) {
					self::errorRedirector('Welcome/errorSQL');
				} else {return false;}

			}

			return $groupTitleAndActive;
		}



		/**
		 * Checks if a user belongs to a group
		 * @param  int  $groupId  groupid
		 * @return bool         answer
		 */
		private function checkBelongToGroup($groupId)
		{
			$groupId = intval($groupId);

			if (self::checkOwnerGroup($groupId) !== false ) return true;

			$this->load->model('Group_model');

			// We have to check if the form is a draft and belongs to the user
			$test = $this->Group_model->isUserInsideGroup($groupId);
			if($test === $this->session->userErrorCode){
					self::errorRedirector('Welcome/errorSQL');
			}

			return $test;
		}


		/**
		 * Simple lists of the groups of the user
		 */
    public function index()
    {
			$this->load->model('Group_model');

			$yourGroups = $this->Group_model->getYourGroups();
			if($yourGroups === $this->session->userErrorCode){
				self::errorRedirector('Welcome/errorSQL');
			}

			$groupsYouBelongTo = $this->Group_model->getTheGroupsYouBelongTo();
			if($groupsYouBelongTo === $this->session->userErrorCode){
				self::errorRedirector('Welcome/errorSQL');
			}

			if (count($yourGroups)===0){
				$yourGroupsOutput = "Vous n'avez créé aucun groupe";
			} else {

				$yourGroupsOutput =  '<ul>';
				foreach ($yourGroups as $group) {
					if ($group->active === true){
						 $active = '(groupe actif <i class="fa fa-check" style="color:green"></i>)';
					} else {
						$active = '(groupe inactif <i class="fa fa-times" style="color:red"></i>)';
					}
					$url = base_url("Groups/editGroup/$group->groupid");
					$yourGroupsOutput.= '<li>  <a href="'.$url.'">'.htmlentities($group->title)."</a> $active</li>";
				}
				$yourGroupsOutput .= '</ul>';
			}


			if (count($groupsYouBelongTo)===0){
				$groupsYouBelongToOutput = "Vous n'appartenez à aucun groupe créé par d'autres.";
			} else {

				$groupsYouBelongToOutput =  '<ul>';
				foreach ($groupsYouBelongTo as $group) {
					if ($group->active === true){
						 $active = '(groupe actif <i class="fa fa-check" style="color:green"></i>)';
					} else {
						$active = '(groupe inactif <i class="fa fa-times" style="color:red"></i>)';
					}
					$url = base_url("Groups/previewGroup/$group->groupid");
					$groupsYouBelongToOutput.= '<li>  <a href="'.$url.'">'.htmlentities($group->title)."</a> $active</li>";
				}
				$groupsYouBelongToOutput .= '</ul>';
			}

			$param = array(
				'yourGroups'=>$yourGroupsOutput,
				'groupsYouBelongTo'=>$groupsYouBelongToOutput
			);

			$this->layouts->setTitle("Gestion de groupes")
										->addView('groups/groupIndex',$param)
										->display();

    }



		/**
		 * Preview a specific group infos
		 * @param  int  $groupId  groupid
		 */
		public function previewGroup($groupId)
		{
				$groupId = intval($groupId);
				/////////////////
				///////////////// security checks
				/////////////////
				$test = self::checkBelongToGroup($groupId);

				if ($test === false) {
					self::errorRedirector('Welcome/errorNoSupport');
				}

				$this->load->model('Group_model');
				$groupInfo = $this->Group_model->getGroupInfo($groupId);
				/////////////////
				/////////////////
				/////////////////
				if($groupInfo === $this->session->userErrorCode){
					self::errorRedirector('Welcome/errorSQL');
				}

				$tmp = $this->Group_model->listParticipantLogin($groupId);
				if($tmp === $this->session->userErrorCode){
					self::errorRedirector('Welcome/errorSQL');
				}

				$participants =  '<ul>';
				foreach ($tmp as $pers) {
					$participants.= '<li>'.$pers->login.'</li>';
				}
				$participants .= '</ul>';

				$title = $groupInfo[0]->title;
				$state = $groupInfo[0]->active;
				$creator = $groupInfo[0]->login;

				if ($state){
					$state = "Le groupe est actif.";
				} else {
					$state = "Le groupe a été désactivé par son créateur";
				}

				$param = array(
					'title'=>$title,
					'state'=>$state,
					'creator'=>$creator,
					'participants' => $participants,
				);

				$this->layouts->setTitle("Prévisualisation du groupe ".htmlentities($title))
											->addView('groups/groupPreview',$param)
											->display();

		}


		/**
		 * Init group creation
		 */
		public function initGroup()
		{
				// We have to create a new form
				$this->load->model('Group_model');
				$grouId = $this->Group_model->createGroup();

				// check
				if($grouId === $this->session->userErrorCode){
					self::errorRedirector('Welcome/errorSQL');
				}

				$url = 'Groups/editGroup/'.$grouId;
				redirect($url,'location');
		}


		/**
		 * Edit a group
		 * @param  int  $groupId  groupid
		 */
		public function editGroup($groupId){
			$groupId = intval($groupId);
			/////////////////
			///////////////// security checks
			/////////////////
			if ($groupId === 0) {
				self::errorRedirector('Welcome/errorNoSupport');
			}
			/////////////////
			/////////////////


			$title = self::checkOwnerGroup($groupId,true);


			$param = array(
				'id'=>$groupId,
				'ajaxToken'=>$this->generateAjaxToken(),
				'ajaxLocation'=>self::ajaxLocation(),
				'mainSavingFormHandler' => 'ajaxSaveGroup',
				'populateFormHandler' => 'ajaxLoadGroup',
			);

			$this->layouts->setTitle("Edition d'un groupe")
										->addView('groups/editGroup')
										->addView('forms/scripts_formLibs',array(),'bottom')
										->addView('groups/script_editGroup',$param,'bottom')
										->display();

		}



		/**
		 * Function to generate the formdata for group edition
		 * @param  array $data data sent by the user
		 * @return JSON   Data to build the form
		 */
		private function ajaxLoadGroup($data)
		{
			$groupId = intval($data["id"]);
			/////////////////
			///////////////// security checks
			/////////////////

			$tmp = self::checkOwnerGroup($groupId,false); //also checks if the user is the creator

			if ($tmp === false){
				$this->JSONerror("You cannot modify this Group, well tried...",false);
			}
			/////////////////
			/////////////////

			$title = $tmp['title'];
			$active = $tmp['active'];


			$this->load->model('Group_model');
			$tmp = $this->Group_model->listParticipantLogin($groupId);

			if ($tmp === $this->session->userErrorCode){
				return $this->JSONerror("SQL Error, please contact the admin.");
			}

			$participants = '';



			foreach ($tmp as $pers) {
				$participants.= $pers->login.' ; ';
			}

			$this->load->library('FormBuilder');
			$data = $this->formbuilder->text("title")
																->setFieldLabel("Intitulé du groupe :")
																->setMaxLength("200")
																->setFieldPlaceholder("Inscrire l'intitulé ici...")
																->setFieldValue(htmlentities($title))
																->closeField()
																->textarea("Participants",false)
																->setMaxLength("20000")
																->setFieldLabel("Liste des logins des personnes présentes dans le groupe :")
																->setFieldPlaceholder("Veiller à séparer les logins par un caractère non alphabétique : ';' ou retour à la ligne par exemple.")
																->setFieldValue(htmlentities($participants))
																->closeField()
																->radio("active")
																->setFieldLabel("Le groupe est-il actif ?")
																->addRadioOption("Oui, le groupe est actif.",'active',$active)
																->addRadioOption("Non, le groupe n'est pas actif",'notActive',!$active)
																->closeField()
																->getFormAndPopulate();

			return $this->JSONsuccess($data);
		}


		/**
		 * Function to save the groups infos
		 * (treatments are applied)
		 * @param  array $data data sent by the user
		 */
		private function ajaxSaveGroup($data)
		{
				$dataFromForm = $data["dataFromForm"];
				$results = $dataFromForm["results"];
				$title = $results["title"];
				$participants = $results["Participants"];
				$groupId = intval($data["id"]);
				$active = $results['active'] === 'active';
				/////////////////
				///////////////// security checks
				/////////////////
				$test = self::checkOwnerGroup($groupId,false); //also checks if the user is the creator

				if ($test === false){
					$this->JSONerror("You cannot modify this Group, well tried...",false);
				}
				/////////////////
				/////////////////

				$test = $this->validateReturnedFormData($data);
				if ($test !== true){
					return $this->JSONerror($test);
				}

				$participants = self::cleanLogins($participants);
				$this->load->model('Group_model');


				$test = $this->Group_model->makeSureLoginsAreInDb($participants);
				if ($test === $this->session->userErrorCode){
					return $this->JSONerror("SQL Error during login insertion, please contact the admin.");
				}

				$participantsIds = array();
				$this->load->model('User_model');

				foreach ($participants as $login) {
					$tmp = $this->User_model->isUserInsideDatabase($login);
					if ($tmp !== false){
							$participantsIds[] = $tmp['id'];
					}
				}

				$test = $this->Group_model->updateGroup($groupId,$title,$participantsIds,$active);
				if ($test === $this->session->userErrorCode){
					return $this->JSONerror("SQL Error during final stage of group update, please contact the admin.");
				} else {
					return $this->JSONsuccess();
				}

		}


		/**
		 * Simple function to clean logins lists used in ajaxSaveGroup
		 * @param  string $string List of logins that were inputed
		 */
		private function cleanLogins($string){
			$re = '/[^a-zA-Z]*([a-zA-Z]+)/';
			preg_match_all($re, $string, $matches,  PREG_PATTERN_ORDER);

			$matches = $matches[1];

			$res = array();
			foreach ($matches as $match) {
				$res[] = strtolower($match);
			}

			return $res;
		}



}
