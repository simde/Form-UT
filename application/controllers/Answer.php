<?php

include_once(APPPATH . 'core/My_Controller_with_login.php');

/**
 * This the controller used answering forms
 * It's access is limited to connected user.
 */
class Answer extends My_Controller_with_login
{
		/**
		 * This paramater has to be redefined so that ajax works correctly
		 */
		protected $className ="Answer";

		/**
		 * Ajax redirector for this controller
		 * @param  string $redirect What function should treat the request.
		 * @param  JSONarray $data   The data sent to the server
		 * @return function call
		 */
		protected function ajaxRedirector($redirect,$data)
		// override the function in My controller with login
		{
			if ($redirect == 'ajaxPreviewForm'){
				return self::ajaxPreviewForm($data);
			} elseif ($redirect == 'ajaxGetInitForm'){
				return self::ajaxGetInitForm($data);
			} elseif ($redirect == 'ajaxSaveInitForm'){
				return self::ajaxSaveInitForm($data);
			} elseif ($redirect == 'ajaxGetEditForm'){
				return self::ajaxGetEditForm($data);
			} elseif ($redirect == 'ajaxSimpleSaveEditForm'){
				return self::ajaxSimpleSaveEditForm($data);
			} elseif ($redirect == 'ajaxMainSaveEditForm'){
				return self::ajaxMainSaveEditForm($data);
			}elseif ($redirect == 'ajaxFunPGP'){
				return self::ajaxFunPGP($data);
			}
			return $this->JSONerror("No function fond for treating the request");
		}

		/**
		 * Simple redirection of the index, just in case.
		 */
		public function index(){
			redirect('Answer/list','location');
		}


		/**
		 * Checks if the forms can be answered by a user
		 * @param  int  $formId   The Id of the form
		 * @return boolean | array     False if can't access, or the the forms info if access is granted
		 */
		private function checkHasAccess($formId){
			$this->load->model('Answer_Model');
			$formInfo = $this->Answer_Model->canAccessForm($formId);

			if($formInfo === $this->session->userErrorCode){
				self::errorRedirector('Welcome/errorFormAccess');
				return false;
			}  else {
				return $formInfo;
			}
		}


		/**
		 * Checks if the user has already answered a form
		 * @param  int  $formId Id of the form
		 * @return boolean     Answers the question
		 */
		private function hasAnsweredForm($formId){
			$this->load->model('Answer_Model');
			return $this->Answer_Model->hasAnsweredForm($formId);
		}


		/**
		 * Public function to list all forms that can be answered by the user.
		 */
		public function list()
		{
			$this->load->model('Answer_Model');

			$formsYouCanAnswer = $this->Answer_Model->getTheFormsYouCanAnswer();
			if($formsYouCanAnswer === $this->session->userErrorCode){
				self::errorRedirector('Welcome/errorSQL');
			}

			// We get an array of (formID => true) whith all the formId of the forms that have been answered
			$formsYouHaveAnswered = $this->Answer_Model->formsYouHaveAnswered();
			if($formsYouHaveAnswered === $this->session->userErrorCode){
				self::errorRedirector('Welcome/errorSQL');
			}

			$yourForms = $formsYouCanAnswer["yours"];
			$othersForms = $formsYouCanAnswer["others"];

			$this->load->library("tools");

			if (count($yourForms)===0){
				$yourFormsOutput = "Vous n'avez créé aucun formulaire";
			} else {

				$yourFormsOutput =  '<ul>';
				foreach ($yourForms as $form) {
					$title = htmlentities($form->title);
					$formId = $form->formid;
					$closeDate = $this->tools->timestampToDate($form->closedate);


					$info = '<i class="fa fa-times" style="color:red"></i>';

					if (array_key_exists($formId,$formsYouHaveAnswered)){
						// on a commencé la procédure
						if (is_null($formsYouHaveAnswered[$formId])){
							$info = '<i class="fa fa-question" aria-hidden="true"></i><span class="fa-stack fa-lg">
  								<i class="fa fa-user-o  fa-stack-1x"></i>
  								<i class="fa fa-ban fa-stack-1x text-danger"></i>
									</span>';
						} else if($formsYouHaveAnswered[$formId] === true) {
							$info = '<i class="fa fa-exclamation-triangle" style="color:orange;" aria-hidden="true"></i><i class="fa fa-user-o"></i>';
						} else if($formsYouHaveAnswered[$formId] === false){
							$info = '<i class="fa fa-check" style="color:green"></i><i class="fa fa-user-o"></i>';
						}
					}

					if ($form->open === true){
						$url = base_url("Answer/form/$formId");
						$yourFormsOutput.= '<li> '.$info.' <a href="'.$url.'">'.$title."</a> (Formulaire ouvert jusqu'au $closeDate)</li>";
					} else {
						$yourFormsOutput .= "<li> $info $title (formulaire clos)</li>";
					}
				}
				$yourFormsOutput .= '</ul>';
			}

			if (count($othersForms)===0){
				$othersFormsOutput = "Vous ne pouvez accéder à aucun formulaire créé par d'autres personnes.";
			} else {

				$othersFormsOutput =  '<ul>';
				foreach ($othersForms as $form) {
					$title = htmlentities($form->title);
					$formId = $form->formid;
					$closeDate = $this->tools->timestampToDate($form->closedate);
					$login = $form->login;


					$info = '<i class="fa fa-times" style="color:red"></i>';


					if (isset($formsYouHaveAnswered[$formId])){
						// on a commencé la procédure
						if ($formsYouHaveAnswered[$formId] === NULL){
							$info = '<i class="fa fa-question" aria-hidden="true"></i><span class="fa-stack fa-lg">
									<i class="fa fa-user-o  fa-stack-1x"></i>
									<i class="fa fa-ban fa-stack-1x text-danger"></i>
									</span>';
						} else if($formsYouHaveAnswered[$formId] === true) {
							$info = '<i class="fa fa-exclamation-triangle" style="color:orange;" aria-hidden="true"></i><i class="fa fa-user-o"></i>';
						} else if($formsYouHaveAnswered[$formId] === false){
							$info = '<i class="fa fa-check" style="color:green"></i><i class="fa fa-user-o"></i>';
						}
					}

					if ($form->open === true){
						$url = base_url("Answer/$formId");
						$othersFormsOutput.= '<li> '.$info.' <a href="'.$url.'">'.$title."</a> (Formulaire ouvert jusqu'au $closeDate)</li>";
					} else {
						$othersFormsOutput .= "<li> $info $title (formulaire clos)</li>";
					}
				}
				$othersFormsOutput .= '</ul>';
			}


			$param = array(
				'yourForms'=>$yourFormsOutput,
				'otherForms'=>$othersFormsOutput
			);

			$this->layouts->setTitle("Répondre à un formulaire")
										->addView('answer/list',$param)
										->display();

		}

		/**
		 * Function to answer a specific form
		 * @param  string|int $formId id of the form to answer
		 */
		public function form($formId)
		{
				/////////////////
				///////////////// security checks
				/////////////////
				$formId = intval($formId);
				if ($formId === 0) {
					return redirect('Welcome/errorNoSupport','location');
				}

				$formInfo = self::checkHasAccess($formId);
				if ($formInfo === false) {
					return redirect('Welcome/errorFormAccess','location');
				} else {
					$this->session->set_userdata("Access$formId",true); // the user has access to the form
				}

				///////////////
				/////////////// Smart redirection
				///////////////
				$submissionId = self::hasAnsweredForm($formId);

				if ($submissionId === $this->session->userErrorCode) {
					$this->session->unset_userdata("Access$formId");
					return redirect('Welcome/errorSQL','location');
				} else if ($submissionId !== false){ // The user has already answred or started a form submission so we can squeeze the init stage
					redirect("Answer/getAccess/$formId",'location');
				} else { // The person hasn't responded yet
					redirect("Answer/init/$formId",'location');
				}
		}



		/**
		 * Creation of a formSubmission
		 * @param  string|int $formId the form to answer
		 */
		public function init($formId){
			/////////////////
			///////////////// security checks
			/////////////////
			$formId = intval($formId);
			if ($this->session->userdata("Access$formId") === NULL) {
				return redirect('Welcome/errorNoSupport','location');
			} // We can have access to this step

			$submissionId = $this->session->userdata("edit$formId");
			if ($submissionId !== NULL){
				return redirect('Welcome/errorNoSupport','location'); //can(t go back)
			}

			/////////////////
			/////////////////
			/////////////////

			$this->load->model('Answer_Model');
			$formInfo = $this->Answer_Model->getFormInfo($formId);
			if ($formInfo === false OR $formInfo === $this->session->userErrorCode){
				return redirect('Welcome/errorSQL','location'); //can(t go back)
			}

			$title = $formInfo->title;

			$param1 = array(
				'title'=>$title
			);

			$param2 = array(
				'id'=>$formId,
				'ajaxToken'=>$this->generateAjaxToken(),
				'ajaxLocation'=>self::ajaxLocation(),
				'simpleSavingFormHandler' => 'ajaxSaveInitForm',
				'mainSavingFormHandler' => 'ajaxSaveInitForm',
				'populateFormHandler' => 'ajaxGetInitForm',
				'nextPage' => base_url("Answer/getAccess/$formId"),
				'previewHandler' => 'ajaxPreviewForm'
			);

			$this->layouts->setTitle("Réponse à un formulaire")
										->addView('answer/init',$param1)
										->addView('forms/scripts_formLibs',array(),'bottom')
										->addView('answer/script_init',$param2,'bottom')
										->display();

		}



		/**
		 * Function to receive ajax data and generate the init form
		 * @param  array $data data sent by the user
		 * @return JSON   Data to build the form
		 */
		private function ajaxGetInitForm($data)
		{
			/////////////////
			///////////////// security checks
			/////////////////
			$formId = intval($data["id"]);
			if ($this->session->userdata("Access$formId") === NULL) {
				return $this->JSONerror("Id is not valid !!");
			} // We can have access to this step

			/////////////////
			/////////////////
			/////////////////

			$this->load->model('Answer_Model');
			$formInfo = $this->Answer_Model->getFormInfo($formId);
			if ($formInfo === false OR $formInfo === $this->session->userErrorCode){
				return $this->JSONerror("SQL Error, contact the admin");
			}

			$title = $formInfo->title;
			$anonymous = $formInfo->anonymous;
			$personnalInfo = $formInfo->personnalinfo;

			$this->load->library('FormBuilder');
			$data = $this->formbuilder->paragraph("introAnonymat")
																->setFieldLabel("- Concernant le caractère anonyme de votre réponse (cela concerne votre login, nom, prénom et adresse mail) :")
																->closeField();

			if ($anonymous === 2){
				$data = $data->paragraph("ssdf")
									  	->setFieldLabel("Vous n'avez pas le choix : **vous êtes anonyme**.")
											->closeField();
			} else if ($anonymous === 0){
				$data = $data->paragraph("ssdfqsd")
											->setFieldLabel("Vous n'avez pas le choix : **vous n'êtes pas anonyme**.")
											->closeField();
			} else {
				$data = $data->paragraph("ssdfdf")
											->setFieldLabel("Vous avez le choix d'être anonyme ou non.")
											->closeField()
											->radio("anonyme")
											->setFieldLabel("Je fais le choix :")
											->addRadioOption("D'être anonyme.",'2')
											->addRadioOption("De ne pas être anonyme.",'0')
											->closeField();
			}
			$data = $data->paragraph("introDonnéesCas")
									 ->setFieldLabel("- Concernant les informations du CAS (cela concerne votre groupe CAS ainsi que le semestre d'étude pour les étudiant⋅e⋅s).")
									 ->closeField();

			if ($personnalInfo === 2){
				$data = $data->paragraph("ssdff")
											->setFieldLabel("Vous n'avez pas le choix : **ces informations ne seront pas divulguées**.")
											->closeField();
			} else if ($personnalInfo === 0){
				$data = $data->paragraph("ssdfqsddg")
											->setFieldLabel("Vous n'avez pas le choix : **ces informations seront divulguées**.")
											->closeField();
			} else {
				$data = $data->paragraph("ssdfdfhrr")
											->setFieldLabel("Vous avez le choix de les divulguer ou non.")
											->closeField()
											->radio("infoPerso")
											->setFieldLabel("Je fais le choix :")
											->addRadioOption("De ne pas les divulguer.",'2')
											->addRadioOption("De les divulguer.",'0')
											->closeField();
			}

			$data = $data->getFormAndPopulate();

			return $this->JSONsuccess($data);

		}



		/**
		 * Function to receive ajax data and save the init form
		 * @param  array $data data sent by the user
		 * @return JSON       Success or error
		 */
		private function ajaxSaveInitForm($data)
		{

			/////////////////
			///////////////// security checks
			/////////////////

			$formId = intval($data["id"]);
			if ($this->session->userdata("Access$formId") === NULL) {
				return $this->JSONerror("Id is not valid !!");
			} // We can have access to this step

			/////////////////
			/////////////////
			/////////////////


			$this->load->model('Answer_Model');
			$formInfo = $this->Answer_Model->getFormInfo($formId);
			if ($formInfo === false OR $formInfo === $this->session->userErrorCode){
				return $this->JSONerror("SQL Error, contact the admin");
			}

			$anonymous = $formInfo->anonymous;
			$personnalInfo = $formInfo->personnalinfo;


			if ($anonymous !== 0 or $personnalInfo !== 0){
				$test = $this->validateReturnedFormData($data);
				if ($test !== true){
					return $this->JSONerror($test);
				}
				$dataFromForm = $data["dataFromForm"];
				$results = $dataFromForm["results"];
			}


			if ($anonymous === 2 or $anonymous === 0){
				$anonymous = ($anonymous === 2);
			} else {
				$anonyme = $results['anonyme'];
				$anonymous = intval($anonyme) === 2;
			}

			if ($personnalInfo === 2 or $personnalInfo === 0){
				$personnalInfo = ($personnalInfo === 2);
			} else {
				$infoPerso = $results['infoPerso'];
				$personnalInfo = intval($infoPerso) === 2;
			}

			$uid = $this->generateRandomString(100);

			$this->load->model('Answer_Model');
			$submissionId = $this->Answer_Model->initFormSubmission($formId, $uid, $anonymous, $personnalInfo);

			if ($submissionId === $this->session->userErrorCode){
				$this->JSONerror("Erreure pendant l'insertion dans la base de donnée, merci de contacter les administrateurs du site.");
			} else {
				$this->session->set_userdata("FormSubmissionId$formId",$submissionId);
				if ($anonymous === true) $this->session->set_userdata("anonymous$formId",$uid);
				$this->session->set_userdata("edit$formId",$submissionId);
				return $this->JSONsuccess();
			}
		}




		/**
		 * Function to receive ajax data and send back the preview of the form
		 * @param  array $data data sent by the user
		 * @return JSON       formdata
		 */
		private function ajaxPreviewForm($data)
		{

			/////////////////
			///////////////// security checks
			/////////////////
			$formId = $data['id'];
			$formId = intval($formId);

			if ($this->session->userdata("Access$formId") === NULL) {
				return $this->JSONerror("Id is not valid !!");
			} // We can have access to this step
			/////////////////
			/////////////////
			/////////////////

			$this->load->model('FormCreation_model');
			$JSONFormData = $this->FormCreation_model->getJSONformData($formId);
			if($JSONFormData === $this->session->userErrorCode){
				return $this->JSONerror("Erreur lors de l'interrogation de la base de donnée, merci de contacter les administrateurs du site.");
			}

			//if there is nothing...
			if ($JSONFormData === false OR $JSONFormData ==NULL){
				$JSONFormData = array();
			}

			$return = array("formBuilderData" => $JSONFormData);

			return $this->JSONsuccess($return);
		}




		/**
		 * Access check for submissions.
		 * They must have been created befor
		 * @param  string $formId if of the form
		 */
		public function getAccess($formId){
			$formId = intval($formId);

			// if we have just init the form (and therefore have setted edit$formId) we squizz the rest of the function
			$test = $this->session->userdata("edit$formId");
			if ($test !== NULL){
				$this->session->unset_userdata("Access$formId");
				redirect("Answer/edit/$formId",'location');
			}
			/////////////////
			///////////////// security checks
			/////////////////
			if ($this->session->userdata("Access$formId") === NULL) {
				return redirect('Welcome/errorNoSupport','location');
			} // We can have access to this step
			/////////////////
			/////////////////
			/////////////////

			// we now have to check if we have the FormSubmissionId (if not it is because the submission was anonymous)
			$submissionId = self::hasAnsweredForm($formId);
			if ($submissionId === NULL){
				redirect("Answer/anonymousAccess/$formId",'location');
			} else {
				$this->session->set_userdata("edit$formId",$submissionId);
				redirect("Answer/edit/$formId",'location');
			}
		}



		/**
		 * Function to retreive the FormSubmissionId thanks to the uid
		 * No ajax this time, just pure post.
		 * @param  string $formId id of the form to access
		 */
		public function anonymousAccess($formId){
			$formId = intval($formId);
			/////////////////
			///////////////// security checks
			/////////////////
			if ($this->session->userdata("Access$formId") === NULL) {
				return redirect('Welcome/errorNoSupport','location');
			}
			/////////////////
			/////////////////
			/////////////////

			$code = $this->input->post('code');
			if ($code === NULL){
				$this->session->set_userdata('nbessais',3);
			} else {
				$this->load->model('Answer_Model');
				$subId = $this->Answer_Model->checkUid($formId,$code);

				if($subId === $this->session->userErrorCode){
					redirect('Welcome/errorSQL','location');
				} else if ($subId !== false) {// we have found it !
					$this->session->set_userdata("edit$formId",$subId);
					redirect("Answer/getAccess/$formId",'location');
				} else {
					$this->session->set_userdata('nbessais',$this->session->nbessais-1);
					if ($this->session->nbessais === 0) redirect('Users/logout','location');
				}
			}

			$param = array(
				'nbessais'=> $this->session->nbessais,
				'formId'=> $formId
			);

			$this->layouts->setTitle("Accès à un formulaire anonyme")
										->addView('answer/anonymousAccess',$param)
										->display();

		}




		/**
		 * Edition of a formSubmission
		 * @param  string $formId id of the form
		 */
		public function edit($formId){
			$formId = intval($formId);
			/////////////////
			///////////////// security checks
			/////////////////
			$submissionId = $this->session->userdata("edit$formId");
			if ($submissionId === NULL) {
				return redirect('Welcome/errorNoSupport','location');
			}
			/////////////////
			/////////////////
			/////////////////

			// we check if the submission is anonymous to display more stuff
			$anonymous = self::hasAnsweredForm($formId)===NULL;


			$layout = $this->layouts->setTitle("Réponse à un formulaire");

			if($anonymous){
					$this->load->model('Answer_Model');
					$uid = $this->Answer_Model->getuid($submissionId);
					if($uid === $this->session->userErrorCode){
						redirect('Welcome/errorSQL','location');
					}


					$param1 = array('uid'=>$uid,
												'email'=>$this->session->userEmail,
											);

					$param2 = array(
						'id'=>$formId,
						'ajaxToken'=>$this->generateAjaxToken(),
						'ajaxLocation'=>self::ajaxLocation()
					);


					$layout = $layout->setTitle("Réponse anonyme à un formulaire")
												->addView('forms/scripts_formLibs',array(),'bottom')
												->addView('answer/anonymousAnswer',$param1)
												->addView('answer/script_pgp',$param2,'bottom');
			}

			$param3 = array(
				'id'=>$formId,
				'ajaxToken'=>$this->generateAjaxToken(),
				'ajaxLocation'=>self::ajaxLocation(),
				'simpleSavingFormHandler' => 'ajaxSimpleSaveEditForm',
				'mainSavingFormHandler' => 'ajaxMainSaveEditForm',
				'populateFormHandler' => 'ajaxGetEditForm',
				'nextPage' => base_url("Answer/confirm/$formId")
			);

			$formInfo = $this->Answer_Model->getFormInfo($formId);
			if ($formInfo === false OR $formInfo === $this->session->userErrorCode){
				return redirect('Welcome/errorSQL','location'); //can(t go back)
			}

			$title = $formInfo->title;

			$param1 = array(
				'title'=>$title
			);
			$param4 = array(
				'title'=>$title
			);

			$layout = $layout->addView('answer/edit',$param4)
										->addView('forms/scripts_formLibs',array(),'bottom')
										->addView('answer/script_edit',$param3,'bottom')
										->display();

		}




		/**
		 * Function to send the form data to the use
		 *
		 * Form validation actually starts here with a tocken and an array being setted
		 *
		 * @param  array $data data sent by the user
		 * @return JSON    form data
		 */
		private function ajaxGetEditForm($data)
		{
			$formId = $data['id'];
			/////////////////
			///////////////// security checks
			/////////////////
			$formId = intval($formId);
			$submissionId = $this->session->userdata("edit$formId");
			if ($submissionId === NULL) {
				return $this->JSONerror("Id is not valid !!");
			}
			/////////////////
			/////////////////
			/////////////////

			$this->load->model('FormCreation_model');
			$JSONFormData = $this->FormCreation_model->getJSONformData($formId);
			if($JSONFormData === $this->session->userErrorCode){
				return $this->JSONerror("Erreur lors de l'interrogation de la base de donnée, merci de contacter les administrateurs du site.");
			}
			//if there is nothing...
			if ($JSONFormData === false OR $JSONFormData ==NULL){
				$JSONFormData = array();
			}

			$token = $this->generateRandomString(10);
			while ($this->session->userdata($token) !== NULL){
				$token = self::generateRandomString(10);
			}
			// We settup a token for a simple form validation when the data will be sent back
			$this->session->set_userdata($token,$JSONFormData);


			//we get previous answers :
			$this->load->model('Answer_Model');
			$populate = $this->Answer_Model->getFormAnswers($submissionId);
			if($populate === $this->session->userErrorCode or $populate === false){
				return $this->JSONerror("Erreur lors de l'interrogation de la base de donnée, merci de contacter les administrateurs du site.");
			}

			$return = array("formToBuild" => $JSONFormData,"populate"=>$populate,'tokenForFormValidation'=>$token);

			return $this->JSONsuccess($return);
		}


		/**
		 * Function to save the results of formsubmission
		 *
		 * @param  array $data data sent by the user
		 * @return JSON    succes or error
		 */
		private function ajaxSimpleSaveEditForm($data)
		{
				$dataFromForm = $data["dataFromForm"];
				$results = $dataFromForm["results"];
				$answers = $dataFromForm["elementsSerialized"];
				$formId = intval($data["id"]);

				/////////////////
				///////////////// security checks
				/////////////////
				$submissionId = $this->session->userdata("edit$formId");
				if ($submissionId === NULL) {
					return $this->JSONerror("Id is not valid !!");
				}
				/////////////////
				/////////////////
				/////////////////
				/*
				$test = $this->validateReturnedFormData($data);
				if ($test !== true){
					return $this->JSONerror($test);
				}
				*/

				$this->load->model('Answer_Model');
				//$results = json_encode($results);
				//$answers = json_encode($answers);
				if ($this->Answer_Model->updateFormSubmission($submissionId, $results, $answers) === true){
					$this->session->set_userdata("valid$formId",false);

						return $this->JSONsuccess();
				} else {
					return $this->JSONerror("Erreur lors de l'interrogation de la base de donnée, merci de contacter les administrateurs du site.");
				}

		}




		/**
		 * Function to CHECK and save the results of formsubmission
		 *
		 * @param  array $data data sent by the user
		 * @return JSON    succes or error
		 */
		private function ajaxMainSaveEditForm($data)
		{
				$dataFromForm = $data["dataFromForm"];
				$results = $dataFromForm["results"];
				$answers = $dataFromForm["elementsSerialized"];
				$formId = intval($data["id"]);

				/////////////////
				///////////////// security checks
				/////////////////
				$submissionId = $this->session->userdata("edit$formId");
				if ($submissionId === NULL) {
					return $this->JSONerror("Id is not valid !!");
				}
				/////////////////
				/////////////////
				/////////////////

				$test = $this->validateReturnedFormData($data);
				if ($test !== true){
					return $this->JSONerror($test);
				}


				$this->load->model('Answer_Model');
				//$results = json_encode($results);
				//$answers = json_encode($answers);
				if ($this->Answer_Model->updateFormSubmission($submissionId, $results, $answers) === true){
					$this->session->set_userdata("valid$formId",true);
						return $this->JSONsuccess();
				} else {
					return $this->JSONerror("Erreur lors de l'interrogation de la base de donnée, merci de contacter les administrateurs du site.");
				}

		}


		/**
		 * Function for sending the uid with pgp encryption
		 * @param  array $data  data sent by the user
		 */
		private function ajaxFunPGP($data)
		{
			$formId = $data['id'];
			$formId = intval($formId);

			/////////////////
			///////////////// security checks
			/////////////////
			$submissionId = $this->session->userdata("edit$formId");
			if ($submissionId === NULL) {
				return $this->JSONerror("Id is not valid !!");
			}
			/////////////////
			/////////////////
			/////////////////

			$this->load->model('Answer_Model');
			$uid = $this->Answer_Model->getuid($submissionId);
			if($uid === $this->session->userErrorCode){
				return $this->JSONerror("Impossible de retrouver l'identifiant :( ");
			}

			$email = $data['email'];
			$pgpKey = $data['pgpkey'];

			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				return $this->JSONerror("Ce n'est pas une adresse mail valide !");
			}

			if (strlen($pgpKey)===0) {
				return $this->JSONerror("Votre clef pgp est bien courte.");
			}

			$from = 'florent.chehab@etu.utc.fr';

$subject = "Message de form-ut";

$message = "
Bonjour,
Voici l'identifiant pour accéder au formulaire $formId sur form-ut :

$uid

Bonne journée,
L'équipe de Form'UT.
";
			$this->load->library('MailSender');
			$mail = $this->mailsender->SendMail($from, $email, $subject, $message, $pgpKey);
			if ($mail){
				return $this->JSONsuccess();
			} else {
				return $this->JSONerror("Erreure lors de l'envoi du mail.");
			}
		}



		/**
		 * Function to confirm the submission
		 * @param  string $formId id of the form
		 */
		public function confirm($formId){
			$formId = intval($formId);
			/////////////////
			///////////////// security checks
			/////////////////
			$validationStatus = $this->session->userdata("valid$formId");
			if ($validationStatus === false or $validationStatus === NULL){
				return redirect('Welcome/errorNoSupport','location');
			}
			/////////////////
			/////////////////
			/////////////////


			$submissionId = $this->session->userdata("edit$formId");
			$this->load->model('Answer_Model');
			$data = $this->Answer_Model->getFormSubmission($submissionId);

			$this->load->model('FormCreation_model');
			$source = $this->FormCreation_model->getJSONformData($formId);

			if($data === false or $source===false){
				redirect('Welcome/errorNoSupport','location');
			} else if ($data === $this->session->userErrorCode or $source === $this->session->userErrorCode ){
				redirect('Welcome/errorSQL','location');
			}

			$this->load->library('ResultsBuilder');
			$result = $this->resultsbuilder->buildResults($data,$source,false); // false because we don't want the export
			$this->load->helper('security');
			$result=$this->security->xss_clean($result);

			$formInfo = $this->Answer_Model->getFormInfo($formId);
			if ($formInfo === false OR $formInfo === $this->session->userErrorCode){
				return redirect('Welcome/errorSQL','location'); //can(t go back)
			}

			$title = $formInfo->title;

			$param1 = array(
				'title'=>$title,
				'nextPage' => base_url("Answer/success/$formId"),
				'previousPage' => base_url("Answer/edit/$formId"),
				'result' => $result,
			);

			$this->layouts->setTitle("Confirmation de la participation")
										->addView('answer/confirm',$param1)
										->addView('answer/script_confirm',array(),'bottom')
										->display();

		}





		/**
		 * Success function form confirming that the submission has been taken into account
		 * @param  string $formId id of the form
		 */
		public function success($formId){
			$formId = intval($formId);
			/////////////////
			///////////////// security checks
			/////////////////
			$validationStatus = $this->session->userdata("valid$formId");
			if ($validationStatus === false or $validationStatus === NULL){
				return redirect('Welcome/errorNoSupport','location');
			}

			$submissionId = $this->session->userdata("edit$formId");
			if ($submissionId === NULL) {
				redirect('Welcome/errorNoSupport','location');
			}
			/////////////////
			/////////////////
			/////////////////


			$this->load->model('Answer_Model');
			$update = $this->Answer_Model->publish($submissionId);

			$formInfo = $this->Answer_Model->getFormInfo($formId);
			if ($formInfo === false OR $formInfo === $this->session->userErrorCode){
				return redirect('Welcome/errorSQL','location'); //can(t go back)
			}

			$title = $formInfo->title;

			if($update === $this->session->userErrorCode){
				redirect('Welcome/errorSQL','location');
			}  else {


				$param = array(
					'title' => $title,
					'editUrl' => base_url("/Answer/form/$formId")
				);

				$tmp = array(
					"valid$formId",
					"edit$formId",
					"Access$formId",
					"FormSubmissionId$formId",
					"anonymous$formId",
					'nbessais',
				);

				$this->session->unset_userdata($tmp);

					$this->layouts->setTitle("Félicitation")
												->addView('answer/success',$param)
												->display();


			}

		}


}
