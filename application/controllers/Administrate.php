<?php

include_once(APPPATH . 'core/My_Controller_with_login.php');

/**
 * This the controller used for form administration manipulations.
 * It's access is limited to connected user.
 */
class Administrate extends My_Controller_with_login
{
	/**
	 * This paramater has to be redefined so that ajax works correctly
	 */
		protected $className ="Administrate";

		/**
		 * Ajax redirector for this controller
		 * @param  string $redirect What function should treat the request.
		 * @param  JSONarray $data   The data sent to the server
		 * @return function call
		 */
		protected function ajaxRedirector($redirect,$data)
		// override the function in My controller with login
		{
			if ($redirect == 'ajaxGetAdmin'){
				return self::ajaxGetAdmin($data);
			} else if ($redirect == 'ajaxSaveAdmin'){
				return self::ajaxSaveAdmin($data);
			}

			return $this->JSONerror("No function fond for treating the request");
		}



		/**
		 * Function that lists all the forms that the user can administrate or those that he/she hasn't finished to build.
		 */
		public function index(){
			/// lists des formulaires auquels on peut accéder pour les résultats
			$this->load->model('Administrate_Model');

			$tmp = $this->Administrate_Model->administrate();
			if($tmp === $this->session->userErrorCode){
				self::errorRedirector('Welcome/errorSQL');
			}

			$finished = $tmp['finished'];
			$draft = $tmp['draft'];

			if (count($finished)===0){
				$finishedOutput = "Vous n'avez terminé aucun formulaire.";
			} else {

				$finishedOutput =  '<ul>';
				foreach ($finished as $form) {
					$title = htmlentities($form->title);
					$formId = $form->formid;
					$url = base_url("Administrate/admin/$formId");
					$finishedOutput.= '<li> <a href="'.$url.'">'.$title."</a></li>";
				}
				$finishedOutput .= '</ul>';
			}

			if (count($draft)===0){
				$draftOutput = "Vous avez terminé de construire tout vos formulaires.";
			} else {

				$draftOutput =  '<ul>';
				foreach ($draft as $form) {
					$title = htmlentities($form->title);
					$formId = $form->formid;
					$url = base_url("FormCreation/formTitle/$formId");
					$draftOutput.= '<li> <a href="'.$url.'">'.$title."</a></li>";
				}
				$draftOutput .= '</ul>';
			}


			$param = array(
				'finished'=>$finishedOutput,
				'draft'=>$draftOutput
			);

			$this->layouts->setTitle("Administrer un formulaire")
										->addView('administrate/administrate',$param)
										->display();

		}


		/**
		 * Function that lists all the form that the user can see the results of :
		 *  - his/her none-draft forms
		 *  - The forms where he has been granted access to for seeing results.
		 */
		public function results(){
			/// lists des formulaires auquels on peut accéder pour les résultats
			$this->load->model('Administrate_Model');

			$tmp = $this->Administrate_Model->seeResults();
			if($tmp === $this->session->userErrorCode){
				self::errorRedirector('Welcome/errorSQL');
			}

			$yours = $tmp['yours'];
			$others = $tmp['others'];

			if (count($yours)===0){
				$yourOutput = "Vous n'avez publié aucun formulaire.";
			} else {

				$yourOutput =  '<ul>';
				foreach ($yours as $form) {
					$title = htmlentities($form->title);
					$formId = $form->formid;
					$url = base_url("Administrate/showResults/$formId");
					$yourOutput.= '<li> <a href="'.$url.'">'.$title."</a></li>";
				}
				$yourOutput .= '</ul>';
			}

			if (count($others)===0){
				$othersOutput = "Vous ne pouvez voir les résultats d'aucun formulaire créé par d'autres personnes.";
			} else {

				$othersOutput =  '<ul>';
				foreach ($others as $form) {
					$title = $form->title;
					$formId = $form->formid;
					$url = base_url("Administrate/showResults/$formId");
					$othersOutput.= '<li> <a href="'.$url.'">'.$form->title."</a></li>";
				}
				$othersOutput .= '</ul>';
			}


			$param = array(
				'yours'=>$yourOutput,
				'others'=>$othersOutput
			);

			$this->layouts->setTitle("Répondre à un formulaire")
										->addView('administrate/results',$param)
										->display();
		}



		/**
		 * Function to show the results of a form
		 * The access to this function is obviously checked !
		 *
		 * @param  string $formId The id of the form to check (it's a string due to the URI process.)
		 */
		public function showResults($formId){
			$formId = intval($formId);
			/////////////////
			///////////////// security checks
			/////////////////
			$this->load->model('Administrate_Model');
			$title = $this->Administrate_Model->canSeeResults($formId);

			if($title === $this->session->userErrorCode){
				self::errorRedirector('Welcome/errorSQL');
			}
			if($title === false){
				self::errorRedirector('Welcome/errorNoSupport');
			}
			/////////////////
			/////////////////
			/////////////////



			// The user is allowed to access the part bellow if has passed the test

			$data = $this->Administrate_Model->getResults($formId);

			$this->load->model('FormCreation_model');
			$source = $this->FormCreation_model->getJSONformData($formId);

			if($data === false or $source===false){
				redirect('Welcome/errorNoSupport','location');
			} else if ($data === $this->session->userErrorCode or $source === $this->session->userErrorCode ){
				redirect('Welcome/errorSQL','location');
			}

			$this->load->library('ResultsBuilder');
			$result = $this->resultsbuilder->buildResults($data,$source);
			$this->load->helper('security');
			$result=$this->security->xss_clean($result);

			$param1 = array(
				'title'=>$title,
				'result' => $result,
				'formId' => $formId
			);

			$this->layouts->setTitle("Résultats")
										->addView('administrate/showResults',$param1)
										->addView('answer/script_confirm',array(),'bottom')
										->display();

		}




	/**
	 * Function to administrate a form
	 * The access to this function is obviously checked !
	 *
	 * @param  string $formId The id of the form to check (it's a string due to the URI process.)
	 */
	public function admin($formId){
		$formId = intval($formId);

		/////////////////
		///////////////// security checks
		/////////////////
		$this->load->model('Administrate_Model');
		$formTitle = $this->Administrate_Model->canAdministrate($formId);

		if($formTitle === $this->session->userErrorCode){
			self::errorRedirector('Welcome/errorSQL');
		}
		if($formTitle === false){
			self::errorRedirector('Welcome/errorNoSupport');
		}
		/////////////////
		/////////////////
		/////////////////


			$param = array(
				'id'=>$formId,
				'ajaxToken'=>$this->generateAjaxToken(),
				'ajaxLocation'=>self::ajaxLocation(),
				'mainSavingFormHandler' => 'ajaxSaveAdmin',
				'populateFormHandler' => 'ajaxGetAdmin',
			);

			$param2 = array(
				'formId'=>$formId,
				'title' => $formTitle,
				'shareUrl' => base_url("Answer/form/$formId")
			);

			$this->layouts->setTitle("Administration du formulaire $formId")
										->addView('administrate/admin',$param2)
										->addView('forms/scripts_formLibs',array(),'bottom')
										->addView('administrate/script_admin',$param,'bottom')
										->display();
	}



	/**
	 * Function to generate the administration form.
	 * This function will send the requested data back only if the user has the authorization to access it.
	 *
	 * @param  array $data  The data sent by the user trough ajax
	 */
	private function ajaxGetAdmin($data)
	{
		$formId = intval($data["id"]);
		/////////////////
		///////////////// security checks
		/////////////////
		$this->load->model('Administrate_Model');
		$test = $this->Administrate_Model->canAdministrate($formId);

		if($test === $this->session->userErrorCode){
			return $this->JSONerror('Erreur SQL, merci de contacter les admins');
		}
		if($test === false){
			return $this->JSONerror('VOUS NE POUVEZ PAS ACCÉDER À CES INFORMATIONS',false);
		}
		///////////////
		/////////////// Access check finished
		///////////////





		$this->load->model('FormCreation_model');
		$data = $this->FormCreation_model->getFormInfo($formId);
		if($data === $this->session->userErrorCode){
			return $this->JSONerror("Erreur lors de l'interrogation de la base de donnée, merci de contacter les administrateurs du site.");
		}

		$closeDate = $data->closedate;
		$publicJSONsource = $data->publicjsonsource;

		$this->load->library('tools');
		$closeDate = $this->tools->timestampToDate($closeDate);


		$this->load->model('Groupform_Model');
		$groupsThatCanAccess = $this->Groupform_Model->accessGroupForm($formId,false);
		$groupsCurrentlyAccessing = $this->Groupform_Model->groupsAccessingForm($formId);
		$groupsCanSeeResults=$this->Administrate_Model->getGroups();
		$groupsCurrentlySeeingResults = $this->Administrate_Model->groupsAccessingResults($formId);

		if($groupsThatCanAccess === $this->session->userErrorCode
		or $groupsCurrentlyAccessing === $this->session->userErrorCode
		or $groupsCanSeeResults === $this->session->userErrorCode
		or $groupsCurrentlySeeingResults === $this->session->userErrorCode
		)	return $this->JSONerror("Erreur lors de l'interrogation de la base de donnée, merci de contacter les administrateurs du site.");

		// building the awsome selector
		$this->load->library('FormBuilder');
		$form = $this->formbuilder->adminSelect("FormAccess",false)
															->addMultiple()
															->setFieldLabel("Quels sont les groupes d'utilisateurs⋅rices qui peuvent répondre à ce formulaire ?")
															->setFieldPlaceholder("Groupes disponibles")
															->addGroup("Groupes principaux");

		foreach ($groupsThatCanAccess['mainGroups'] as $element) {
			$id = $element->groupid;
			$form = $form -> addGroupOption(htmlspecialchars($element->title),$id,isset($groupsCurrentlyAccessing[$id]));
		}
		$form = $form->closeGroup()
									->addGroup("Autres groupes administratifs");

		foreach ($groupsThatCanAccess['otherAdministrativeGroups'] as $element) {
			$id = $element->groupid;
			$form = $form -> addGroupOption(htmlspecialchars($element->title),$id,isset($groupsCurrentlyAccessing[$id]));
		}
		$form = $form->closeGroup()
									->addGroup("Vos groupes");

		foreach ($groupsThatCanAccess['yourGroups'] as $element) {
			$id = $element->groupid;
			$form = $form -> addGroupOption(htmlspecialchars($element->title),$id,isset($groupsCurrentlyAccessing[$id]));
		}
		$form = $form->closeGroup()
									->addGroup("Autres groupes auxquels vous appartenez");

		foreach ($groupsThatCanAccess['otherGroupsYouBelongTo'] as $element) {
			$id = $element->groupid;
			$form = $form -> addGroupOption(htmlspecialchars($element->title).' (créé par '.htmlspecialchars($element->login).')',$id,isset($groupsCurrentlyAccessing[$id]));
		}
		$form = $form->closeGroup()
									->closeField()
									->adminSelect("FormResults",false)
									->addMultiple()
									->setFieldLabel("Quels sont les groupes d'utilisateurs⋅rices qui peuvent accéder aux résultats de ce formulaire ? (Parmi ceux que vous avez créés, ou créés par d'autres et auxquels vous appartenez)")
									->setFieldPlaceholder("Groupes disponibles")
									->addGroup("Vos groupes");


			foreach ($groupsCanSeeResults['yours'] as $element) {
				$id = $element->groupid;
				$form = $form -> addGroupOption(htmlspecialchars($element->title),$id,isset($groupsCurrentlySeeingResults[$id]));
			}
			$form = $form->closeGroup()
										->addGroup("Autres groupes auxquels vous appartenez");

			foreach ($groupsCanSeeResults['others'] as $element) {
				$id = $element->groupid;
				$form = $form -> addGroupOption(htmlspecialchars($element->title).' (créé par '.htmlspecialchars($element->creator).')',$id,isset($groupsCurrentlySeeingResults[$id]));
			}
			$form = $form->closeGroup()
										->closeField();

			$form = $form->date('closeDate',true,$closeDate)
									->setFieldLabel("À quelle date souhaitez-vous clore le formulaire ? (La clôture s'effectuera à 23h59)")
									->closeField()
									->radio("publicSource")
									->setFieldLabel("Souhaitez-vous que le code source de votre formulaire soit disponible auprès de l'ensemble des utilisateurs⋅rices pour qu'ils⋅elles puissent l'utiliser comme base ?")
									->addRadioOption("Oui, je le partage.",'true',$publicJSONsource)
									->addRadioOption("Non, je ne le partage pas.",'false',!$publicJSONsource)
									->closeField()
									->getFormAndPopulate();

		return $this->JSONsuccess($form);
	}





	/**
	 * Function to save the form generated
	 *
	 * @param  array $data  The data sent by the user trough ajax
	 */
	private function ajaxSaveAdmin($data)
	{
			$dataFromForm = $data["dataFromForm"];
			$results = $dataFromForm["results"];
			$formId = intval($data["id"]);
			/////////////////
			///////////////// security checks
			/////////////////

			$this->load->model('Administrate_Model');
			$test = $this->Administrate_Model->canAdministrate($formId);

			if($test === $this->session->userErrorCode){
				return $this->JSONerror('Erreur SQL, merci de contacter les admins');
			}
			if($test === false){
				return $this->JSONerror('VOUS NE POUVEZ PAS ACCÉDER À CES INFORMATIONS',false);
			}



			///////////////
			/////////////// Access check finished
			///////////////

			// automatic validation
			$test = $this->validateReturnedFormData($data);
			if ($test !== true){
				return $this->JSONerror($test);
			}

			$accessGroups = $results['FormAccess'];
			$closeDate = $results['closeDate'];
			$accessResults = $results['FormResults'];
			$publicSource = $results['publicSource'];

			$this->load->library("tools");
			$accessGroups = $this->cleanGroupResults($accessGroups);
			$accessResults = $this->cleanGroupResults($accessResults);

			$publicSource = $publicSource === 'true';
			$closeDate = $this->tools->dateToTimestamp($closeDate);

			$this->load->model('Administrate_Model');

			if ($this->Administrate_Model->administrateForm($formId, $publicSource,$closeDate,$accessGroups,$accessResults) === true){
					return $this->JSONsuccess();
			} else {
				return $this->JSONerror("Erreur lors de l'interrogation de la base de donnée, merci de contacter les administrateurs du site.");
			}

	}

}
