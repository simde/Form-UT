<?php

include_once(APPPATH . 'core/My_Controller_with_login.php');

/**
 * This the controller used for Form creation.
 * It's access is limited to connected user.
 */
class FormCreation extends My_Controller_with_login
{
	/**
	 * This paramater has to be redefined so that ajax works correctly
	 */
		protected $className ="FormCreation";


		/**
		 * Ajax redirector for this controller
		 * @param  string $redirect What function should treat the request.
		 * @param  JSONarray $data   The data sent to the server
		 * @return function call
		 */
		protected function ajaxRedirector($redirect,$data)
		// override the function in My controller with login
		{
			if ($redirect == 'ajaxSaveTitle'){
				return self::ajaxSaveTitle($data);
			} elseif ($redirect == 'ajaxGetTitle'){
				return self::ajaxGetTitle($data);
			} elseif ($redirect == 'ajaxLoadFormArchitecture'){
				return self::ajaxLoadFormArchitecture($data);
			} elseif ($redirect == 'ajaxSaveFormArchitecture'){
				return self::ajaxSaveFormArchitecture($data);
			} elseif ($redirect == 'ajaxMainSaveFormArchitecture'){
				return self::ajaxMainSaveFormArchitecture($data);
			} elseif ($redirect == 'ajaxPreviewInspiration'){
				return self::ajaxPreviewInspiration($data);
			} elseif ($redirect == 'ajaxGetInspiration'){
				return self::ajaxGetInspiration($data);
			} elseif ($redirect == 'ajaxCheckInspiration'){
				return self::ajaxCheckInspiration($data);
			} elseif ($redirect == 'ajaxGetFinalForm'){
				return self::ajaxGetFinalForm($data);
			} elseif ($redirect == 'ajaxSaveFinalForm'){
				return self::ajaxSaveFinalForm($data);
			}elseif ($redirect == 'ajaxFinalPreview'){
				return self::ajaxFinalPreview($data);
			}


			return $this->JSONerror("No function fond for treating the request");
		}

    public function index()
    {
			redirect('FormCreation/initForm');
    }


		public function displayFormManager()
		{
		// Nothing yet :)
		}

		/**
		 * Checks that the form is in draft and that the user is the owner
		 * @param  int  $formId   The Id of the form
		 * @param  boolean $redirect Do we force redirect the user, not sure it is working
		 * @return boolean | string     False if can't access, or the title of the form if access is granted
		 */
		private function checkAccessDraftForm($formId, $redirect = false)
		{
			$formId = intval($formId);

			$this->load->model('FormCreation_model');

			// We have to check if the form is a draft and belongs to the user
			$formTitle = $this->FormCreation_model->isFormADraft($formId);
			if($formTitle === $this->session->userErrorCode){
				if ($redirect === true) {
					return self::errorRedirector('Welcome/errorSQL');
				} else {return false;}

			}

			return $formTitle;
		}

		private function moveToStep($formId,$step){
			$this->session->set_userdata('formStep'.$formId,$step);
		}

		private function checkStep($formId,$step){
			if ($this->session->userdata('formStep'.$formId) === NULL) return false;
			return $this->session->userdata('formStep'.$formId) >= $step;
		}


		/**
		 * Init the form creation (creates a new form in the database)
		 */
		public function initForm()
		{
				// We have to create a new form
				$this->load->model('FormCreation_model');
				$formId = $this->FormCreation_model->createForm($this->session->userIDinDB);

				// check
				if($formId === $this->session->userErrorCode){
					self::errorRedirector('Welcome/errorSQL');
				}

				$url = 'FormCreation/formTitle/'.$formId;
				redirect($url,'location');
		}




		/**
		 * Ask for the form title
		 * @param  string $formId fomid
		 */
		public function formTitle($formId = false){
			$formId = intval($formId);

			if ($formId === 0) {
				return redirect('Welcome/errorNoSupport','location');

			}
			/////////////////
			///////////////// security checks
			/////////////////
			$title = self::checkAccessDraftForm($formId,true); //this forces the redirection happen if you can't access the form

			if ($title === false) {
				return redirect('Welcome/errorNoSupport','location');

			}
			/////////////////
			/////////////////

			$param = array(
				'id'=>$formId,
				'ajaxToken'=>$this->generateAjaxToken(),
				'ajaxLocation'=>self::ajaxLocation(),
				'simpleSavingFormHandler' => 'ajaxSaveTitle',
				'mainSavingFormHandler' => 'ajaxSaveTitle',
				'populateFormHandler' => 'ajaxGetTitle',
				'nextPage' => base_url("FormCreation/builder/$formId")
			);

			$this->layouts->setTitle("Création d'un formulaire")
										->addView('forms/initForm')
										->addView('forms/scripts_formLibs',array(),'bottom')
										->addView('forms/script_initForm',$param,'bottom')
										->display();

		}




		/**
		 * Function to receive ajax data and generate the title form
		 * @param  array $data data sent by the user
		 * @return JSON   Data to build the form
		 */
		private function ajaxGetTitle($data)
		{
			$formId = intval($data["id"]);
			/////////////////
			///////////////// security checks
			/////////////////
			$title = self::checkAccessDraftForm($formId,false); //also checks if the user is the creator

			if ($title === false){
				$this->JSONerror("You cannot modify this form, well tried...",false);
			}
			/////////////////
			/////////////////
			/////////////////

			$this->load->library('FormBuilder');
			$data = $this->formbuilder->text("title")
																->setFieldLabel("Intitulé du formulaire :")
																->setMaxLength("200")
																->setFieldPlaceholder("Inscrire l'intitulé ici...")
																->setFieldValue($title)
																->closeField()
																->getFormAndPopulate();

			return $this->JSONsuccess($data);

		}


		/**
		 * Function to check and save the title
		 * @param  array $data data sent by the user
		 * @return JSON   succes or error
		 */
		private function ajaxSaveTitle($data)
		{
				$dataFromForm = $data["dataFromForm"];
				$results = $dataFromForm["results"];
				$title = $results["title"];
				$formId = intval($data["id"]);
				/////////////////
				/////////////////
				/////////////////

				$test = self::checkAccessDraftForm($formId,false); //also checks if the user is the creator

				if ($test === false){
					return $this->JSONerror("You cannot modify this form, well tried...",false);
				}
				/////////////////
				/////////////////
				/////////////////

				$test = $this->validateReturnedFormData($data);
				if ($test !== true){
					return $this->JSONerror($test); // not valid form
				}

				$this->load->model('FormCreation_model');

				if ($this->FormCreation_model->updateFormTitle($title,$formId) === true){
						self::moveToStep($formId,1);
						return $this->JSONsuccess();
				} else {
					return $this->JSONerror("Erreur lors de l'interrogation de la base de donnée, merci de contacter les administrateurs du site.");
				}

		}





		/**
		 * Function for building the form (a lot of JS going on there)
		 * @param  string $formid the id of the form
		 */
		public function builder($formId)
		{
			$formId = intval($formId);
			/////////////////
			///////////////// security checks
			/////////////////
			if (!self::checkStep($formId,1)){
				redirect('Welcome/errorNoSupport','location');

			} self::moveToStep($formId,1);


			$title = self::checkAccessDraftForm($formId, true);
			if ($title === false) 				redirect('Welcome/errorNoSupport','location');
			/////////////////
			/////////////////
			/////////////////


			$param1 = array(
				'id'=>intval($formId),
				'ajaxToken1'=>$this->generateAjaxToken(),
				'ajaxToken2'=>$this->generateAjaxToken(),
				'ajaxLocation'=>self::ajaxLocation(),
				'inspirationLoadHandler' => '',
				'simpleSavingFormHandler' => 'ajaxSaveFormArchitecture',
				'mainSavingFormHandler' => 'ajaxMainSaveFormArchitecture',
				'populateFormHandler' => 'ajaxLoadFormArchitecture',
				'nextPage' => base_url("FormCreation/finish/$formId"),
				'previousPage' => base_url("FormCreation/formTitle/$formId"),
				'translationLocation' => base_url("assets/lang/"),
				'inspirationGetHandler' => 'ajaxGetInspiration',
				'inspirationPreviewHandler' => 'ajaxPreviewInspiration',
				'checkInspirationHandler' => 'ajaxCheckInspiration'
			);

			$param2 = array(
				'title' => $title
			);

			$this->layouts->setTitle("Création d'un formulaire")
										->addView('forms/builder',$param2)
										->addView('forms/scripts_formLibs',array(),'bottom')
										->addView('forms/script_builder',$param1,'bottom')
										->display();
		}




		/**
		 * Function to send the previous formdata
		 * @param  array $data data sent by the user
		 * @return JSON   form architecture
		 */
		private function ajaxLoadFormArchitecture($data)
		{
			$formId = intval($data["id"]);
			/////////////////
			///////////////// security checks
			/////////////////
			if (!self::checkStep($formId,1)){
				return $this->JSONerror("You are not authorize to be at this stage of form creation");
			}

			$formTitle = self::checkAccessDraftForm($formId, false); //also checks if the user is the creator

			if ($formTitle === false){
				return $this->JSONerror("Couldn't intrept the ID");
			}
			/////////////////
			/////////////////
			/////////////////

			$this->load->model('FormCreation_model');
			$JSONFormData = $this->FormCreation_model->getJSONformData($formId);
			if($JSONFormData === $this->session->userErrorCode){
				return $this->JSONerror("Erreur lors de l'interrogation de la base de donnée, merci de contacter les administrateurs du site.");
			}

			//if there is nothing...
			if ($JSONFormData === false OR $JSONFormData ==NULL){
				$JSONFormData = array();
			}

			$return = array("formBuilderData" => $JSONFormData);

			return $this->JSONsuccess($return);
		}


		/**
		 * Function to save the formA rchitecture
		 * @param  array $data data sent by the user
		 */
		private function ajaxSaveFormArchitecture($data)
		{
			$formBuilderData = $data["formBuilderData"];
			$formId = intval($data["id"]);
			/////////////////
			///////////////// security checks
			/////////////////

			if (!self::checkStep($formId,1)){
				return $this->JSONerror("You are not authorize to be at this stage of form creation");
			}

			$title = self::checkAccessDraftForm($formId,false); //also checks if the user is the creator
			if ($title === false){
				return $this->JSONerror("Couldn't intrept the ID");
			}
			/////////////////
			/////////////////

			$this->load->model('FormCreation_model');
			$request = $this->FormCreation_model->updateJSONsource($formBuilderData,$formId);
			if ($request !== $this->session->userErrorCode ){
				self::moveToStep($formId,1);
				return $this->JSONsuccess();
			} else {
				return $this->JSONerror("Erreur lors de l'interrogation de la base de donnée, merci de contacter les administrateurs du site.");
			}

		}



		/**
		 * Function to check and save the form architecture
		 * @param  array $data data sent by the user
		 */
		private function ajaxMainSaveFormArchitecture($data)
		{
			$formBuilderData = $data["formBuilderData"];
			$formId = intval($data["id"]);
			/////////////////
			///////////////// security checks
			/////////////////

			if (!self::checkStep($formId,1)){
				return $this->JSONerror("You are not authorize to be at this stage of form creation");
			}

			$test = self::checkAccessDraftForm($formId,false); //also checks if the user is the creator
			if ($test === false){
				return $this->JSONerror("Couldn't intrept the ID");
			}
			/////////////////
			/////////////////

			$this->load->library('Validator');
			$test = $this->validator->formCreatedValidation(json_decode($formBuilderData,true));
			if ($test !== true){
				return $this->JSONerror($test);
			}

			$this->load->model('FormCreation_model');
			$request = $this->FormCreation_model->updateJSONsource($formBuilderData,$formId);
			if ($request !== $this->session->userErrorCode ){
				self::moveToStep($formId,2);
				return $this->JSONsuccess();
			} else {
				return $this->JSONerror("Erreur lors de l'interrogation de la base de donnée, merci de contacter les administrateurs du site.");
			}

		}


		/**
		 * Function to load the inpirationnal data (the select of all the form accesible)
		 * @param  array $data data sent by the user
		 */
		private function ajaxGetInspiration($data)
		{
			$this->load->model('FormCreation_model');
			$data = $this->FormCreation_model->getInspired();

			if($data === $this->session->userErrorCode){
				return $this->JSONerror("Erreur lors de l'interrogation de la base de donnée, merci de contacter les administrateurs du site.");
			}

			// building the awsome selector
			$this->load->library('FormBuilder');
			$form = $this->formbuilder->adminSelect("getInspired")
																->setFieldLabel("Formulaire(s) dont vous pouvez vous inspirer :")
																->setFieldPlaceholder("Formulaires disponibles (ou pas)...")
																->addGroup("Vos formulaires");

			foreach ($data['yourself'] as $element) {
				$form = $form -> addGroupOption($element->title,$element->formid);
			}
			$form = $form->closeGroup()
										->addGroup("Autres formulaires disponibles");

			foreach ($data['others'] as $element) {
				$form = $form -> addGroupOption($element->title.' (créé par '.$element->login.')',$element->formid);
			}
			$form = $form->closeGroup()
										->closeField()
										->getFormAndPopulate();

			return $this->JSONsuccess($form);
		}


		/**
		 * Function send the form architecture data of inspirationnal stuff
		 * @param  array $data data sent by the user
		 */
		private function ajaxPreviewInspiration($data)
		{
			$test = $this->validateReturnedFormData($data);
			if ($test !== true){
				return $this->JSONerror($test);
			}

			$dataFromForm = $data["dataFromForm"];
			$results = $dataFromForm["results"];
			$formId = $results["getInspired"];
			$formId = $this->cleanGroupResults($formId)[0];
			$formId = intval($formId);


			$this->load->model('FormCreation_model');
			$JSONFormData = $this->FormCreation_model->getJSONformData($formId);
			if($JSONFormData === $this->session->userErrorCode){
				return $this->JSONerror("Erreur lors de l'interrogation de la base de donnée, merci de contacter les administrateurs du site.");
			}

			//if there is nothing...
			if ($JSONFormData === false OR $JSONFormData ==NULL){
				$JSONFormData = array();
			}

			$return = array("formBuilderData" => $JSONFormData);

			return $this->JSONsuccess($return);
		}




		/**
		 * Function to check that the subimmitted data can be validated
		 * @param  array $data data sent by the user
		 */
		private function ajaxCheckInspiration($data){
			$this->load->library('Validator');
			$test = $this->validator->formCreatedValidation($data['json']);
			if ($test !== true){
				return $this->JSONerror($test);
			}

			return $this->JSONsuccess();
		}



		/**
		 * Last step of form creation, we ask more technicals details
		 * @param  string $formId id of the form
		 */
		public function finish($formId = false){
			/////////////////
			///////////////// security checks
			/////////////////
			if (!self::checkStep($formId,2)){
				redirect('Welcome/errorNoSupport','location');

			}
			self::moveToStep($formId,2);

			$formId = intval($formId);

			if ($formId === 0) {
				redirect('Welcome/errorNoSupport','location');
			}
			/////////////////
			/////////////////

			$title = self::checkAccessDraftForm($formId,true); //this forces the redirection happen if you can't access the form


			$param = array(
				'id'=>$formId,
				'ajaxToken'=>$this->generateAjaxToken(),
				'ajaxLocation'=>self::ajaxLocation(),
				'simpleSavingFormHandler' => 'ajaxSaveFinalForm',
				'mainSavingFormHandler' => 'ajaxSaveFinalForm',
				'populateFormHandler' => 'ajaxGetFinalForm',
				'previousPage' => base_url("FormCreation/builder/$formId"),
				'nextPage' => base_url("FormCreation/confirm/$formId")
			);

			$param2 = array(
				'title'=>$title
			);

			$this->layouts->setTitle("Création d'un formulaire")
										->addView('forms/finish',$param2)
										->addView('forms/scripts_formLibs',array(),'bottom')
										->addView('forms/script_finish',$param,'bottom')
										->display();

		}


		/**
		 * Function to generate the finish form
		 * @param  array $data data sent by the user
		 */
		private function ajaxGetFinalForm($data)
		{
			$formId = intval($data["id"]);
			/////////////////
			///////////////// security checks
			/////////////////
			if (!self::checkStep($formId,2)){
				return $this->JSONerror("You are not authorize to be at this stage of form creation");
			}


			$test = self::checkAccessDraftForm($formId,false); //also checks if the user is the creator
			if ($test === false){
				return $this->JSONerror("Couldn't intrept the ID");
			}
			/////////////////
			/////////////////

			$this->load->model('FormCreation_model');
			$data = $this->FormCreation_model->getFormInfo($formId);
			if($data === $this->session->userErrorCode){
				return $this->JSONerror("Erreur lors de l'interrogation de la base de donnée, merci de contacter les administrateurs du site.");
			}

			$closeDate = $data->closedate;
			$anonymous = $data->anonymous;
			$personnalInfo = $data->personnalinfo;

			$this->load->library('tools');
			$closeDate = $this->tools->timestampToDate($closeDate);

			// Form id à récupéreturn
			$this->load->model('Groupform_Model');

			$data = $this->Groupform_Model->accessGroupForm($formId,false);
			if($data === $this->session->userErrorCode){
				return $this->JSONerror("Erreur lors de l'interrogation de la base de donnée, merci de contacter les administrateurs du site.");
			}


			$actualData = $this->Groupform_Model->groupsAccessingForm($formId);
			if($actualData === $this->session->userErrorCode){
				return $this->JSONerror("Erreur lors de l'interrogation de la base de donnée, merci de contacter les administrateurs du site.");
			}

			$result = array('mainGroups'=>array(),'otherAdministrativeGroups'=>array(),'yourGroups'=>array(),'otherGroupsYouBelongTo'=>array());

			// building the awsome selector
			$this->load->library('FormBuilder');
			$form = $this->formbuilder->paragraph("blabla")
																->setFieldLabel("Les réponses aux points 2 et 3 sont non modifiables après publication.")
																->closeField()
																->adminSelect("FormAccess",false)
																->addMultiple()
																->setFieldLabel("1) Choisissez les groupes d'utilisateurs⋅rices qui auront accès à ce formulaire :")
																->setFieldPlaceholder("Groupes disponibles")
																->addGroup("Groupes principaux");

			foreach ($data['mainGroups'] as $element) {
				$id = $element->groupid;
				$form = $form -> addGroupOption(htmlspecialchars($element->title),$id,isset($actualData[$id]));
			}
			$form = $form->closeGroup()
										->addGroup("Autres groupes administratifs");

			foreach ($data['otherAdministrativeGroups'] as $element) {
				$id = $element->groupid;
				$form = $form -> addGroupOption(htmlspecialchars($element->title),$id,isset($actualData[$id]));
			}
			$form = $form->closeGroup()
										->addGroup("Vos groupes");

			foreach ($data['yourGroups'] as $element) {
				$id = $element->groupid;
				$form = $form -> addGroupOption(htmlspecialchars($element->title),$id,isset($actualData[$id]));
			}
			$form = $form->closeGroup()
										->addGroup("Autres groupes auxquels vous appartenez");

			foreach ($data['otherGroupsYouBelongTo'] as $element) {
				$id = $element->groupid;
				$form = $form -> addGroupOption(htmlspecialchars($element->title).' (créé par '.htmlspecialchars($element->login).')',$id,isset($actualData[$id]));
			}
			$form = $form->closeGroup()
										->closeField()
										->radio("anonymous")
										->setFieldLabel("2) Souhaitez-vous que les sondé⋅e⋅s soient anonymes ? (Cela concerne le login CAS, le nom et prénom et l'adresse mail de ces derniers)")
										->addRadioOption('Oui.','2',$anonymous===2)
										->addRadioOption('Non.','0',$anonymous===0)
										->addRadioOption('Je les laisse choisir.','1',$anonymous===1)
										->closeField()
										->radio("cas")
										->setFieldLabel("3) Souhaitez-vous connaître les informations du CAS ? (Groupe CAS et semestre d'études à l'UTC pour les étudiants - quand ces données seront accessibles)")
										->addRadioOption('Oui.','0',$personnalInfo===0)
										->addRadioOption('Non.','2',$personnalInfo===2)
										->addRadioOption('Je les laisse choisir.','1',$personnalInfo===1)
										->closeField()
										->date('closeDate',true,$closeDate)
										->setFieldLabel("4) À quelle date souhaitez-vous clore le formulaire ? (La clôture s'effectuera à 23h59)")
										->closeField()
										->getFormAndPopulate();

			return $this->JSONsuccess($form);
		}




		/**
		 * Function to check and save the finish form
		 * @param  array $data data sent by the user
		 */
		private function ajaxSaveFinalForm($data)
		{
				$dataFromForm = $data["dataFromForm"];
				$results = $dataFromForm["results"];
				$formId = intval($data["id"]);

				/////////////////
				///////////////// security checks
				/////////////////
				if (!self::checkStep($formId,2)){
					return $this->JSONerror("You are not authorize to be at this stage of form creation");
				}


				$test = self::checkAccessDraftForm($formId,false); //also checks if the user is the creator

				if ($test === false){
					return $this->JSONerror("You cannot modify this form, well tried...",false);
				}
				/////////////////
				/////////////////

				$test = $this->validateReturnedFormData($data);
				if ($test !== true){
					return $this->JSONerror($test);
				}

				$accessGroups = $results['FormAccess'];
				$closeDate = $results['closeDate'];
				$anonymous = $results['anonymous'];
				$cas = $results['cas'];

				$this->load->library("tools");
				$accessGroups = $this->cleanGroupResults($accessGroups);
				$closeDate = $this->tools->dateToTimestamp($closeDate);

				$this->load->model('FormCreation_model');

				if ($this->FormCreation_model->updateForm($formId, $anonymous,$cas,$closeDate,$accessGroups) === true){
						self::moveToStep($formId,3);

						return $this->JSONsuccess();
				} else {
					return $this->JSONerror("Erreur lors de l'interrogation de la base de donnée, merci de contacter les administrateurs du site.");
				}

		}


		/**
		 * Function to confirm the form creation
		 * @param  string $formId formId
		 */
		public function confirm($formId = false){
			/////////////////
			///////////////// security checks
			/////////////////
			if (!self::checkStep($formId,3)){
				redirect('Welcome/errorNoSupport','location');
			}

			$formId = intval($formId);

			if ($formId === 0) {
				redirect('Welcome/errorNoSupport','location');
			}
			/////////////////
			/////////////////

			$title = self::checkAccessDraftForm($formId,true); //this forces the redirection happen if you can't access the form

			$param1 = array(
				'title'=>$title,
				'nextPage' => base_url("FormCreation/success/$formId"),
				'previousPage' => base_url("/FormCreation/finish/$formId")
			);


			$param2 = array(
				'id'=>$formId,
				'ajaxToken'=>$this->generateAjaxToken(),
				'ajaxLocation'=>self::ajaxLocation(),
				'populateFormHandler' => 'ajaxFinalPreview',
			);

			$this->layouts->setTitle("Création d'un formulaire")
										->addView('forms/confirm',$param1)
										->addView('forms/scripts_formLibs',array(),'bottom')
										->addView('forms/script_confirm',$param2,'bottom')
										->display();
		}




		/**
		 * Function to load the final preview
		 * @param  array $data data sent by the user
		 */
		private function ajaxFinalPreview($data)
		{
			$formId = $data["id"];
			$formId = intval($formId);
			/////////////////
			///////////////// security checks
			/////////////////
			$test = self::checkAccessDraftForm($formId,true); //this forces the redirection happen if you can't access the form
			if ($test === false) {
				$this->JSONerror("Vous ne pouvez accédez aux données de ce formulaire.");
			}
			/////////////////
			/////////////////

			$this->load->model('FormCreation_model');
			$JSONFormData = $this->FormCreation_model->getJSONformData($formId);
			if($JSONFormData === $this->session->userErrorCode){
				return $this->JSONerror("Erreur lors de l'interrogation de la base de donnée, merci de contacter les administrateurs du site.");
			}

			$return = array("formBuilderData" => $JSONFormData);

			return $this->JSONsuccess($return);
		}


		/**
		 * Simple function to alert the user that everything went well
		 * An email with the form info is sent.
		 * @param  string $formId formId
		 */
		public function success($formId = false){
			/////////////////
			///////////////// security checks
			/////////////////
			if (!self::checkStep($formId,3)){
				redirect('Welcome/errorNoSupport','location');

			}

			$formId = intval($formId);

			if ($formId === 0) {
				redirect('Welcome/errorNoSupport','location');
			}
			/////////////////
			/////////////////

			$this->load->model('FormCreation_model');
			$title = $this->FormCreation_model->getFormInfo($formId)->title;

			$test = $this->FormCreation_model->publishForm($formId);

			if ($test !== true){
				redirect('Welcome/errorSQL','location');
			}

			$urlToShare = base_url("/Answer/form/$formId");
			$urlToAdmin = base_url("/Administrate/admin/$formId");
			$param = array(
				'title'=>$title,
				'urlToShare' => $urlToShare,
				'urlToAdmin' => $urlToAdmin
			);

			$this->layouts->setTitle("Création d'un formulaire")
										->addView('forms/success',$param)
										->display();

										$this->load->library('email');

			$this->email->from('florent.chehab@etu.utc.fr', 'Form-UT');
			$this->email->to($this->session->userEmail);
			$this->email->subject('Formulaire créé !');
			$this->email->message("
	Bonjour,

Vous venez de créer le formulaire $title sur Form-UT.

Le lien pour répondre au formulaire est :
$urlToShare

Le lien pour administrer le formulaire est :
$urlToAdmin

	Cordialement,
	L'Équipe de Form-UT
			");

			$this->email->send();
		}


}
