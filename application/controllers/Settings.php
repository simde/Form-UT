<?php

include_once(APPPATH . 'core/My_Controller_with_login.php');

/**
 * This the controller used for settings manipulations.
 * It's access is limited to connected user.
 *
 * This controller is currently useless
 */
class Settings extends My_Controller_with_login
{
	/**
	 * This paramater has to be redefined so that ajax works correctly
	 * @var [type]
	 */
		protected $className ="Settings";

		/**
		 * Ajax redirector for this controller
		 * @param  string $redirect What function should treat the request.
		 * @param  JSONarray $data   The data sent to the server
		 * @return function call
		 */
		protected function ajaxRedirector($redirect,$data)
		// override the function in My controller with login
		{
			if ($redirect == 'ajaxGetLanguage'){
				return self::ajaxGetLanguage($data);
			} elseif ($redirect == 'ajaxSaveLanguage'){
				return self::ajaxSaveLanguage($data);
			}

			return $this->JSONerror("No function fond for treating the request");
		}

    public function index()
    {
			redirect('Settings/changeLanguage','location');
    }


		private function changeLanguage()
		{
			$param = array(
				'id'=>1,
				'ajaxToken'=>$this->generateAjaxToken(),
				'ajaxLocation'=>self::ajaxLocation(),
				'simpleSavingFormHandler' => 'ajaxSaveLanguage',
				'mainSavingFormHandler' => 'ajaxSaveLanguage',
				'populateFormHandler' => 'ajaxGetLanguage',
			);

			$this->layouts->setTitle("Création d'un formulaire")
										->addView('settings/changeLanguage')
										->addView('forms/scripts_formLibs',array(),'bottom')
										->addView('settings/script_changeLanguage',$param,'bottom')
										->display();
		}


		private function ajaxGetLanguage($data)
		{
			$this->load->library('FormBuilder');
			$data = $this->formbuilder->select("language")
																->setFieldLabel("Merci de sélectionner la langue désirée :")
																->setFieldPlaceholder("Dans la liste...")
																->addSelectOption('Français','fr_FR','fr_FR'===$this->session->userLanguage)
																->addSelectOption('English','en_US','en_US'===$this->session->userLanguage)
																->closeField()
																->getFormAndPopulate();

			return $this->JSONsuccess($data);

		}

		private function ajaxSaveLanguage($data)
		{
				$dataFromForm = $data["dataFromForm"];
				$results = $dataFromForm["results"];
				$language = $results["language"];

				$test = $this->validateReturnedFormData($data);
				if ($test !== true){
					return $this->JSONerror($test);
				}

				$this->session->userLanguage = $this->cleanGroupResults($language)[0];
				$this->load->model('User_model');

				if ($this->User_model->updateUserLanguage()){
						return $this->JSONsuccess();
				} else {
					return $this->JSONerror("Erreur lors de l'interrogation de la base de donnée, merci de contacter les administrateurs du site.");
				}
		}

}
