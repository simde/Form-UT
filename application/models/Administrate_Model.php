<?php
include_once(APPPATH . 'core/My_Model.php');
/**
 * Class for handling database manipulations related to users.
 */
class Administrate_Model extends MY_Model
{

		/**
		 * Function to check if a user can access the results of a form
		 * @param  int $formId id of the form
		 * @return bool | string    If access is granted the title of the form is returned
		 */
		public function canSeeResults($formId){
			$userId = $this->session->userIDinDB;
			$this->load->database();
			$userId = $this->db->escape($userId);
			$formId = $this->db->escape($formId);

			$queryString = "SELECT title FROM userFormRightsRecap, Forms WHERE userFormRightsRecap.formId = Forms.formId AND userId = $userId AND userFormRightsRecap.formid = $formId AND canseeresults='true';";

			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}

			$result = $this->result;

			if (count($result)>0) {
					return $this->result[0]->title;
			} else {
				return false;
			}

		}



		/**
		* Function to check if a user can administrate a form
		 * @param  int $formId id of the form
		 * @return std object        Returns the form info if access is granted
		 */
		public function canAdministrate($formId){
			$userId = $this->session->userIDinDB;
			$this->load->database();
			$userId = $this->db->escape($userId);
			$formId = $this->db->escape($formId);

			$queryString = "SELECT * FROM Forms WHERE creator = $userId AND formid = $formId AND draft='false';";

			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}

			$result = $this->result;

			if (count($result)>0) {
					return $result[0]->title;
			} else {
				return false;
			}

		}


		/**
		 * Function to generate the list of the forms that cn be administrate (administrate and draft separated)
		 * @return array
		 */
		public function administrate(){
			$forms = array();
			$userId = $this->session->userIDinDB;
			$this->load->database();
			$userId = $this->db->escape($userId);

			$queryString = "SELECT formId, title FROM Forms WHERE creator = $userId AND draft='false';";
			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}
			$forms['finished'] = $this->result;


			$queryString = "SELECT formId, title FROM Forms WHERE creator = $userId AND draft='true' AND tmp='false';";

			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}
			$forms['draft'] = $this->result;


			return $forms;
		}



		/**
		 * Function to generate a list of the forms that the user can access the results of
		 * @return array
		 */
		public function seeResults(){
			$seeResults = array();
			$userId = $this->session->userIDinDB;
			$this->load->database();
			$userId = $this->db->escape($userId);

			$queryString = "SELECT Forms.formid, forms.title FROM Forms WHERE Forms.draft = 'false' AND forms.creator = $userId ORDER BY Forms.title ASC;";
			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}
			$seeResults['yours'] = $this->result;


			$queryString = "SELECT userFormRightsRecap.formid, forms.title FROM userFormRightsRecap, Forms WHERE userFormRightsRecap.formId = Forms.formId AND userFormRightsRecap.userid = $userId AND userFormRightsRecap.isCreator = 'false' AND userFormRightsRecap.canseeresults = 'true' AND Forms.draft = 'false' ORDER BY Forms.title ASC;";

			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}
			$seeResults['others'] = $this->result;


			return $seeResults;

		}


		/**
		 * Function for getting the results of a form
		 * @param  int $formId id of the form
		 * @return array
		 */
		public function getResults($formId){
			$this->load->database();
			$formId = $this->db->escape($formId);



			$queryString = "SELECT userInfo, results FROM FormSubmissions WHERE formid = $formId";
			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}

			return $this->result;
		}



		/**
		 * Function for updating form infos
		 * @param  int $formid    id of the form
		 * @param  bool $publicSource is the source of the form public
		 * @param  TIMESTAMP $closeDate   Date for closing the form
		 * @param  array $accessGroups list of the groupid accessing the form
		 * @param  array $resultGroups list of the groupid that can access results
		 */
		public function administrateForm($formid, $publicSource, $closeDate, $accessGroups, $resultGroups){
				$this->load->database();
				$formid = $this->db->escape($formid);
				$publicSource = $this->db->escape($publicSource);
				$closeDate = $this->db->escape($closeDate);

				$queries = array();

				$queries[]="UPDATE Forms SET publicjsonsource=$publicSource, closeDate = $closeDate WHERE formid = $formid;";

				$queries[]="DELETE FROM FormAccess WHERE formId = $formid;";
				foreach ($accessGroups as $group) {
						$tmp = $this->db->escape($group);
						$queries[]="INSERT INTO FormAccess(groupid,formid) VALUES ($tmp,$formid) ON CONFLICT DO NOTHING;";
				}


				$queries[]="DELETE FROM GroupFormRelations WHERE formId = $formid AND canSeeResults='true';";
				foreach ($resultGroups as $group) {
						$tmp = $this->db->escape($group);
						$queries[]="INSERT INTO GroupFormRelations(groupid,formid,canSeeResults,canAdministrate) VALUES ($tmp,$formid,'true','false') ON CONFLICT DO NOTHING;";
				}

				if (self::makeTransaction($queries) === false) {
						// sql error handling
						return $this->userErrorCode;
				}
				return true;

		}


		/**
		 * get the groups the user can access (he is either creator of the groups or belong to the group)
		 *
		 * TODO redoundant with the function below ?
		 */
		public function getGroups(){
			$userid = $this->session->userIDinDB;

			$this->load->database();
			$userid = $this->db->escape($userid);

			$queryString = "SELECT * FROM activeGroups WHERE creator=$userid AND title != '' ORDER BY title ASC;";

			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}
			$groups = array();
			$groups['yours'] = $this->result;


			$queryString = "SELECT * FROM activeGroups, UserGroupAssocations WHERE creator!=$userid AND userid = $userid AND title != '' ORDER BY activeGroups.title ASC;";

			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}
			$groups['others'] = $this->result;

			return $groups;
		}


		/**
		 * Get infos about the groups the user belong to
		 */
		public function getTheGroupsYouBelongTo(){
			$userid = $this->session->userIDinDB;

			$this->load->database();
			$userid = $this->db->escape($userid);

			$queryString = "SELECT * FROM groupsAndParticipantsLogin WHERE userid=$userid AND creator!=$userid ORDER BY groupsAndParticipantsLogin.title ASC;";


			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}

			return $this->result;
		}


		/**
		 * Function to list the groups that can access results
		 * @param  int $formId id of the form
		 */
		public function groupsAccessingResults($formId){

			$this->load->database();
			$formId = $this->db->escape($formId);
			$queryString = "SELECT groupid FROM GroupFormRelations WHERE GroupFormRelations.formid = $formId AND canSeeResults = 'true';";


			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}

			$result = $this->result;

			$tmp = array();
			foreach ($result as $group) {
				$tmp[$group->groupid] = true;
			}

			return $tmp;
	}


}
