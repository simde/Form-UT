<?php
include_once(APPPATH . 'core/My_Model.php');
/**
 * Class for handling database manipulations related to form creation (and some functions are also used in other steps).
 */
class FormCreation_model extends MY_Model
{


		/**
		 * Function for creating a form in the db
		 * @param  int  $creator          id of the user in the db TODO remove and replacea inside the function
		 * @param  string $infoJSON       Ohter form related json to insert in the db
		 * @param  integer $anonymous      What value of anonymous do we want ?
		 * @param  boolean $formJSON        Json of the form architecture
		 * @param  string  $draft            Is the form in draft mode ?
		 * @param  boolean $closeDate        closedate for the form
		 * @param  string  $publicResult     Are the results public TODO functionnality not used currently
		 * @param  string  $publicJSONsource  Is the form source widely available
		 * @param  blob $finalResult      blob containing the final results TODO functionnality not used currently
		 * @param  bool  $tmp            Is the form in tmp mode
		 */
		public function createForm($creator, $infoJSON = false, $anonymous = 2, $formJSON = false, $draft = 'true', $closeDate = false, $publicResult = 'false', $publicJSONsource = 'false', $finalResult = false, $tmp='true')
		{

			$this->load->database();

			// TODO clean this !
			// Most of this funciton is useless
			if ($closeDate == false){
				$closeDate = 'NULL';
			} else {
				$closeDate = "TIMESTAMP '$closeDate'";
			}

			if ($finalResult == false){
				$finalResult = 'NULL';
			} else {
				$finalResult =  $this->db->escape($finalResult);
			}

			if ($formJSON == false){
				$formJSON = 'NULL';
			} else {
				$formJSON = $this->db->escape($formJSON);
			}


			if ($infoJSON == false){
				$infoJSON = 'NULL';
			} else {
				$infoJSON = $this->db->escape($infoJSON);
			}

			$queryString = "INSERT INTO Forms(title, infoJSON,creator,formJSON,draft,creationDate,lastModificationDate,closeDate,publicResult,publicJSONsource,anonymous,finalResult,tmp, personnalInfo)
			VALUES ('', $infoJSON,'$creator',$formJSON,$draft,now(),now(),$closeDate,$publicResult,$publicJSONsource,'$anonymous',$finalResult,$tmp,'2');";


			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}


			$secondQueryString = "SELECT formID, creationDate FROM Forms WHERE creator = '$creator' ORDER BY creationDate DESC LIMIT 1;";
			if (self::makeQuery($secondQueryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}

			$result = $this->result;

			$row = $result[0];
			$idOFform = $row->formid;

			$this->session->set_userdata('lastCreatedFormID',$idOFform);
			return $idOFform;
		}


		/**
		 * Function to check if the form is a draft and that the owner is the user
		 * @param  int  $formId  formid
		 * @param  int  $creator 		idOfthe user TODO remove
		 */
		public function isFormADraft($formId,$creator='')
		{
				// We get the supposed creator of the form
				if ($creator === ''){
					$creator = $this->session->userIDinDB;
				}

        $this->load->database();
				$creator = $this->db->escape($creator);
				$formId = $this->db->escape($formId);
        $queryString = "SELECT * FROM draftForms WHERE formID = $formId AND creator=$creator;";


				if (self::makeQuery($queryString) === false) {
						// sql error handling
						return $this->userErrorCode;
				}


				$result = $this->result;

        if (count($result)>0) {
            return $result[0]->title;
        } else {
            return false;
        }
    }


		/**
		 * retreive the form architecture
		 * @param  int  $formId  formid
		 * @return array         JSONarray of the formArchitecture
		 */
		public function getJSONformData($formId)
		{
			$this->load->database();
			$formId = $this->db->escape($formId);
				$queryString = "SELECT formJSON::json FROM Forms WHERE formId = $formId;";

				if (self::makeQuery($queryString) === false) {
						// sql error handling
						return $this->userErrorCode;
				}
				$result = $this->result;

				if (count($result)>0) {
					return json_decode($result[0]->formjson,true);
				} else {
						return false;
				}
		}



		/**
		 * Function to update the form title
		 * @param  string $title  new title
		 * @param  int  $formId  formid
		 */
		public function updateFormTitle($title,$formId)
		{
				$this->load->database();
				$title = $this->db->escape($title);
				$formId = $this->db->escape($formId);
				$queryString = "UPDATE Forms SET title=$title, tmp='false' WHERE formID = $formId;";

				if (self::makeQuery($queryString) === false) {
						// sql error handling
						return $this->userErrorCode;
				}
				return true;
		}


		/**
		 * Function for updating the form infos
		 * @param  int  $formId  formid
		 * @param  int $anonymous    anonymous value for the form
		 * @param  int $cas          What info from the cas are asked to the those who respond to the form ?
		 * @param  TIMESTAMP $closeDate    close date for the form
		 * @param  array $accessGroups list of the groups ids that can access the form
		 */
		public function updateForm($formid, $anonymous,$cas,$closeDate,$accessGroups){
				$this->load->database();
				$formid = $this->db->escape($formid);
				$anonymous = $this->db->escape($anonymous);
				$cas = $this->db->escape($cas);
				$closeDate = $this->db->escape($closeDate);

				$queries = array();

				$queries[]="UPDATE Forms SET anonymous=$anonymous, personnalInfo=$cas, closeDate = $closeDate WHERE formid = $formid;";
				$queries[]="DELETE FROM FormAccess WHERE formId = $formid;";

				foreach ($accessGroups as $group) {
						$tmp = $this->db->escape($group);
						$queries[]="INSERT INTO FormAccess(groupid,formid) VALUES ($tmp,$formid) ON CONFLICT DO NOTHING;";
				}

				if (self::makeTransaction($queries) === false) {
						// sql error handling
						return $this->userErrorCode;
				}
				return true;

		}


		/**
		 * Function for updating the form architecture
		 * @param  string $JSON   the json string containing the architecture
		 * @param  int  $formId  formid
		 */
		public function updateJSONsource($JSON,$formId)
		{
			$this->load->database();
			$JSON = $this->db->escape($JSON);

			$queryString = "UPDATE Forms SET formjson=$JSON, tmp=false WHERE formID = '$formId';";

			if (self::makeQuery($queryString) === false) {
					return $this->userErrorCode;
			}
			return true;
		}

		/**
		 * Function to list the formId that a user can get inspired of (access the formArchitecture)
		 * @return array
		 */
		public function getInspired()
		{
				$result = array('yourself'=>array(),'others'=>array());
			 	$this->load->database();
			 	$userId = $this->session->userIDinDB;
				$userId = $this->db->escape($userId);

				$queryString = "SELECT * FROM getInspiredByEveryone WHERE creator != $userId;";

				if (self::makeQuery($queryString) === false) {
						// sql error handling
						return $this->userErrorCode;
				}
				$result['others'] = $this->result;

				$queryString = "SELECT * FROM getInspiredByYourself WHERE creator = $userId;";

				if (self::makeQuery($queryString) === false) {
						// sql error handling
						return $this->userErrorCode;
				}
				$result['yourself'] = $this->result;

				return $result;
		}


		/**
		 * Access to the form informations
		 * TODO move this to the form model
		 * @param  int  $formId  formid
		 */
		public function getFormInfo($formId)
		{
				$this->load->database();
				$formId = $this->db->escape($formId);

				$queryString = "SELECT title, closedate, anonymous, personnalInfo, publicjsonsource FROM Forms WHERE formid = $formId;";

				if (self::makeQuery($queryString) === false) {
						// sql error handling
						return $this->userErrorCode;
				}
				return $this->result[0];

		}

		/**
		 * Function to publish a form (setting draft to false)
		 * @param  int  $formId  formid
		 */
		public function publishForm($formId)
		{
				$this->load->database();
				$formId = $this->db->escape($formId);
				$queryString = "UPDATE Forms SET draft='false' WHERE formID = $formId;";

				if (self::makeQuery($queryString) === false) {
						// sql error handling
						return $this->userErrorCode;
				}
				return true;
		}


}
