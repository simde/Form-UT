<?php
include_once(APPPATH . 'core/My_Model.php');
/**
 * Class for handling database manipulations related to forms in general
 */
class Form_model extends MY_Model
{

		/**
		 * Function for cleaning the database : removing the tmp form
		 */
		public function cleanTmpForms()
		{
			$creator = $this->session->userIDinDB;
			$this->load->database();
			$creator = $this->db->escape($creator);
			$queryString = "DELETE FROM Forms WHERE creator = $creator AND tmp = true;";

			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}
			return true;
		}

}
