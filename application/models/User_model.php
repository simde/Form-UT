<?php
include_once(APPPATH . 'core/My_Model.php');

/**
 * Class for handling database manipulations related to users.
 */
class User_model extends MY_Model
{
		/**
		 * Function to check if a user identified by its login is already inside the database
		 * @param  string  $login login of the Users
		 * @return boolean | integer        Returns the userID in the database corresponding
		 *                   								to the login or false, if there is no user corresponding
		 */
    public function isUserInsideDatabase($login)
    {
        $this->load->database();
				$login = $this->db->escape($login);
        $query = "SELECT * FROM Users WHERE login = $login;";
        $query = $this->db->query($query);
        if ($query == false) {
            echo "erreur user_model";
        } elseif ($query->num_rows()>0) {
            $row = $query->row();
            return array('id'=>$row->userid,'language'=>$row->language);
        } else {
            return false;
        }
    }

		/**
		 * Function to insert or update user info in the database after logging in
		 *
		 * @return string the id of the user in the database
		 */
    public function loginOrUpdate()
    {
        $userData = $this->session->get_userdata();
        $this->load->database();

				$login = $this->session->userName;
				$databaseInfo = self::isUserInsideDatabase($login);


				$loginESC = $this->db->escape($userData['userName']);
				$mailESC = $this->db->escape($userData['userEmail']);
				$typeESC = $this->db->escape($userData['userAccountProfile']);
        if ($databaseInfo != false) {
						$userIDinDB = $databaseInfo['id'];
            $query = $this->db->query("UPDATE Users SET email = $mailESC, type = $typeESC WHERE userID = $userIDinDB;");
        } else {
            $query = $this->db->query("INSERT INTO Users(login,email,type,language) VALUES ($loginESC,	$mailESC, $typeESC, 'fr_FR');");
            var_dump($query);
            echo "insert";

						$databaseInfo = self::isUserInsideDatabase($login);
        }
				$userIDinDB = $databaseInfo['id'];
				$userLanguage = $databaseInfo['language'];
				// We keep this data for later use.
				$this->session->set_userdata('userIDinDB', $userIDinDB);
				$this->session->set_userdata('userLanguage', $userLanguage);

				return $userIDinDB;

    }


		/**
		 * Function to update user language preferences
		 * @return bool or error 
		 */
		public function updateUserLanguage()
		{
				$userId = $this->session->userIDinDB;
				$userLanguage = $this->session->userLanguage;

				$this->load->database();
				$userId = $this->db->escape($userId);
				$userLanguage = $this->db->escape($userLanguage);

				$queryString = "UPDATE Users SET language=$userLanguage WHERE userId = $userId;";

				if (self::makeQuery($queryString) === false) {
						// sql error handling
						return $this->userErrorCode;
				}
				return true;
		}
}
