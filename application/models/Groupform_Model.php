<?php
include_once(APPPATH . 'core/My_Model.php');
/**
 * Class for handling database manipulations related to users.
 */
class Groupform_model extends MY_Model
{


		/**
		 * Function for generating the lists of groups that the user can shoose from for form access
		 * @param  int  $formId id of the form
		 * @param  boolean $strict if strict is set to true, then we cannot remove groups that have already gain access
		 * @return array   Array of the groups, one key for main groups, other key for ....
		 */
    public function accessGroupForm($formId,$strict = true)
    {
        $result = array('mainGroups'=>array(),'otherAdministrativeGroups'=>array(),'yourGroups'=>array(),'otherGroupsYouBelongTo'=>array());
        $this->load->database();
        $userId = $this->session->userIDinDB;
        $userId = $this->db->escape($userId);
        $formId = $this->db->escape($formId);

				if ($strict === true){
					$queryString1 = "SELECT groupid, title FROM mainGroups WHERE groupId NOT IN ( SELECT groupId FROM FormAccess WHERE formId=$formId AND groupId IS NOT NULL) ORDER BY groupId ASC;";
					$queryString2 = "SELECT groupid, title FROM activeGroups WHERE creator = $userId AND groupId NOT IN ( SELECT groupId FROM FormAccess WHERE formId=$formId AND groupId IS NOT NULL)  ORDER BY title ASC;";
				 	$queryString3 = "SELECT activeGroups.groupid, title FROM activeGroups, UserGroupAssocations WHERE UserGroupAssocations.groupid = activeGroups.groupid AND activeGroups.creator != $userId AND UserGroupAssocations.userId = $userId AND activeGroups.groupId NOT IN ( SELECT groupId FROM FormAccess WHERE formId=$formId AND groupId IS NOT NULL)  ORDER BY title ASC;";
				} else {
					$queryString1 = "SELECT groupid, title FROM mainGroups ORDER BY groupId ASC;";
					$queryString2 = "SELECT groupid, title FROM activeGroups WHERE creator = $userId  ORDER BY title ASC;";
					$queryString3 = "SELECT activeGroups.groupid, title FROM activeGroups, UserGroupAssocations WHERE UserGroupAssocations.groupid = activeGroups.groupid AND activeGroups.creator != $userId AND UserGroupAssocations.userId = $userId ORDER BY title ASC;";
				}
        if (self::makeQuery($queryString1) === false) {
            // sql error handling
                        return $this->userErrorCode;
        }
        $result['mainGroups'] = $this->result;


        if (self::makeQuery($queryString2) === false) {
            // sql error handling
                        return $this->userErrorCode;
        }
        $result['yourGroups'] = $this->result;



				if (self::makeQuery($queryString3) === false) {
						// sql error handling
												return $this->userErrorCode;
				}
				$result['otherGroupsYouBelongTo'] = $this->result;


        return $result;
    }


		/**
		 * Function to list the groups currently accessing the form
		 * @param  int  $formId id of the form
		 * @return array  array where the key are the groupid that can access the group
		 */
		public function groupsAccessingForm($formId){

			$this->load->database();
			$formId = $this->db->escape($formId);
			$queryString = "SELECT groupid FROM FormAccess WHERE FormAccess.formid = $formId;";


			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}

			$result = $this->result;

			$tmp = array();
			foreach ($result as $group) {
				$tmp[$group->groupid] = true; //true is just a dummy value
			}

			return $tmp;
	}

}
