<?php
include_once(APPPATH . 'core/My_Model.php');
/**
 * Class for handling database manipulations related to groups.
 */
class Group_model extends MY_Model
{
		/**
		 * Simple group creation function
		 * @return int the id of the created group
		 */
		public function createGroup(){

				$userId = $this->session->userIDinDB;
				$this->load->database();
				$userId = $this->db->escape($userId);

				$queryString = "INSERT INTO Groups(title,creator,active,type) VALUES ('',$userId,true,'');";

				if (self::makeQuery($queryString) === false) {
						// sql error handling
						return $this->userErrorCode;
				}

				return $this->db->insert_id();
		}



		/**
		 * Function to check ig the user is the owner of a groups
		 * @param  int  $groupId id of the group
		 * @return boolean | error
		 */
		public function isUserOwner($groupId){
			// We get the supposed creator of the form

				$creator = $this->session->userIDinDB;


			$this->load->database();
			$creator = $this->db->escape($creator);
			$groupId = $this->db->escape($groupId);
			$queryString = "SELECT * FROM Groups WHERE groupId = $groupId AND creator=$creator;";


			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}

			$result = $this->result;

			if (count($result)>0) {
					return array('title'=>$result[0]->title, 'active'=>$result[0]->active);
			} else {
					return false;
			}
		}


		/**
		 * Simple function to check if the user is inside a group
		 * @param  int  $groupId id of the group
		 */
		public function isUserInsideGroup($groupId){
			// We get the supposed creator of the form

			$userid = $this->session->userIDinDB;


			$this->load->database();
			$userid = $this->db->escape($userid);
			$groupId = $this->db->escape($groupId);

			$queryString = "SELECT * FROM groupsAndParticipantsLogin WHERE groupId = $groupId AND userId=$userid;";


			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}

			$result = $this->result;

			if (count($result)>0) {
					return true;
			} else {
					return false;
			}
		}

		/**
		 * Function to retreive the info a group
		 * @param  int  $groupId id of the group
		 */
		public function getGroupInfo($groupId){
			// We get the supposed creator of the form

				$this->load->database();
				$groupId = $this->db->escape($groupId);
				$queryString = "SELECT Groups.title, Groups.active, Users.login FROM Groups, Users WHERE Users.userid = groups.creator AND groupId = $groupId;";


			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}

			$result = $this->result;

			return $result;
		}


		/**
		 * Function to get the groups where the user is the owner
		 * @return array of std element
		 */
		public function getYourGroups(){
			$userid = $this->session->userIDinDB;

			$this->load->database();
			$userid = $this->db->escape($userid);

			$queryString = "SELECT * FROM Groups WHERE creator=$userid AND groups.title != '' ORDER BY Groups.title ASC;";


			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}

			return $this->result;
		}


		/**
		 * Function to retreive the lists of the groups you belong to (groups created by others)
		 * @return array
		 */
		public function getTheGroupsYouBelongTo(){
			$userid = $this->session->userIDinDB;

			$this->load->database();
			$userid = $this->db->escape($userid);

			$queryString = "SELECT * FROM groupsAndParticipantsLogin WHERE userid=$userid AND creator!=$userid ORDER BY groupsAndParticipantsLogin.title ASC;";


			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}

			return $this->result;
		}


		/**
		 * Function to list the participants and logins of the member of group
		 * @param  int  $groupId id of the group
		 */
		public function listParticipantLogin($groupId){
			// We get the supposed creator of the form

				$creator = $this->session->userIDinDB;


				$this->load->database();
				$groupId = $this->db->escape($groupId);
				$queryString = "SELECT login FROM groupsAndParticipantsLogin WHERE groupId = $groupId ORDER BY login ASC;";


			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}

			$result = $this->result;

			return $result;
		}

		/**
		 * Function for updating a group in the db
		 * @param  int  $groupId id of the group
		 * @param  string $newTitle              new title for the group
		 * @param  array $newArrayOfParticipant Array of all the participants id
		 * @param  bool $active                is the group active
		 */
		public function updateGroup($groupId,$newTitle,$newArrayOfParticipant,$active){

				$this->load->database();
				$groupId = $this->db->escape($groupId);
				$newTitle = $this->db->escape($newTitle);

				if ($active === true){
					$active = 'true';
				} else {
					$active = 'false';
				}

				$queries = array();


				$queries[]="UPDATE Groups SET title=$newTitle, active=$active WHERE groupid = $groupId;";
				$queries[]="DELETE FROM UserGroupAssocations WHERE groupid =$groupId;";

				foreach ($newArrayOfParticipant as $participant) {
						$tmp = $this->db->escape($participant);
						$queries[]="INSERT INTO UserGroupAssocations(groupid,userId) VALUES ($groupId,$tmp) ON CONFLICT DO NOTHING;";
				}

				if (self::makeTransaction($queries) === false) {
						// sql error handling
						return $this->userErrorCode;
				}
				return true;
		}


		/**
		 * Function for automaticly creating a user in the db if he is listed in a group
		 * TODO When will have access to the CAS remove this functionnality !
		 * @param  array $logins array of logins
		 */
		public function makeSureLoginsAreInDb($logins){
			if (count($logins)===0) return true;
			$this->load->database();

			$queries = array();

			foreach ($logins as $login) {
					$tmp = $this->db->escape($login);
					$queries[]="INSERT INTO Users(login) VALUES ($tmp) ON CONFLICT DO NOTHING;";
			}

			if (self::makeTransaction($queries) === false) {
					// sql error handling
					return $this->userErrorCode;
			}
			return true;
		}


		/**
		 * Simple function for cleaning the db and removing tmp groups
		 */
		public function cleanGroups()
		{
			$this->load->database();
			$userId = $this->session->userIDinDB;
			$userId = $this->db->escape($userId);

			$queryString = "DELETE FROM Groups WHERE creator = $userId AND title = '';";

			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}
			return true;
		}
}
