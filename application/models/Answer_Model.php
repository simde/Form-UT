<?php
include_once(APPPATH . 'core/My_Model.php');
/**
 * Class for handling database manipulations related to form answering.
 */
class Answer_Model extends MY_Model
{

	/**
	 * Checks if a user can answer a form
	 * @param  int  $formId  formid
	 * @return bool | array    if can access it returns some form info to use
	 */
		public function canAccessForm($formId){
			$userId = $this->session->userIDinDB;
			$this->load->database();
			$userId = $this->db->escape($userId);
			$formId = $this->db->escape($formId);
			$queryString = "SELECT forms.title, users.login FROM Users, Forms, FormAccess, UserGroupAssocations WHERE Users.userId = forms.creator AND Forms.formid = FormAccess.formid AND FormAccess.groupid = UserGroupAssocations.groupid AND UserGroupAssocations.userid=$userId AND FormAccess.formId=$formId AND Forms.closeDate>now();";

			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}

			$result = $this->result;

			if (count($result)>0) {
					return array('title'=>$result[0]->title, 'creator'=>$result[0]->login);
			}

			// on vérifie si ce n'est pas le créateur lui-même :
			$queryString = "SELECT forms.title, users.login FROM Users, Forms WHERE Users.userId = forms.creator AND forms.creator =$userId AND forms.formid = $formId AND forms.closeDate>now();";
			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}

			$result = $this->result;

			if (count($result)>0) {
					return array('title'=>$result[0]->title, 'creator'=>$result[0]->login);
			}	else {
					return false;
			}
		}


		/**
		 * Checks if a user has answered a form
		 * @param  int  $formId  formid
		 * @return boolean | int  It returns the submission id (might be null) if the user has already answered the form
		 */
		public function hasAnsweredForm($formId){
			$userId = $this->session->userIDinDB;

			$this->load->database();
			$userId = $this->db->escape($userId);
			$formId = $this->db->escape($formId);

			$queryString = "SELECT submissionId, formId FROM FormSubmitted WHERE FormSubmitted.userId = $userId AND FormSubmitted.formid = $formId;";

			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}

			$result = $this->result;

			if (count($result)>0) {
					return $result[0]->submissionid;
			} else {
					return false;
			}

		}


		/**
		 * Function to get the uid of a submission
		 * @param  int $submissionId id of th the submission
		 */
		public function getuid($submissionId){

			$this->load->database();
			$submissionId = $this->db->escape($submissionId);

			$queryString = "SELECT uid FROM FormSubmissions WHERE FormSubmissions.submissionId = $submissionId;";

			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}

			$result = $this->result;

			if (count($result)>0) {
					return $result[0]->uid;
			} else {
					return false;
			}

		}


		/**
		 * Function to check if a uid is valid and return the real submissionId
		 * @param  int  $formId  formid
		 * @param  string $uid
		 * @return bool | the submissionId
		 */
		public function checkUid($formId,$uid){

			$this->load->database();
			$formId = $this->db->escape($formId);
			$uid = $this->db->escape($uid);


			$queryString = "SELECT submissionId, formId FROM FormSubmissions WHERE formid = $formId AND uid = $uid;";

			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}

			$result = $this->result;

			if (count($result)>0) {
					return $result[0]->submissionid;
			} else {
					return false;
			}

		}



		/**
		 * Function to init a form submission
		 * TODO add subgroup CAS when available
		 * @param  int  $formId  formid
		 * @param  string $uid
		 * @param  boolean $anonymous              is the submission anonymous
		 * @param  boolean $anonymousPersonnalInfo  Are the personnal data stored
		 * @return int                          the submissionid
		 */
		public function initFormSubmission($formId, $uid, $anonymous = true, $anonymousPersonnalInfo = true){

			$userId = $this->session->userIDinDB;
			$this->load->database();
			$userId = $this->db->escape($userId);
			$formId = $this->db->escape($formId);
			$uid = $this->db->escape($uid);


			$info = array();

			if($anonymous === true){
				$info['userName'] = 'anonymous';
				$info['userDisplayName'] = 'anonymous';
				$info['userEmailAdress'] = 'anonymous';
			} else if ($anonymous === false){
				$info['userName'] = $this->session->userName;
				$info['userDisplayName'] = $this->session->userDisplayName;
				$info['userEmailAdress'] = $this->session->userEmail;
			}

			if ($anonymousPersonnalInfo === false){
				$info['userCasType'] = $this->session->userAccountProfile;
				$info['userSubType'] = "Form-ut n'a pas encore accès à cette info...";
			}else if ($anonymousPersonnalInfo === true){
				$info['userCasType'] = 'anonymous';
				$info['userSubType'] = 'anonymous';
			}
			$info = $this->db->escape(json_encode($info));

			$queryString = "INSERT INTO FormSubmissions(formId,uId,answers,results,userInfo,draft) VALUES ($formId,$uid,'{}','{}',$info,'true');";
			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}

			$formSubmissionIdForReturn = $this->db->insert_id();

			if ($anonymous === true){
				$formSubmissionId = 'NULL';
			} else {
				$formSubmissionId = $formSubmissionIdForReturn;
			}

			$queryString = "INSERT INTO FormSubmitted(userId,formId,submissionId) VALUES ($userId,$formId,$formSubmissionId) ON CONFLICT DO NOTHING;";
			if (self::makeQuery($queryString) === false) {

					return $this->userErrorCode;
			}

			return $formSubmissionIdForReturn;
		}




		/**
		 * Function for updating a FormSubmission
		 * @param  int $submissionId id of the form submission
		 * @param  string $results      json string containing the results (serialized from JS saveformlib)
		 * @param  string $answers      json string containing the exact state of the form submission for easy later repolution
		 */
		public function updateFormSubmission($submissionId, $results, $answers){
			$this->load->database();
			$submissionId = $this->db->escape($submissionId);
			$results = $this->db->escape(json_encode($results));
			$answers = $this->db->escape(json_encode($answers));


			$queryString = "UPDATE FormSubmissions SET results = $results, answers = $answers, draft = true WHERE submissionId = $submissionId";
			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}

			return true;
		}


		/**
		 * Function for getting the content of a form submission
		 * @param  int $submissionId id of the form submission
		 * @return stdArray
		 */
		public function getFormSubmission($submissionId){
			$this->load->database();
			$submissionId = $this->db->escape($submissionId);

			$queryString = "SELECT * FROM FormSubmissions WHERE submissionId = $submissionId";
			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}

			$result = $this->result;

			if (count($result)>0) {
					return $result;
			} else {
					return false;
			}
		}






		/**
		 * Function for retreiving the FormSubmissions results
		 * @param  int $submissionId id of the form submission
		 */
		public function getFormAnswers($submissionId){
			$this->load->database();
			$submissionId = $this->db->escape($submissionId);

			$queryString = "SELECT answers FROM FormSubmissions WHERE submissionId = $submissionId";
			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}


			$result = $this->result;

			if (count($result)>0) {
					return json_decode($result[0]->answers,true);
			} else {
					return false;
			}
		}



		/**
		 * Function for getting the form info
		 *  TODO redoundant function !
		 * @param  int $formId id of the form
		 */
		public function getFormInfo($formId){
			$this->load->database();
			$formId = $this->db->escape($formId);

			$queryString = "SELECT * FROM Forms WHERE formid = $formId";
			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}


			$result = $this->result;

			if (count($result)>0) {
					return $result[0];
			} else {
					return false;
			}
		}




		/**
		 * Function for publishing a formsubmission (set draft to false)
		 * @param  int $submissionId id of the form submission
		 */
		public function publish($submissionId){
			$this->load->database();
			$submissionId = $this->db->escape($submissionId);

			$queryString = "UPDATE FormSubmissions SET draft = false WHERE submissionid = $submissionId;";
			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}
			return true;
		}


		/**
		 * Function to list the forms that a user can answer
		 * @return array
		 */
		public function getTheFormsYouCanAnswer(){
			$formsYouCanAnswer = array();
			$userId = $this->session->userIDinDB;
			$this->load->database();
			$userId = $this->db->escape($userId);


			$queryString = "SELECT Forms.formid, forms.title, users.login, forms.closeDate, forms.closeDate>now() AS open FROM Users, Forms, FormAccess, UserGroupAssocations WHERE Forms.draft = 'false' AND Users.userId = forms.creator AND Forms.formid = FormAccess.formid AND FormAccess.groupid = UserGroupAssocations.groupid AND UserGroupAssocations.userid=$userId AND Forms.creator != $userId ORDER BY  Forms.title ASC;";


			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}
			$formsYouCanAnswer['others'] = $this->result;

			$queryString = "SELECT Forms.formid, forms.title, forms.closeDate, forms.closeDate>now() AS open FROM Forms WHERE Forms.draft = 'false' AND forms.creator = $userId ORDER BY Forms.title ASC;";
			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}
			$formsYouCanAnswer['yours'] = $this->result;

			return $formsYouCanAnswer;

		}



		/**
		 * Function to list the forms that the user have answred
		 */
		public function formsYouHaveAnswered(){
			$userId = $this->session->userIDinDB;
			$this->load->database();
			$userId = $this->db->escape($userId);


			$queryString = "SELECT FormSubmitted.formid, FormSubmitted.submissionId IS NULL AS anonymous, draft FROM FormSubmitted LEFT JOIN FormSubmissions ON FormSubmitted.submissionId = FormSubmissions.submissionId WHERE userId = $userId;";


			if (self::makeQuery($queryString) === false) {
					// sql error handling
					return $this->userErrorCode;
			}
			$tmp = $this->result;

			$data = array();

			foreach ($tmp as $form) {
				$data[$form->formid] = $form->draft;
			}

			return $data;

		}

}
