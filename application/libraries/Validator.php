<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Class for validating both created forms and forms submissions
 */
class Validator
{

	/**
	 * Returns a simple error message
	 * @param  string $message more info on the error
	 * @return string       the full error message
	 */
	private function error($message){
			return "La validation server a échoué : \n".$message;
	}

	/**
	 * Returns a bool (true) if the validation was successfull
	 * @return bool Is the validation successfull (true)
	 */
	private function success(){
		return true;
	}



	///////////////////////////////////////////////////////////////////////////::
	///////////////////////////////////////////////////////////////////////////::
	///////////////////////////////////////////////////////////////////////////::
	///////////////////////////////////////////////////////////////////////////::
	///////////////////////////////////////////////////////////////////////////::
	///////////////////////////////////////////////////////////////////////////::
	///////////////////////////////////////////////////////////////////////////::
	///////////////////////////////////////////////////////////////////////////::


	/**
	 * Validation of the created form
	 * @param  arrray  $form  formdata
	 * @param  boolean $admin is it the admin asking for validation ?
	 * @return bool | string      is the form validation successfull
	 */
	public function formCreatedValidation($form, $admin = false){
		if (!is_array($form)) return self::error("Le formulaire n'a pas le bon format, un tableau est attendu.");

		$names = array();

		foreach ($form as $formElement){
			$test = true;
			if ((!isset($formElement['type'])) or (!isset($formElement['name']))){
				return self::error("Un élément du formulaire ne contient pas de champs 'name' ou 'type'. Ces champs sont essentiels au bon fonctionnement de l'application. Si vous n'êtes pas en mode expert, contacter les administrateurs, ne vous inquiétez pas les données sont quand mêmes enregistrées, vous ne pouvez juste pas aller plus loin dans la création.");
 			}
			$name = $formElement['name'];

			if (isset($names[$name])){
				return self::error("Vous ne pouvez pas utiliser deux fois le même champ 'nom', élément concerné : ".$formElement['name']);
			} else {
				$names[$name] = true;
			}

			if ($name === 'userName' or $name === 'userDisplayName' or $name === 'userEmailAdress' or $name === 'userCasType' or $name === 'userSubType'){
				return self::error("Le nom $name est réservé pour l'application, merci d'en choisir un autre.");
			}

			$type = $formElement['type'];
			if ($type === "adminSelect" and $admin === false){
				return self::error("Vous ne pouvez pas utiliser la fonctionnalité admin select, champ concerné : ".$formElement['name']);
			} else if ($type === "adminSelect" and $admin === true){
				//on ne fait rien
			} else if ($type === "text" or $type === "textarea" or $type === "date" or $type === "number" or $type === 'likertScale' or $type ==='select' or $type === 'checkbox-group' or $type==='radio-group'){
				if (!isset($formElement['label']))	return self::error("L'élément ".$formElement['name']." n'a pas de Label, ce n'est pas une bonne chose...");
				$label = $formElement['label'];
				if ($type === 'select' or $type ==='checkbox-group' or $type === 'radio-group'){
						if (!isset($formElement['values'])) return self::error("Ils manquent des valeurs pour le champ :". $formElement['label'] );

						$labels = array();
						$values = array();
						foreach ($formElement['values'] as $option) {
								if((!isset($option['label'])) or (!isset($option['value']))) return self::error("Un label ou une valeur associée à une option manque dans l'élément : ".$formElement['label']);

								$test = self::checkNotEmpty($option['value']);
								if ($test === false) return self::error("Il manque une valeur dans le champs : ".$formElement['label']);

								// check if options and labels are different for coherence...
								if (isset($values[$option['value']])){
									return self::error("Les valeurs doivent tous être différents dans les options ! Éléments concernés : ".$formElement['label']);
								} else {
									$values[$option['value']] = true;
								}

								if (isset($values[$option['label']])){
									return self::error("Les labels doivent tous être différents dans les options ! Éléments concernés : ".$formElement['label']);
								} else {
									$values[$option['label']] = true;
								}

						}

						if (isset($formElement['other']) and $formElement['other'] === true){
							$test = 1;
						} else {
							$test = 2;
						}


						if (count($values)<$test){
							return self::error("Vous n'avez pas assez d'options dans l'élément ".$formElement['label']. ". Pour faire un choix il faut au moins deux options.");
						}

				} // end of checks for groups submits



				if ($type === 'likertScale'){
					if (!isset($formElement['subtype'])) return self::error("Vous n'avez pas sélectionné de sous-type d'échelle de Likert ".$formElement['label'].'!');
					if (!($formElement['subtype']==="withNeutral" or $formElement['subtype']==="withoutNeutral")) return self::error("La propriété de ".$formElement['label']." n'a pas pu être reconnue, vous avez peut être oublié de sélection la version.");
				}

				if ($type === 'text' or $type === 'textarea'){
					$test = self::checkMaxLengthAttribute($formElement);
					if ($test === false){
							return self::error("La longueur maximale n'est pas lisible pour le champ : $label");
					}
				}


				if ($type === 'number'){
					$test = self::checkNumberAttributes($formElement);
					if ($test === false){
							return self::error("Les attributs (options) du champ $label ne sont pas lisibles.");
					}
				}


			} elseif ($type !== 'newPageDelimiter' and $type !== 'paragraph' ) {
				return self::error($type." n'est pas un élément de formulaire reconnu");
			}

		}

		return self::success();

	}


	/**
	 * Simple check for maxlength attribute
	 * @param  array $formElement
	 * @return bool
	 */
	private function checkMaxLengthAttribute($formElement){
		if (!isset($formElement['maxlength'])) return true;
		$max = $formElement['maxlength'];
		intval($max)>0;
	}



	/**
	 * Simple checks for number attributes
	 * @param  array $formElement
	 * @return bool
	 */
	private function checkNumberAttributes($formElement){
		$max = false;
		$min = false;
		if (isset($formElement['max'])) $max = $formElement['max'];
		if (isset($formElement['min'])) $min = $formElement['min'];

		if ($max === false and $min === false) return true;

		if ($max === false and $min !== false){
			return self::checkNumber($min);
		} else if ($max !== false and $min === false){
			return self::checkNumber($max);
		} else {
			$max = self::checkNumber($max);
			$min = self::checkNumber($min);

			if ($max === false or $min === false) return false;

			return $max > $min;
		}
	}




///////////////////////////////////////////////////////////////////////////::
///////////////////////////////////////////////////////////////////////////::
///////////////////////////////////////////////////////////////////////////::
///////////////////////////////////////////////////////////////////////////::
///////////////////////////////////////////////////////////////////////////::
///////////////////////////////////////////////////////////////////////////::
///////////////////////////////////////////////////////////////////////////::
///////////////////////////////////////////////////////////////////////////::


	/**
	 * Validation of a form submission based on the source form
	 * @param  array $results    array containing the results
	 * @param  array  $sourceForm array containing the source form
	 * @return bool | string
	 */
	public function validate($results,$sourceForm){
		if (!is_array($results) or !is_array($sourceForm) ) return self::error("Les résultats transmis n'ont pas le bon format, un tableau est attendu.");

		// simple check
		$copy = $results;
		foreach ($sourceForm as $formElement){
			$type = $formElement['type'];
			if ($type === "adminSelect" or $type === "text" or $type === "textarea" or $type === "date" or $type === "number" or $type === 'likertScale' or $type ==='select' or $type === 'checkbox-group' or $type==='radio-group'){
				if (!isset($results[$formElement['name']])) return self::error("Il manque les données de résultat pour le champ : ".$formElement['label']);
				if (!is_string($results[$formElement['name']])) return self::error("Le résultat n'a pas le bon format pour le champ". $formElement['label'].'Un élément texte est attendu. (si vous voyez ce message contacter les admins)');
				unset($copy[$formElement['name']]);
			}
		}
		if (count($copy) > 0 and !(count($results) === 1 and $results[0] ==='')) return self::error("Il y a trop de valeurs qui ont été transmises lors de la transmission des résultats.");



		foreach ($sourceForm as $formElement){
			$test = true;

			$type = $formElement['type'];
			$name = htmlentities($formElement['name']);
			if ($type === "adminSelect" or $type === "text" or $type === "textarea" or $type === "date" or $type === "number" or $type === 'likertScale' or $type ==='select' or $type === 'checkbox-group' or $type==='radio-group'){
				$result = $results[$name];
				$label = htmlentities($formElement['label']);
				if(isset($formElement['required']) and $formElement['required'] === true){
					$test = self::checkNotEmpty($result);
					if ($test === false) return self::error("Le champ $label est requis !");
				}

				if ($type === 'date'){
					$test = self::checkDate($result);
					if ($test === false) return self::error("Le champ de type date $label n'a pas un format ou un contenu valide.");
				}


				if ($type === 'number'){
					$number = self::checkNumber($result);
					if ($number === false) return self::error("Le champ de type nombre $label n'a pas un format valide.");

					// We have also to check the min and max values
					if (isset($formElement['max'])){
						$max = self::checkNumber($formElement['max']);
						if ($number > $max) return self::error("Le nombre entré dans le champ $label est trop grand : $number > $max");
					}

					if (isset($formElement['min'])){
						$min = self::checkNumber($formElement['min']);
						if ($number < $min) return self::error("Le nombre entré dans le champ $label est trop petit : $number < $min");
					}


				}// End of number check

				if ($type === 'text' or $type === 'textarea'){
					if (isset($formElement['maxlength'])){
						$max = intval($formElement['maxlength']);
						if (strlen($result)>($max+2)){
							return self::error("Le texte entrée dans le champ $label est trop long.");
						}
					}

				}

				if ($type === 'adminSelect' OR $type === 'select' or $type === 'checkbox-group' or $type === 'radio-group' or $type==='likertScale'){
					$test = self::checkGroupInputCoherence($formElement,$result);
					if ($test === false) return self::error("Les informations transmises à propos du champ $label semblent manquer de cohérence.");
				}

			}// end of type check
		} // end of iteration


		return self::success();



	} // end of function


	/**
	 * Function for checking group inputs coherance
	 * @param  array $formElement array containing the source of the element
	 * @param  string $result     the results transmitted
	 * @return bool
	 */
	private function checkGroupInputCoherence($formElement,$result){
		if (isset($formElement['other']) and $formElement['other'] === true) return true; // we can't check anything
		$type = $formElement['type'];

		// we start by building all the possibilities
		$possibilities = array();
		if ($type === 'adminSelect' ){
			foreach ($formElement['groups'] as $group) {
				if (!isset($group['values'])) continue;
				foreach ($group['values'] as $option) {
					$possibilities[$option['value']] = true;
				}

			}

		} else if ($type === 'likertScale'){
			$possibilities['++'] = true;
			$possibilities['--'] = true;
			$possibilities['-'] = true;
			$possibilities['+'] = true;
			if($formElement['subtype'] === 'withNeutral'){
				$possibilities['0'] = true;
			}
		} else {
			foreach ($formElement['values'] as $option) {
				$possibilities[$option['value']] = true;
			}
		}

		if ($type === 'radio-group'){
			$resultSeparated = array($result=>true);
		} else {
			$resultSeparated = self::separateResult($result);
		}
		if ($resultSeparated === false) return false; // duplicate values in result !!

		$numberOfAnswers = 0;
		foreach ($resultSeparated as $key => $value) {
			if (!isset($possibilities[$key])) return false;
			$numberOfAnswers++;
		}
		if ($type === 'checkbox-group') return true;
		// for other type of groups we have to check multiplicity
		if (  (!isset($formElement['multiple']))  && $numberOfAnswers>1) return false;
		if (  (isset($formElement['multiple'])) && $formElement['multiple'] === false  && $numberOfAnswers>1) return false;

		return true;
	}


/**
 * Séparation des résultats, de string vers array
 * @param  string $string the string containing the results for a field
 * @return array         one result per row
 */
public function separateResult($string){
		$re = '/\|([^\|]*)\|/';

		preg_match_all($re, $string, $matches,  PREG_PATTERN_ORDER);

		// Print the entire match result
		$matches = $matches[1];

		$res = array();
		foreach ($matches as $match) {
			if (isset($res[$match])) return false;
			$res[$match] = true;
		}

		return $res;
}


/**
 * Variant of the above function used in the app for separating results.
 */
public function separateResultVariant($string){
		$re = '/\|([^\|]*)\|/';

		preg_match_all($re, $string, $matches,  PREG_PATTERN_ORDER);

		// Print the entire match result
		$matches = $matches[1];

		$res = array();
		foreach ($matches as $match) {
			$res[] = $match;
		}

		return $res;
}


/**
 * Checks if a string is not empty
 * @return bool
 */
private function checkNotEmpty($string){
	if (!is_string($string)) return false;
	if (strlen($string)===0) return false;
	return true;
}


/**
 * Checks if a string correspond to a date
 * @return bool
 */
private function checkDate($string){
	$re = '/^([01-9]{1,2})\/([01-9]{1,2})\/([01-9]{4})$/';
	preg_match($re, $string, $matches);

	if (!isset($matches[0])) return false;

	$day = intval($matches[1]);
	$month = intval($matches[2]);
	$year = intval($matches[3]);

	return checkdate ( $month , $day ,  $year );
}


/**
 * Check if a string is a valid number
 * @return bool
 */
private function checkNumber($string){
	$re = '/^\-?[01-9]\d*(\.\d+)?$/';
	preg_match($re, $string, $matches);

	if (!isset($matches[0])) return false;

	return floatval($string);
}



}
