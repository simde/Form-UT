<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Library used for outputing nice results table
 */
class ResultsBuilder
{

	/**
	 * array containing the headers of the table
	 * @var array
	 */
	private $headers;

	/**
	 * Resulting html
	 * @var string
	 */
	private $html;


	/**
	 * Main function for building resutls
	 * @param  Array  $resultsAndInfos   Array containing the results with info (the two attributes in the SQL table)
	 * @param  array  $source          	Form data
	 * @param  boolean $showExport      Do we enable export from the generating table
	 * @return string                   html table (xss cleaned later)
	 */
	public function buildResults($resultsAndInfos,$source,$showExport = true){
		$this->html = '<table id="results"
		 data-toggle="table"';
		 if ($showExport === true) $this->html .= 'data-show-export="true" ';
		 $this->html .='
		 data-page-list="[10, 25, 50, 100, ALL]"
  		data-search="true"
			data-filter="true"
			 	data-sortable="true"
				data-fixed-columns="true"
				data-mobile-responsive="true"
				data-sort-name="stargazers_count"
				data-show-columns="true"
				data-check-on-init="true"
				data-cell-style="pre"

			 >'."\n";
		$correspondants = array(
			'userName' => 'Login utilisateur',
			'userDisplayName' => 'Nom et Prénom',
			'userEmailAdress' => 'Adresse email',
			'userCasType' => "Catégorie d'utilisateur CAS",
			'userSubType' => "Sous-groupe CAS"
		);

		$this->headers = array(
			0 => 'userName',
			1 => 'userDisplayName',
			2 => 'userEmailAdress',
      3 => 'userCasType',
      4 => 'userSubType'
		);


		$i = 5;

		foreach ($source as $formElement){
			$type = $formElement['type'];
			if ($type === "adminSelect" or $type === "text" or $type === "textarea" or $type === "date" or $type === "number" or $type === 'likertScale' or $type ==='select' or $type === 'checkbox-group' or $type==='radio-group'){
				$correspondants[$formElement['name']] = $formElement['label'];
				$this->headers[$i] = $formElement['name'];
				$i++;
			}
		}

		self::buildHeader($correspondants);
		$this->html .="\t<tbody>\n";

		foreach ($resultsAndInfos as $answer) {
			$re = '/[^\\\\](\\\\n)/'; //get the new line back !
			$tmp = preg_replace($re,"&#32;<br>&#32;",$answer->results);
			$userinfo = json_decode($answer->userinfo,true);
			$answers = json_decode($tmp,true);

			//transfert des infos perso
			foreach ($userinfo as $key => $value) {
				$answers[$key] = $value;
			}

			self::buildRow($answers);
		}

		$this->html .="\t</tbody>\n";
		$this->html .= '</table>';
		return $this->html;
	}


	/**
	 * Function to build a row of the table based on the results
	 * @param  array $results 	one result and info
	 */
	private function buildRow($results){
		$tmp = "\t\t<tr> \n";

		foreach ($this->headers as $key => $name) {
				$tmp.="\t\t\t".'<td style="white-space: pre;"'.">".$results[$name]."</td>\n";
		}
		$tmp .= "\t\t</tr> \n";

		$this->html.=$tmp;
	}

	/**
	 * Function to build the headers of the table based on the results
	 * @param  array $results 	one result and info
	 */
	private function buildHeader($results){
		$tmp = "\t <thead>\n";
		$tmp .= "\t\t<tr> \n";

		foreach ($this->headers as $key => $name) {
				$tmp.="\t\t\t".'<th data-sortable="true" style="min-width:50px;" data-field="'.$name.'">'.$results[$name]."</th>\n";
		}
		$tmp .= "\t\t</tr> \n";
		$tmp .= "\t </thead>\n";
		$this->html.=$tmp;
	}


}
