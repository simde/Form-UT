<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class that enables easey crypted mail sending.
 */
class MailSender
{
		/**
		 * Easy mail sending function
		 * @param string  $from         Sender email adress
		 * @param string  $to           Receiver email adress
		 * @param string  $subject      Subject of the email
		 * @param string  $message      Content of the mail
		 * @param sting $pub_key      String containing the public PGP key
		 */
    public function SendMail($from, $to, $subject, $message, $pub_key)
    {
        // We add a new line, juste to make sure encryption will perform well.
        $message = '
				'.$message;

				$CI =& get_instance();

        $CI->load->library('email');

        $CI->load->library('PGPlib');


        $mailDATA = $CI->pgplib->prepareCryptoMail($message, $pub_key);

        $message=$mailDATA['message'];
        $headers=$mailDATA['headers'];
        $CI->email->set_header($headers[0], $headers[1]);


        $CI->email->from($from);
        $CI->email->to($to);
        $CI->email->subject($subject);
        $CI->email->message($message);

        if ($CI->email->send()) {
            return true;
        } else {
            return false;
        }
    }
}
