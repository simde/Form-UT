<?php
/**
 * Insipired by :
 * WP OpenPGP, a WordPress interface to OpenPGP
 *
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * @copyright Copyright (c) 2016 by Meitar "maymay" Moscovitz
 *
 * @package WordPress\Plugin\WP_PGP_Encrypted_Emails\WP_OpenPGP
 */

if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

// Load dependencies.
if (! class_exists('OpenPGP')) {
    require_once ASSETS_PATH.'openpgp-php-related/autoload.php';
    require_once ASSETS_PATH.'openpgp-php-master/lib/openpgp.php';
    require_once ASSETS_PATH.'openpgp-php-master/lib/openpgp_crypt_rsa.php';
    require_once ASSETS_PATH.'openpgp-php-master/lib/openpgp_crypt_symmetric.php';
}

/**
 * Main class for OpenPGP operations in CodeIgniter.
 */
class PGPlib
{

    /**
     * find the public key corresponding to an email address on the MIT keyserver.
     *
     * @param string $email the email adress.
     *
     * @return pgpKey | false
     */
    public static function findPublicKeyOnline($email)
    {
        $emailSat = str_replace("@", "%40", $email);
        $url = "https://pgp.mit.edu/pks/lookup?search=".$emailSat."&op=index";
        $html = file_get_contents($url);

                // if there has been an error :
                if (!$html) {
                    return false;
                }

        $re = '/search=(\w*)"/';
        $i = preg_match_all($re, $html, $matches, PREG_SET_ORDER, 0);

            //no match found:
            if ($i < 1) {
                return false;
            } else {
                $keyID = $matches[0][1];
            }

        $url = "https://pgp.mit.edu/pks/lookup?op=get&search=".$keyID;
        $html = file_get_contents($url);

                // if there has been an error :
                if (!$html) {
                    return false;
                }

        $re = '/<pre>\s([\w\W\s]*)\s<\/pre>/m';
        $i = preg_match_all($re, $html, $matches, PREG_SET_ORDER, 0);

                //no match found:
                if ($i < 1) {
                    return false;
                } else {
                    return $matches[0][1]; // the public PGP key
                }
    }


        /**
     * Gets an OpenPGP public key.
     *
     * @param mixed $key The OpenPGP key.
     * @param bool $ascii Whether or not the key is ASCII-armored.
     *
     * @return OpenPGP_Message|false
     */
        public static function getKey($key, $ascii = true)
        {
            if ($ascii) {
                preg_match('/-----BEGIN ([A-Za-z ]+)-----/', $key, $matches);
                $marker = (empty($matches[1])) ? 'MESSAGE' : $matches[1];
                $key = OpenPGP::unarmor($key, $marker);
            }
            $openpgp_msg = OpenPGP_Message::parse($key);
            return (is_null($openpgp_msg)) ? false : $openpgp_msg;
        }



    /**
     * Sign arbitrary data with a key.
     *
     * @param string $data
     * @param string $signing_key
     *
     * @return string
     */
         /*
    public static function clearsign($data, $signing_key)
    {
        $packet = new OpenPGP_LiteralDataPacket($data, array(
            'format' => 'u', 'filename' => 'message.txt'
        ));
        $packet->normalize(true);
        $signer = new OpenPGP_Crypt_RSA($signing_key[0]);
        $m = $signer->sign($packet);
        $sigs = $m->signatures();
        $packets = $sigs[0];
        $clearsign = "-----BEGIN PGP SIGNED MESSAGE-----\nHash: SHA256\n\n";
        $clearsign .= preg_replace("/^-/", "- -", $packets[0]->data)."\n";
        return $clearsign.apply_filters('openpgp_enarmor', $packets[1][0]->to_bytes(), 'PGP SIGNATURE');
    }*/

    /**
     * Signing is an alias to clearsign() right now.
     *
     * @param string $data
     * @param string $signing_key
     *
     * @return string
     */
         /*
    public static function sign($data, $signing_key)
    {
        return self::clearsign($data, $signing_key);
    }

    /**
     * Signs and then encrypts data.
     *
     * This is a shortcut for calling `sign()` and then `encrypt()`.
     *
     * @param string $data
     * @param OpenPGP_SecretKeyPacket $signing_key
     * @param array|string $recipient_keys_and_passphrases
     * @param bool $armor
     *
     * return string
     */
         /*
    public static function signAndEncrypt($data, $signing_key, $recipient_keys_and_passphrases, $armor = true)
    {
        $signed_data = apply_filters('openpgp_sign', $data, $signing_key);
        return apply_filters('openpgp_encrypt', $signed_data, $recipient_keys_and_passphrases, $armor);
    }
        */

    /**
     * Encrypts data to a PGP public key, passphrase, or set of passphrases or keys.
     *
     * @param string $data
     * @param string|array|OpenPGP_Message $keys A passphrase (as a `string`), a PGP public key, or an array of these.
     * @param bool $armor
     *
     * @return string
     */
    public static function encrypt($data, $keys, $armor = true)
    {
        $plain_data = new OpenPGP_LiteralDataPacket($data, array(
            'format' => 'u', 'filename' => 'encrypted.gpg'
        ));
        $encrypted = OpenPGP_Crypt_Symmetric::encrypt($keys, new OpenPGP_Message(array($plain_data)));
        if ($armor) {
            $encrypted = self::enarmor($encrypted->to_bytes(), 'PGP MESSAGE');
        }
        return $encrypted;
    }



    /**
     * ASCII-armors a value.
     *
     * This function wraps the `OpenPGP::enarmor()` method and offers
     * a WordPress filter hook (`openpgp_enarmor`) to plugin API calls.
     *
     * @param string $data
     * @param string $marker
     * @param array $headers
     *
     * @link https://singpolyma.github.io/openpgp-php/classOpenPGP.html#aa9d90195277e4c9d435ea70488f89c83
     *
     * @return string
     */
    public static function enarmor($data, $marker = 'MESSAGE', $headers = array())
    {
        // Wrap to no more than 64 characters as old-school PEM spec.
        // See also https://github.com/meitar/wp-pgp-encrypted-emails/issues/11
                return OpenPGP::enarmor($data, $marker, $headers);
        return wordwrap(OpenPGP::enarmor($data, $marker, $headers), 64, "\n", true);
    }


        /**
     * Generates a random string
     *
     * @param integer $length length of the string ti generate

     * @return string
     */
        public static function generateRandomString($length = 10)
        {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

    /**
     * Encrypts an email to a single recipient.
     *
     * If we have no public key with which to encrypt a message, such
     * as in the case of an unrecognized recipient, the content of the
     * message in the return array is unchanged.
     *
     * @param string $message
     * @param string $pub_key the receiver pgp public key, in the form :
         *			-----BEGIN PGP PUBLIC KEY BLOCK-----
         *			[...]
         *			-----END PGP PUBLIC KEY BLOCK-----
         *
         *
     * @return array (message and headers)
     */
    public static function prepareCryptoMail($message, $pub_key)
    {
        $delimiter = self::generateRandomString(33);

        $pub_key = self::getKey($pub_key);

        $message = self::encrypt($message, $pub_key);

        $headerCryptSpec = 'multipart/encrypted; boundary='.$delimiter.'; protocol="application/pgp-encrypted"';
        $headers = array(0 => 'Content-Type', 1 => $headerCryptSpec);

        $message = '--'.$delimiter.'
Content-Type: application/pgp-encrypted

Version: 1

--'.$delimiter.'
Content-Type: application/octet-stream

'.$message.'

--'.$delimiter.'--
';

        $mailDATA =  array(
            'message' => $message,
            'headers' => $headers,
        );
        return $mailDATA;
    }
}
