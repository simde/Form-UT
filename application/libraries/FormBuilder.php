<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}
/**
 * library used for generating forms from the server
 */
class FormBuilder
{
  	/**
		* Holds a CodeIgniter instance
		* @var CodeIgniter
		*/
    private $CI;


		/**
		 * Holding the form data
		 * @var array
		 */
    private $form = array();

		/**
		 * Holding  form element
		 * @var array
		 */
    private $formElement = array();

		/**
		 * holding the data for populate
		 * @var array
		 */
		private $elementsSerialized = array();

// Function //

    /**
     * Constructor of the class
     */
  public function __construct()
  {
      $this->CI =& get_instance();
  }


	/**
	 * Generates the resulting form, save it in the session associated with a token for later form validation
	 * @return array Array containing the formData, the token for form validation and the data for populating the form
	 */
	public function getFormAndPopulate(){
		$token = self::generateRandomString();
		while ($this->CI->session->userdata($token) !== NULL){
			$token = self::generateRandomString();
		}
		$this->CI->session->set_userdata($token,$this->form);

		return array('formToBuild'=>$this->form, 'populate'=>$this->elementsSerialized, 'tokenForFormValidation' => $token);
	}


	/**
	 * Creates a text field
	 * @param  string  $name     Name of the field
	 * @param  boolean $required Is the field required
	 * @param  string  $value    value of the field
	 */
	public function text($name, $required = true, $value = ''){
		$this->formElement = array('type'=>'text','name'=>$name, 'required'=>$required);
		$this->elementsSerialized[$name] = $value;
		return $this;
	}

	/**
	 * Creates a textarea field
	 * @param  string  $name     Name of the field
	 * @param  boolean $required Is the field required
	 * @param  string  $value    value of the field
	 */
	public function textarea($name, $required = true, $value = ''){
		$this->formElement = array('type'=>'textarea','name'=>$name, 'required'=>$required);
		$this->elementsSerialized[$name] = $value;
		return $this;
	}

	/**
	 * Creates a paragraph field
	 * @param  string  $name     Name of the field
	 */
	public function paragraph($name){
		$this->formElement = array('type'=>'paragraph','name'=>$name);
		return $this;
	}

	/**
	 * Set the maxlength attributes for a text or textarea
	 * @param int $int desired max length
	 */
	public function setMaxLength($int){
		$this->formElement['maxlength'] = $int;
		return $this;
	}


	/**
	 * Set the label of the field
	 * @param string $label desired label
	 */
	public function setFieldLabel($label){
		$this->formElement['label'] = $label;
		return $this;
	}


	/**
	 * Set the placeholder for the field
	 * @param string $placeholder placeholder for the field
	 */
	public function setFieldPlaceholder($placeholder){
		$this->formElement['placeholder'] = $placeholder;
		return $this;
	}

	/**
	 * Function to close a field
	 */
	public function closeField(){
		$this->form[]=$this->formElement;
		$this->formElement = array();
		return $this;
	}

	/**
	 * function to set the field value
	 * @param string $val desired value
	 */
	public function setFieldValue($val){
		$this->elementsSerialized[$this->formElement['name']] = $val;
		return $this;
	}



	/**
	 * Creates a date field
	 * @param  string  $name     Name of the field
	 * @param  boolean $required Is the field required
	 * @param  string  $value    value of the field
	 */
	public function date($name, $required = true, $value = ''){
		$this->formElement = array('type'=>'date','name'=>$name, 'required'=>$required);
		$this->elementsSerialized[$name] = $value;
		return $this;
	}


	/**
	 * Creates a select field
	 * @param  string  $name     Name of the field
	 * @param  boolean $required Is the field required
	 */
	public function select($name, $required = true){
		$this->optionCounter = 0;
		$this->formElement = array('type'=>'select','name'=>$name, 'required'=>$required);
		return $this;
	}


	/**
	 * Add the multiple option for a field
	 * @param boolean $multiple can the field have multiple inputs ?
	 */
	public function addMultiple($multiple = true){
		if (!isset($this->formElement['multiple'])) $this->formElement['multiple'] = $multiple;
		return $this;
	}

	/**
	 * Counter for the number of options (used for generating correct names)
	 * @var int
	 */
	private $optionCounter = 0;



	/**
	 * Add an option to a field
	 * @param string  $label    Label for the option
	 * @param string  $value    value for the field
	 * @param boolean $selected is the option selected ?
	 */
	public function addSelectOption($label,$value,$selected = false){
		if (!isset($this->formElement['values'])) $this->formElement['values'] = array();
		$this->formElement['values'][] = array('label' => htmlspecialchars($label), 'value' => $value);
		$optionID = $this->formElement['name'].'-'.$this->optionCounter;
		$this->elementsSerialized[$optionID] = $selected;
		$this->optionCounter++;
		return $this;
	}


	/**
	 * Creates a radio field
	 * @param  string  $name     Name of the field
	 * @param  boolean $required Is the field required
	 */
	public function radio($name, $required = true){
		$this->optionCounter = 0;
		$this->formElement = array('type'=>'radio-group','name'=>$name, 'required'=>$required);
		return $this;
	}


	/**
	 * Add an radio option to a field
	 * @param string  $label    Label for the option
	 * @param string  $value    value for the field
	 * @param boolean $selected is the option selected ?
	 */
	public function addRadioOption($label,$value,$selected = false){
		return self::addSelectOption($label,$value,$selected);
	}


	/**
	 * Creates a adminslect fields (the ones with opt group and search)
	 * @param  string  $name     Name of the field
	 * @param  boolean $required Is the field required
	 */
	public function adminSelect($name, $required = true){
		$this->groupCounter = 0;
		$this->formElement = array('type'=>'adminSelect','name'=>$name, 'required'=>$required);
		return $this;
	}

/**
 * Counter for the number of groups (used for correct name generation)
 * @var int
 */
	private $groupCounter = 0;

	/**
	 * Storage for the current group data
	 * @var array
	 */
	private $currentGroup = array();


	/**
	 * Add a group to the admin select
	 * @param string $label label of the group
	 */
	public function addGroup($label){
		$this->optionCounter = 0;
		if (!isset($this->formElement['groups'])) $this->formElement['groups'] = array();
		$this->currentGroup = array('label' => $label);
		return $this;
	}

	/**
	 * Close a group
	 */
	public function closeGroup(){
		$this->formElement['groups'][] = $this->currentGroup;
		$this->groupCounter++;
		return $this;
	}

	/**
	 * Add an option in a group
	 * @param string  $label    Label of the option
	 * @param string  $value    Value a of the option
	 * @param boolean $selected Is the option selected ?
	 */
	public function addGroupOption($label,$value,$selected = false){
		if (!isset($this->currentGroup['values'])) $this->currentGroup['values'] = array();
		$this->currentGroup['values'][] = array('label' => htmlspecialchars($label), 'value' => $value);
		$optionID = $this->formElement['name'].'-'.$this->groupCounter.'-'.$this->optionCounter;
		$this->elementsSerialized[$optionID] = $selected;
		$this->optionCounter++;
		return $this;
	}


	//////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////
	/**
	 * Generates a random string
	 *
	 * @param integer $length length of the string ti generate

	 * @return string
	 */
	private function generateRandomString($length = 10)
	{
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$charactersLength = strlen($characters);
			$randomString = '';
			for ($i = 0; $i < $length; $i++) {
					$randomString .= $characters[random_int(0, $charactersLength - 1)];
			}
			return $randomString;
	}


}
