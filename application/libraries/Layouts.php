<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Layouts Class.
 * Taken From : https://code.tutsplus.com/tutorials/how-to-create-a-layout-manager-with-codeigniter--net-15533
 * And modified
 */
class Layouts
{
  /**
 * Holds a CodeIgniter instance
 * @var CodeIgniter
 */
    private $CI;

		/**
		 * Holds the template name of the template we have to use to display the views
		 * @var string
		 */
		private $templateName = 'default';


    /**
     * Holds the different wiews data that needs to be inserted in the output
     * @var string
     */
    private $view_content = "<!-- content added through the layout system --> \r\n";


    /**
     * Title of the Page
     * \todo not sure of this
     * @var string
     */
    private $title_for_layout = null;


    /**
     * Holds the title separator
     * @var [type]
     */
    private $title_separator = ' | ';


    /**
     * Array of includes for the top
     * @var array of strings
     */
    private $includesTop = array();


    /**
     * Array of includes for the bottom
     * @var array of strings
     */
    private $includesBottom = array();


// Function //

    /**
     * Constructor of the class
     */
  public function __construct()
  {
      $this->CI =& get_instance();
  }


    /**
     * Set the title of the page
     * @param string $title Title of the page
     */
  public function setTitle($title)
  {
      $this->title_for_layout = $title;
      return $this;
  }


	/**
	* Set the template of the page
	* @param string $tempalte Name of the template to use
	*/
	public function setTemplate($template = 'default')
	{
			$this->templateName = $template;
			return $this;
	}

	/**
	 * Adding a view to the layout (inside)
	 * @param string $view_name the view we want to add
	 * @param array  $params    Parameters of the view
	 * @param string $loc  Where to put the view (for scripts)
	 *
	 * @return this.
	 */
  public function addView($view_name, $params = array(), $loc = 'main')
  {
			$addedView = "<!-- Added view : $view_name -->\r\n";
			$addedView .= $this->CI->load->view('template/'.$this->templateName.'/views/'.$view_name, $params, true);

			if ($loc == 'main'){
				$this->view_content .= $addedView;
			} elseif ($loc == 'bottom') {
				$this->includesBottom[] = $addedView;
			} elseif ($loc == 'top') {
				$this->includesTop[] = $addedView;
			}

      return $this;
  }




	/**
	 * Display the resulting layouts.
	 * @param  string $layout if you want to use a specific layouts
	 */
  public function display()
  {
      // Handle the site's title. If NULL, don't add anything. If not, add a
      // separator and append the title.
      if ($this->title_for_layout !== null) {
          $separated_title_for_layout = APP_NAME . $this->title_separator . $this->title_for_layout;
      } else {
          $separated_title_for_layout = APP_NAME; // we print only the app name defined in the constants area
      }

			$this->view_content .= "<!-- END of the content added through the layout system --> \r\n";

	  // Now load the layout, and pass the differents views stored
	  $this->CI->load->view('template/' .$this->templateName.'/'.$this->templateName, array(
	    		'content_for_layout' => $this->view_content,
    			'title_for_layout' => $separated_title_for_layout
  				));

  }

		/**
		 * Add includes to the layouts
		 * @param string  $string           The full string that needs to be included
		 * @param string  $loc              In wich part of the html you want to put the ressource ?
		 *
		 * @return this
		 */
    public function addInclude($string, $loc = 'bottom')
    {
        if ($loc =='bottom') {
            $this->includesBottom[] = $string;
        } elseif ($loc == 'top') {
            $this->includesTop[] = $string;
        }

        return $this; // This allows chain-methods
    }


		/**
		 * Add includes to the final file
		 * @param  [type] $loc wich includes to print based on locations
		 * @return string      includes
		 */
    public function print_includes($loc)
    {
      // Initialize a string that will hold all includes
    	$final_includes = "<!-- Includes added throug the layout system : --> \r\n";

      if ($loc == 'top') {
          $includes = $this->includesTop;
      } elseif ($loc = 'bottom') {
          $includes = $this->includesBottom;
      }

      foreach ($includes as $include) {
	        $final_includes .= $include."\r\n";
    	}

        return $final_includes;
    }



}
