<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Few tools used in the app
 */
class Tools
{

		/**
		 * Convert a timestamp to a date
		 * @param  string $timestamp timestamp
		 * @return string            date correctly formatted
		 */
    public function timestampToDate($timestamp)
    {
			if ($timestamp === null) return '';

			$re = '/([0-9]{4})-([0-9]{2})-([0-9]{2})/';

			preg_match($re, $timestamp, $matches);

			// Print the entire match result
			$year = $matches[1];
			$month = $matches[2];
			$day = $matches[3];
			return $day.'/'.$month.'/'.$year;
    }


		/**
		 * Convert a date to a timestamp
		 * @param  string $date a date
		 * @return string         the corresponding timestamp (time is set to 23h53)
		 */
    public function dateToTimestamp($date)
    {
        $re = '/^([01-9]{1,2})\/([01-9]{1,2})\/([01-9]{4})$/';
        preg_match($re, $date, $matches);

        $day = intval($matches[1]);
        $month = intval($matches[2]);
        $year = intval($matches[3]);

        return $year.'-'.$month.'-'.$day.' 23:59:59';
    }
}
