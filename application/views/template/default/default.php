<!DOCTYPE HTML>
<html>

	<head>
		<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo $title_for_layout ?></title>

		<link rel="stylesheet" href="<?php echo base_url("node_modules/bootstrap/dist/css/bootstrap.min.css"); ?>" />
		<link rel="stylesheet" href="<?php echo base_url("node_modules/font-awesome/css/font-awesome.min.css"); ?>" />

		<!-- Custom styles for this template -->
		<link rel="stylesheet" href="<?php echo base_url("assets/css/navbar-top-fixed.css"); ?>" />
		<link rel="stylesheet" href="<?php echo base_url("assets/css/sticky-footer.css"); ?>" />
		<link rel="stylesheet" href="<?php echo base_url("node_modules/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css"); ?>" />
		<link rel="stylesheet" href="<?php echo base_url("assets/css/general.css"); ?>" />


		<?php echo $this->layouts->print_includes('top') ?>

	</head>

	<body>


		<nav class="navbar navbar-default navbar-fixed-top">
					<div class="container">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="navbar-brand" href="<?php echo base_url(); ?>" >Form'UT</a>
						</div>
						<div id="navbar" class="navbar-collapse collapse">
							<ul class="nav navbar-nav navbar-left">
								<li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gestion des formulaires <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo base_url("FormCreation"); ?>">Créer un formulaire</a></li>
									<li><a href="<?php echo base_url("Administrate/index"); ?>">Administrer les formulaires</a></li>
									<li><a href="<?php echo base_url("Administrate/results/"); ?>">Consulter les résultats</a></li>
                </ul>
              </li>
							<li ><a href="<?php echo base_url("Answer/list"); ?>">Répondre à un formulaire</a></li>

								<li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gestion des groupes <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo base_url("Groups"); ?>">Afficher les groupes</a></li>
                  <li><a href="<?php echo base_url("Groups/initGroup"); ?>">Créer un groupe</a></li>
                </ul>
              </li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
								<li class="active"><a href="<?php echo base_url('Users/logout'); ?>">Me déconnecter</a></li>
							</ul>
						</div><!--/.nav-collapse -->
						<div id="progress"></div>
				</div>
		</nav>


		<div class="blog">
			<br>
			 <div class="container">
		<?php echo $content_for_layout; ?>

<br>
<br><br>


			 </div>
		 </div>




		 <footer class="footer text-center">
 			<div class="container">
 				<p class="text-muted">Création : Florent Chehab, lien gitlab :
 					<a href="https://gitlab.utc.fr/simde/Form-UT" target="_blank"><i class="fa fa-gitlab" aria-hidden="true"></i> </a> </p>
 			</div>
 		</footer>

		<script type="text/javascript" src="<?php echo base_url("node_modules/jquery/dist/jquery.min.js"); ?>"></script>

		<script type="text/javascript" src="<?php echo base_url("node_modules/bootstrap/dist/js/bootstrap.min.js"); ?>"></script>

		<script type="text/javascript" src="<?php echo base_url("node_modules/bootstrap-notify/bootstrap-notify.min.js"); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url("node_modules/bootbox/bootbox.min.js"); ?>"></script>
		<link rel="stylesheet" href="<?php echo base_url("node_modules/animate.css/animate.min.css"); ?>" />

		<?php echo $this->layouts->print_includes('bottom') ?>




	</body>
</html>
