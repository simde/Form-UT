<div id='builderDiv'>

	<div>
		<h2>
		Création du formulaire :
		</h2>
		<h3 class="formTitle">
			<?php echo htmlspecialchars($title) ;?>
		</h3>
	</div>


	<div id="fb-editor"></div>




	<div class="form-group">
		<br>
		<div class="stylish-justify ">

			<div>
				<button type="button" class="btn btn-secondary btn-lg" id="buttonPreviousStep">
				 <i class="fa fa-angle-double-left"></i> Retour à l'étape précédente
			</button>
			</div>

			<div>
				<button type="reset" id="buttonClearForm" class="btn btn-warning">
				Remettre à zéro <i class="fa fa-trash"></i>
			</button>
			</div>

			<div>
				<button type="button" class="btn btn-success btn-lg" id="buttonNextStep">
				Passer à l'étape suivante  <i class="fa fa-angle-double-right"></i>
			</button>
			</div>
		</div>
	</div>


	<div class="form-group text-center">
		<hr>
		<div class="alert alert-info">
			<strong>Info ! </strong> Vous pouvez utiliser du markdown dans les éléments de type paragraphe.
		</div>
		<div class="btn-group" role="group" aria-label="...">
			<button type="button" class="btn btn-default btn-lg" id="getInspired"><i class="fa fa-angle-down"></i> S'inspirer</button>
			<button type="button" class="btn btn-default btn-lg" id="getPreview">Prévisualiser <i class="fa fa-angle-down"></i></button>
		</div>


	</div>

</div>

<div id="goBackToEdition" class="text-center" class="displayNone">
	<hr>
	<button type="button" class="btn btn-default btn-lg" id="buttonGoBackToEdition">
	<i class="fa fa-angle-double-up"></i> Retour à l'édition <i class="fa fa-angle-double-up"></i>
	</button>
</div>

<div class="">
	<div id="previewBuilder" class="displayNone">
		preview builder
	</div>
</div>
<div id="inspiration" class="row" class="displayNone">
	<br>
	<div id="selectInspiration" class="col-md-5">
		<div style="display:table;">
			<div id="quickPreview"></div>
			<div class="row pull-right">
				<button type="button" class="btn btn-primary" id="previewFromSelect">
				Prévisualiser	<i class="fa fa-angle-double-right"></i>
			</button>
			</div>
			<div style="display: inline;">
				<details>
					<summary><i class="fa fa-hand-o-right" aria-hidden="true"></i> Mode expert</summary>

					<div class="form-group form-group-lg">
						<label for="inputJSON" class="control-label">C'est parti pour du JSON :</label>
						<textarea class="form-control" rows="10" name="inputJSON" id="inputJSON" placeholder="Il faut mettre un TABLEAU JSON ici !"></textarea>
					</div>

					<div class="pull-left">
						<button type="button" class="btn btn-secondary" id="loadFormBuilderData">
						Charger les données actuelles	<i class="fa fa-file-code-o" aria-hidden="true"></i>
					</button>
					</div>
					<div class="pull-right">
						<button type="button" class="btn btn-primary" id="previewFromTextArea">
						Prévisualiser	<i class="fa fa-angle-double-right"></i>
					</button>
					</div>
				</details>
			</div>
		</div>

	</div>
	<div class="col-md-7">
		<div id="btns-preview" class="text-center displayNone">
			<div class="btn-group " role="group">
				<button type="button" class="btn btn-default" id="addBegining">Ajouter au début</button>
				<button type="button" class="btn btn-default" id="addEnd">Ajouter à la fin </button>
				<button type="button" class="btn btn-default" id="addReplace">Remplacer</button>
			</div>
			<br>
		</div>
		<div id="previewInspiration">

		</div>

	</div>
</div>
