<div>
	<H1 style="text-align:center;">
Félicitation !
	</h1>
	<br>
	<h3 style="margin-top: 0px;">
		Le formulaire <span class="formTitle"><?php echo htmlspecialchars($title) ;?></span> est maintenant officiellement créé.
</h3>
<br>
<H4>
	Vous pouvez inviter les personnes à répondre au formulaire directement via le lien suivant :
	<br>
<a href="<?php echo $urlToShare ;?>"><?php echo $urlToShare ;?></a>
</h4>
<p>Attention seules les personnes appartenant à l'un des groupes que vous avez paramètré lors de la création du formulaire
 pourront y accéder. Il ne suffit pas d'avoir accès au lien !</p>

<br>
<H4>
Vous pouvez modifier la liste des groupes accédant au formulaire ici :
<br>
<a href="<?php echo $urlToAdmin ;?>"><?php echo $urlToAdmin ;?></a>
</h4>
<p>
	(Vous pourrez également modifier d'autres paramètres et voir les résultats du formulaire... ça vaut le détour !)
</p>

</div>




</div>
