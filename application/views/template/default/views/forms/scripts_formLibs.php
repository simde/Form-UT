<script type="text/javascript" src="<?php echo base_url("node_modules/jquery-datepicker/jquery-datepicker.min.js"); ?>"  ></script>
<link rel="stylesheet" href="<?php echo base_url("assets/css/jquery-ui.css"); ?>" />
<script type="text/javascript" src="<?php echo base_url("node_modules/bootstrap-select/dist/js/bootstrap-select.min.js"); ?>"  ></script>
<link rel="stylesheet" href="<?php echo base_url("node_modules/bootstrap-select/dist/css/bootstrap-select.min.css"); ?>" />
<script type="text/javascript" src="<?php echo base_url("node_modules/bootstrap-select/dist/js/i18n/defaults-fr_FR.js"); ?>"  ></script>

<script type="text/javascript" src="<?php echo base_url("node_modules/markdown/lib/markdown.min.js"); ?>"  ></script>
<script type="text/javascript" src="<?php echo base_url("node_modules/xss/dist/xss.min.js"); ?>"  ></script>
<script type="text/javascript" src="<?php echo base_url("node_modules/nanobar/nanobar.min.js"); ?>"  ></script>

<!-- automatic form validation -->
<script type="text/javascript">
window.ParsleyConfig = {
    errorClass: 'has-error',
    successClass: 'has-success',
    classHandler: function(ParsleyField) {
        return ParsleyField.$element.parents('.formElement');
    },
    errorsContainer: function(ParsleyField) {
        return ParsleyField.$element.parents('.formElement');
    },
    errorsWrapper: '<span class="help-block">',
    errorTemplate: '<div></div>'
};
</script>


<script type="text/javascript" src="<?php echo base_url("node_modules/parsleyjs/dist/parsley.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/lang/fr.js"); ?>"></script>

<script type="text/javascript" src="<?php echo base_url("assets/js/libs.min.js"); ?>"></script>
<link rel="stylesheet" href="<?php echo base_url("assets/css/lickert.css");?>">
