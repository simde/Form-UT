<!-- specific form creation related scripts : -->
<script type="text/javascript" src="<?php echo base_url("assets/js/formRenderLib.js"); ?>" ></script>



<script type="text/javascript">


ajaxLib.addAjaxToken('<?php if(isset($ajaxToken)){echo $ajaxToken;}?>');
ajaxLib.setURL('<?php if(isset($ajaxLocation)){echo $ajaxLocation;} else echo "'Faut aller où pour les requêtes ajax ??'"; ?>');
formLib.setFormId(<?php if(isset($id)){echo $id;} else echo "'Pas d id !!'"; ?>);

var populatePreview = '<?php echo $populateFormHandler;?>';

	var data = {
		"id": <?php if(isset($id)){echo $id;} else echo "'Pas d id !!'"; ?>
	}
	var ajaxCall = ajaxLib.doItMain(populatePreview, data, 'error');

	ajaxCall.done(successLoadingPreview);


 function successLoadingPreview (returnedData) {
	if (returnedData.success == 'SUCCESS') {
		formRenderLib.setRenderingDiv('preview');
		formRenderLib.setDataToRender(returnedData.data.formBuilderData);
		formRenderLib.renderNow(false);
	}
};

</script>
