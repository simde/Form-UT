<div>
	<H2>Création d'un formulaire.</H2>
</div>

<div class="alert alert-info">
	<strong>Info !</strong> Retrouvez les formulaires que vous aviez commencés mais pas terminés <a href="<?php echo base_url('/Administrate/index');?>">ici</a>.
</div>

<div id="mainForm"> </div>

<div id="mainNavigationDiv">
	<div id="mainPreviousStepButton"></div>
	<div id="mainNextSetpButton"></div>
</div>
