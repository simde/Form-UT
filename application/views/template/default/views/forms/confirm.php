<div id='builderDiv'>

	<div>
		<H2>
		Confirmation de la création du formulaire :
	</h2>
		<h3 class="formTitle">
			<?php echo htmlspecialchars($title) ;?>
</h3>
<div class="text-center">
		<form action="<?php echo $nextPage;?>">
			<button type="submit" class="btn btn-lg btn-success" >
Publier définitivement ?
		</button>
		</form>
	</div>
</div>


	<div>
		<form action="<?php echo $previousPage;?>">
			<div>
				<button type="submit" class="btn btn-lg btn-secondary previousButton" id="buttonPreviousStep">
				 <i class="fa fa-angle-double-left"></i> Retour à l'étape précédente
			</button>
			</div>
		</form>
	</div>

<div></br></br><H2>Prévisualisation du formulaire : </H2> </div>
	<div id="preview"></div>
