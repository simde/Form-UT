<!-- mostly wrappers for cleaner code -->
<script type="text/javascript" src="<?php echo base_url("assets/js/tools.js"); ?>"></script>

<!-- ajax interface for easy handling of ajax -->
<script type="text/javascript" src="<?php echo base_url("assets/js/ajaxInterface.js"); ?>"></script>

<!-- form handling library -->
<script type="text/javascript" src="<?php echo base_url("assets/js/formLib.js"); ?>"></script>


<!-- stuff that needs to be done -->
<script type="text/javascript">
ajaxLib.setToken('<?php if(isset($ajaxToken)){echo $ajaxToken;} else echo "'Il manque un token mon coco'"; ?>');
ajaxLib.setURL('<?php if(isset($ajaxLocation)){echo $ajaxLocation;} else echo "'Faut aller où pour les requêtes ajax ??'"; ?>');

// Global variable
var id = <?php if(isset($id)){echo $id;} else echo "'Pas d id !!'"; ?>;
var getInitialData = <?php if(isset($getInitialData)){echo $getInitialData;} else echo "false"; ?>;
var waitingForData = getInitialData;
var savingFormHandler = '<?php if(isset($savingFormHandler)){echo $savingFormHandler;} else echo "Faut aller où pour enregistrer ??"; ?>';
var populateFormHandler = '<?php if(isset($populateFormHandler)){echo $populateFormHandler;} else echo "Faut aller où pour récupérer les données ??"; ?>';
var nextPage = '<?php if(isset($nextPage)) echo $nextPage; ?>';
var previousPage = '<?php if(isset($previousPage )) echo $previousPage ;?>';
</script>
