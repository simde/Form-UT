<!-- specific form creation related scripts : -->
<script type="text/javascript" src="<?php echo base_url("node_modules/jquery-ui-sortable/jquery-ui.min.js"); ?>"  ></script>
<script type="text/javascript" src="<?php echo base_url("node_modules/formBuilder/dist/form-builder.min.js"); ?>" ></script>


<script type="text/javascript" src="<?php echo base_url("assets/js/formBuilderLibs.min.js"); ?>" ></script>


<script type="text/javascript">

$('#inspiration').toggleClass("displayNone",true);
$('#previewBuilder').toggleClass("displayNone",true);
$('#goBackToEdition').toggleClass("displayNone",true);

ajaxLib.addAjaxToken('<?php if(isset($ajaxToken1)){echo $ajaxToken1;}?>');
ajaxLib.addAjaxToken('<?php if(isset($ajaxToken2)){echo $ajaxToken2;}?>');

ajaxLib.setURL('<?php if(isset($ajaxLocation)){echo $ajaxLocation;} else echo "'Faut aller où pour les requêtes ajax ??'"; ?>');
formBuilderLib.setFormId(<?php if(isset($id)){echo $id;} else echo "'Pas d id !!'"; ?>);
formBuilderLib.setTranslationLocationUrl('<?php if(isset($translationLocation)) echo $translationLocation; ?>');

formLib.setFormId(<?php if(isset($id)){echo $id;} else echo "'Pas d id !!'"; ?>);
formBuilderLib_addons.setInspirationGetHandler('<?php if(isset($inspirationGetHandler)){echo $inspirationGetHandler;} else echo "Faut aller où pour enregistrer ??"; ?>');
formBuilderLib_addons.setInspirationPreviewHandler('<?php if(isset($inspirationPreviewHandler)){echo $inspirationPreviewHandler;} else echo "Faut aller où pour enregistrer ??"; ?>');
formBuilderLib_addons.setSelectInspirationRenderingDiv('quickPreview');
formBuilderLib_addons.setCheckInspirationHandler('<?php if(isset($checkInspirationHandler)){echo $checkInspirationHandler;} else echo "Faut aller où pour enregistrer ??"; ?>');
formBuilderLib.setMainSavingFormHandler('<?php if(isset($mainSavingFormHandler)){echo $mainSavingFormHandler;} else echo "Faut aller où pour enregistrer ??"; ?>');
formBuilderLib.setSimpleSavingFormHandler('<?php if(isset($simpleSavingFormHandler)){echo $simpleSavingFormHandler;} else echo "Faut aller où pour enregistrer ??"; ?>');
formBuilderLib.setPopulateFormHandler('<?php if(isset($populateFormHandler)){echo $populateFormHandler;} else echo "Faut aller où pour récupérer les données ??"; ?>');

formBuilderLib.setFormId(<?php echo $id;?>);
formBuilderLib.initFormBuilder();
formBuilderLib.retreiveFormArchitectureFromServer();

var previousStepUrl = '<?php if(isset($previousPage)){echo $previousPage;} else echo "Faut aller où pour récupérer les données ??" ?>'
formBuilderLib.setNextPage('<?php if(isset($nextPage)){echo $nextPage;}?>');
</script>

<script>
// auto save feature.
//

window.setInterval(function(){
		console.log("autoSavingStart");
		formBuilderLib.simpleSaveArchitecture('error');
		console.log("autoSaingEnd");
}, 60000);

formBuilderLib_addons.setInteractions();

document.getElementById('buttonClearForm').addEventListener('click', function() {
	bootbox.confirm( {message :"Êtes-vous sur de vouloir remettre à zéro le formulaire ?",
		buttons: {confirm: {label: 'Oui',className: 'btn-warning'	},cancel: {	label: 'Non',	className: 'btn-primary'}},
		callback: function(result){ if(result == true) formBuilderLib.clearFields(); }
	});
}); //End click on clear button


document.getElementById('buttonNextStep').addEventListener('click', function() {
		formBuilderLib.mainSaveArchitecture(); // no redirect yet
}); //End click on save button

document.getElementById('buttonPreviousStep').addEventListener('click', function() {
		formBuilderLib.simpleSaveArchitecture('error',previousStepUrl); // no redirect yet
}); //End click on save button



formBuilderLib_addons.loadGetInspired();

</script>

<!-- end of specific form related scripts : -->
