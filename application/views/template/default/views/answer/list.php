<div>
	<h1>
		Répondre.
	</h1>
	<h4>
		Légende :
	</h4>
	<p>
		<ul>
			<li>
				<span class="fa-stack fa-lg">
						<i class="fa fa-user-o  fa-stack-1x"></i>
						<i class="fa fa-ban fa-stack-1x text-danger"></i>
					</span> : participation anonyme,
			</li>
			<li>
<i class="fa fa-user-o"></i> : participation non anonyme,
			</li>
			<li>
<i class="fa fa-check" style="color:green"></i>: participation confirmée,
			</li>
			<li>
				<i class="fa fa-exclamation-triangle" style="color:orange;" aria-hidden="true"></i> : vous n'avez pas confirmer votre participation,
			</li>
			<li>
				<i class="fa fa-question" aria-hidden="true"></i> : statut de la participation inconnu (participation anonyme).
			</li>
		</ul>
	</P>
	<h2>
		Vos formulaires :
		</h2>
	<p>
		<?php echo $yourForms; ?>
	</p>
	<h2>
		Autres formulaires auxquels vous avez accès :
		</h2>
	<p>
		<?php echo $otherForms; ?>
	</p>
</div>
