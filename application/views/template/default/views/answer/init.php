<div>
	<p>Bonjour vous vous apprêtez à répondre au formulaire :</p>
	<h3 class="formTitle"><?php echo htmlentities($title); ?></H3>
	<p> Voici quelques informations de base à confirmer.
		Si un choix est possible, choisissez soigneusement, vous ne pourrez par modifier ces informations par la suite.
		Pour vous aider, vous pouvez prévisualiser le formulaire (si rien ne s'affiche, c'est que le formulaire est volontairement vide).
</p>
</div>

<div id="mainForm"> </div>

<div id="mainNavigationDiv">
	<div id="mainPreviousStepButton"></div>
	<div id="mainNextSetpButton"></div>
</div>

<div class="text-center; display:table;">
<button type="button" class="btn btn-default btn-lg" id="getPreview">Prévisualiser <i class="fa fa-angle-down"></i></button>
<br>
<br>
</div>
<div id="preview"> </div>
