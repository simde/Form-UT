<!-- stuff that needs to be done -->
<script type="text/javascript">

</script>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="<?php echo base_url("node_modules/bootstrap-table/dist/bootstrap-table.min.css");?>">
<link rel="stylesheet" href="<?php echo base_url("node_modules/bootstrap-table/dist/extensions/filter-control/bootstrap-table-filter-control.css");?>">

<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url("node_modules/bootstrap-table/dist/bootstrap-table.min.js");?>"></script>
<script src="<?php echo base_url("node_modules/bootstrap-table/dist/locale/bootstrap-table-fr-FR.min.js");?>"></script>
<script src="<?php echo base_url("node_modules/tableexport.jquery.plugin/tableExport.min.js");?>"></script>

<script src="<?php echo base_url("node_modules/bootstrap-table/dist/extensions/export/bootstrap-table-export.min.js");?>"></script>
<script src="<?php echo base_url("node_modules/bootstrap-table/dist/extensions/filter/bootstrap-table-filter.min.js");?>"></script>
<script src="<?php echo base_url("node_modules/bootstrap-table/dist/extensions/filter-control/bootstrap-table-filter-control.min.js");?>"></script>
<script src="<?php echo base_url("node_modules/bootstrap-table/dist/extensions/mobile/bootstrap-table-mobile.min.js");?>"></script>
<link rel="stylesheet" href="<?php echo base_url("assets/css/results.css");?>">
