<div class="alert alert-warning">

<div>
	<p> Vous répondez de manière anonyme à ce formulaire. Veillez à garder en lieu sûr le code ci-dessous pour toutes modifications ultérieures. </p>
<div style="text-align:center;">
<code class="text-center" id="codeAccesAnonymous"><?php echo $uid; ?></code>
</div>
<div class="text-center">
<button id="simple" class="btn btn-secondary" onclick="copyToClipboard(document.getElementById('codeAccesAnonymous').innerHTML)">Copier</button>
</div>

<br>
</div>
<details>
	<summary class="text-center"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Plus de possibilités pour les expert⋅e⋅s</summary>
	<p> Il est possible d'envoyer ce token par mail mais uniquement encrypté avec PGP. Merci de rentrer les informations nécessaires :</p>
	<br>
	<p> Si vous avez une paire de clef PGP, vous avez surement partagé la partie publique avec le reste du <a target="_blank" href="https://pgp.mit.edu/">monde</a>.</p>
	<div class="formElement">
		<label for="email" class="control-label">
	<span style="color:red">* </span>
	Adresse email :</label>
		<input type="email" id="email" name="email" placeholder="Inscrire l'adresse email associée à la clef..." class="form-control" required="true" value="<?php echo $email; ?>">

	<div class="form-group">
		<label for="PGPkey" class="control-label">	<span style="color:red">* </span>Clé PGP publique :</label>
		<textarea class="form-control" rows="10" name="PGPkey" id="PGPkey" placeholder="-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: SKS 1.1.5
Comment: Hostname: pgp.mit.edu

mQINBFlTj2kBEACVwoAysBKThAOahw5paXjVb7o9ME4NdujzKfoD0Vx8cQm7BT8fIYx12Ecv
KVFRlGrtx6+ea0JTWDEYj2QbDFMq/KFQLfKwMDGs8AJtX0D7mH0Yv9GlTISh/Rvp+j1a5qJ8
/Z+L/FOE3qVyMEVQ8xiarV7lkO4tgoJc9Of1skpM2NYZk9w0r8ez0HCFYtN1fhppb2E1yUs0
BGQF+jMM8Ukt75go17Uq2lk1zI1N24YQbWu6/N0kLlLuHANlhucvR/yD/hPLKJ2ZxSVVmnrB
HTzOVYC/YMhd3OyGySzYj0YSqzlDL509LkAK/PqpTIMKd/Op+ZU5dhxzSO9sIgE3jJ86M/VY
LDbrqdUbNRDCKAOIItk1XGjUnUl2912dhcd+PxKmuxD5pxAgdO0GuGrN44W/fsh0n9a9IMnN
1zLtHlZHNMP3vuedRUTZjGDT35cBeIGlg7pgfjx5STsUQeAnxzxAmIu8gSiIozAOh+fpZc9G
Tf23TGkdXO2k/g3cw9t1voiRGWu+fJuEbA94001odfzQJjuoChjppIyXNFpc5l4QVgZ7VV1i
/+XKhQrEgVW3/mHrVyl0tWEFkGbqpb6lo4c7I0ec/knutE+mrbv8ktR66LnxScKn5FtmhklG
xc1AfY9OZu1WYETcizpI0awW7mQjGAZvUlZa8+G53JHmBapWwkbvW8seU7Nzyw6NlbbNwT0w
NdVLMzEpPoCOghDqqgy2a1lu4y9qeJMv771GpO4BcdwkoJzqj2c+9TRN1v3J1xWqjvVYfJKZ
BSM=
=Abap
-----END PGP PUBLIC KEY BLOCK-----" required></textarea>
	</div>
</div>
<div class="text-center">
<button id="send" class="btn btn-lg btn-success" >
	Envoyer
</button>
</div>


</details>

</div>
