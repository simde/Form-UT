<div>
	<H3 class="formTitle"><?php echo htmlentities($title); ?></H3>
	<p> Si la page est vide, passer directement à l'étape suivante.</p>
	<hr>
</div>

<div id="mainForm"> </div>

<div id="mainNavigationDiv">
	<div id="mainPreviousStepButton"></div>
	<div id="mainNextSetpButton"></div>
</div>
