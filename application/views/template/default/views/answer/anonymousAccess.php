<div>
	<p>Pour accéder au formulaire merci de saisir le code d'accès qui vous a été fourni.</p>
	<p> Votre participation étant anonyme, c'est le seul moyen pour nous de la retrouver. Vous êtes la seule personne possédant le code d'accès....</p>
<br>
<p> Il vous reste <?php echo $nbessais; ?> essais.
</div>

<form action="/answer/anonymousAccess/<?php echo $formId;?>" method="post">
	<div class="formElement">
		<label for="code" class="control-label">
	<span style="color:red">* </span>
	Code d'accès :</label>
		<input type="text" id="code" name="code" placeholder="Inscrire le code ici..." class="form-control" required="true">
	</div>
	<div class="text-center">
	<input type="submit" class="btn btn-lg btn-success" value='Accéder au formulaire.'>
</div>
</form>
