<div>
	<H2>
		Confirmation de la réponse au formulaire :
	</h2>
	<H3 class="formTitle"><?php echo htmlentities($title); ?></H3>
	<p style="font-style: italic;">Si vous avez participé de manière anonyme, garder en sécurité l'identifiant d'accès (disponible à l'étape précédente) pour modifier vos réponses après avoir quitté cette page.</p>
	<br>
	<p> Voici exactement les informations associées à votre participation au formulaire :
	<div>
		<?php echo $result;?>
	</div>
	<br>

	<div class="text-center">
		<form action="<?php echo $nextPage;?>">
			<button type="submit" class="btn btn-lg btn-success" >
Confirmer ma participation ?
		</button>
		</form>
	</div>

	<div style="float:left;">
		<form action="<?php echo $previousPage;?>">
			<div>
				<button type="submit" class="btn btn-lg btn-secondary previousButton" id="buttonPreviousStep">
				 <i class="fa fa-angle-double-left"></i> Retour à l'étape précédente
			</button>
			</div>		</form>
	</div>
</div>
