<!-- stuff that needs to be done -->
<script type="text/javascript">
ajaxLib.addAjaxToken('<?php if(isset($ajaxToken)){echo $ajaxToken;} else echo "'Il manque un token mon coco'"; ?>');
ajaxLib.setURL('<?php if(isset($ajaxLocation)){echo $ajaxLocation;} else echo "'Faut aller où pour les requêtes ajax ??'"; ?>');

var id = <?php if(isset($id)){echo $id;} else echo "'Pas d id !!'"; ?>;
formLib.setFormId(id);
formLib.setMainSavingFormHandler('<?php if(isset($mainSavingFormHandler)){echo $mainSavingFormHandler;} else echo "Faut aller où pour enregistrer ??"; ?>');
formLib.setSimpleSavingFormHandler('<?php if(isset($simpleSavingFormHandler)){echo $simpleSavingFormHandler;} else echo "Faut aller où pour enregistrer ??"; ?>');
formLib.setPopulateFormHandler('<?php if(isset($populateFormHandler)){echo $populateFormHandler;} else echo "Faut aller où pour récupérer les données ??"; ?>');

formInterfaceLib.setNextStepUrl('<?php if(isset($nextPage)) echo $nextPage; ?>');
formRenderLib.setRenderingDiv('mainForm');

formLib.setEverythingUp();

var previewHandler = '<?php if(isset($previewHandler)){echo $previewHandler;} ?>';


document.getElementById('getPreview').addEventListener('click', function() {
	getFormForPreview();
}); //End click on save button


function getFormForPreview() {
	self = this;

	var data = {
		"id": id
	};

	var ajaxCall = ajaxLib.doItMain(previewHandler, data, 'error');

	ajaxCall.done(successGettingForm);
}


function successGettingForm (returnedData) {
	if (returnedData.success == 'SUCCESS') {
		formRenderLib.setRenderingDiv('preview');
		var dataBeingPreviewed = returnedData.data.formBuilderData;
		formRenderLib.setDataToRender(dataBeingPreviewed);
		formRenderLib.renderNow(false);
	}
};



</script>
