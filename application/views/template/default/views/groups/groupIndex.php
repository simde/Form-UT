<div>
	<h1>
		Gestion des groupes.
	</h1>
	<form action="/Groups/initGroup">
    <input class="btn btn-lg btn-primary" type="submit" value="Créer un groupe" />
</form>
	<h2>
		Vos groupes :
		</h2>
	<p>
		<?php echo $yourGroups; ?>
	</p>
	<h2>
		Groupes créés par d'autres personnes et auxquels vous appartenez :
		</h2>
	<p>
		<?php echo $groupsYouBelongTo; ?>
	</p>
</div>
