<div>
	<h2>
		Création/Édition d'un groupe.
	</h2>

</div>

<div class="alert alert-warning">
	<strong>Attention !</strong> Les groupes inactifs verront leurs accès systématiquement supprimés lors de la mise à jour des droits d'accès aux formulaires.
</div>


<div id="mainForm"> </div>


<div id="mainNavigationDiv">
	<div id="mainPreviousStepButton"></div>
	<div id="mainSaveButton"></div>
	<div id="mainNextSetpButton"></div>
</div>
