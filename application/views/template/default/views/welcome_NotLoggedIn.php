<!DOCTYPE HTML>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Accueil</title>

	<link rel="stylesheet" href="<?php echo base_url("node_modules/bootstrap/dist/css/bootstrap.min.css"); ?>" />
	<link rel="stylesheet" href="<?php echo base_url("node_modules/font-awesome/css/font-awesome.min.css"); ?>" />

	<!-- Custom styles for this template -->
	<link rel="stylesheet" href="<?php echo base_url("assets/css/navbar-top-fixed.css"); ?>" />
	<link rel="stylesheet" href="<?php echo base_url("assets/css/sticky-footer.css"); ?>" />
</head>

<body>


	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
		            <span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		          </button>
				<a class="navbar-brand" href="<?php echo base_url(); ?>">Form'UT</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li class="active"><a href="<?php echo base_url("Users/login"); ?>">Me connecter</a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>




	<div class="jumbotron">
		<div class="container">
			<h1>Bonjour !</h1>
			<p>Dans un premier temps, merci de vous connecter au CAS :</p>
			<p><a class="btn btn-primary btn-lg" href="<?php echo base_url("Users/login"); ?>" role="button">Me connecter au CAS</a></p>
		</div>
	</div>



	<footer class="footer text-center">
		<div class="container">
			<p class="text-muted">Création : Florent Chehab, lien gitlab :
				<a href="https://gitlab.utc.fr/simde/Form-UT" target="_blank"><i class="fa fa-gitlab" aria-hidden="true"></i> </a> </p>
		</div>
	</footer>

	<script type="text/javascript" src="<?php echo base_url("node_modules/jquery/dist/jquery.min.js"); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url("node_modules/bootstrap/dist/js/bootstrap.min.js"); ?>"></script>
</body>

</html>
