<div class="alert alert-danger" role="alert">
	<h1>Bug.</h1>
	<p> Le site ne supporte pas cette fonctionnalité... </p>
	<br>
	<p> Il se peut qu'en vous déconnectant puis en vous reconnectant, l'erreur soit corrigée. </p>
	<br>
	<p> Si vous pensez que c'est une erreur, merci de nous <a href="<?php echo base_url("Welcome/contact");?>">contacter</a> !  </p>
</div>
