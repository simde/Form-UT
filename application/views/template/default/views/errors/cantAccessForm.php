<div class="alert alert-danger" role="alert">
	<h1>Tentative d'accès infructueuse à un formulaire.</h1>
	<p> Vous avez peut-être tenté d'accéder à un formulaire clos ou un formulaire auquel vous n'avez pas le droit d'accéder.</p>
	<br>
	<p> Veuillez contacter la personne ayant mis en ligne le formulaire.</p>
</div>
