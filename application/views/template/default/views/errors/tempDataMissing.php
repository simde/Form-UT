<div class="alert alert-danger" role="alert">
	<h1>Bug.</h1>
	<p>Il semblie y avoir une erreure provenant soit :</p>
	<ul>
	  <li> <p> D'une donnée stockée temporairement qui a disparue... <i class="fa fa-frown-o"></i> </p> </li>
	  <li> <p> D'une tentative de connection depuis un compte CAS non reconnu.</p> </li>
	</ul>
	<br>
	<br>

	<p><b>Merci de vous déconnecter du site en cliquanr sur le bouton ci-dessous (cela vous déconnectra également du CAS), puis de revenir nous voir <i class="fa fa-smile-o"></i></b> </p>
	<br>
	<br>
	<form action="/users/logout">
		<button type="submit" class="btn btn-danger btn-lg btn-block">Me déconnecter</button>
	</form>
	<br>
	<p> Si l'erreur persiste, merci de nous <a href="<?php echo base_url("Welcome/contact");?>">contacter.</a> </p>
</div>
