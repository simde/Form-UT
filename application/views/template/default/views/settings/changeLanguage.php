<div>
	<h2>
		Sélection de la langue par défaut.
	</h2>

	</br>
	<p>Ce choix déterminera :</p>
	<ul>
		<li>La langue de l’interface,</li>
		<li>La langue par défaut lors de la création d’un formulaire et de la réponse à un formulaire.</li>
	</ul>
	<p>Vous pourrez à tout moment change ce choix en revenant ici-même.</p>
</div>

<div id="mainForm"> </div>

<div class="alert alert-info">
	<strong>Info!</strong> Ce formulaire est un example de formulaire que le site est capable de générer.
</div>

<div id="mainNavigationDiv">
	<div id="mainPreviousStepButton"></div>
	<div id="mainSaveButton"></div>
	<div id="mainNextSetpButton"></div>
</div>
