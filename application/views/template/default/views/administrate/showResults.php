<div>
	<H2>
		Voici les résultats pour le formulaire n°<?php echo $formId;?> :
	</h2>
	<h3 class="formTitle">
			<?php echo htmlentities($title);?>
</h3>
<div>
		<?php echo $result;?>
	</div>
<br>
<p style="font-weight: bold;">
	Vous pouvez exporter ces résultats en cliquant sur l'icône : <i class="glyphicon glyphicon-export icon-share"></i>.
</p>
<p style="font-style: italic;">
<ul>
	<li>
		Cette fonctionnalité n'est pas garantie sur les appareils iOS,
	</li>
	<li>
		Vous risquez de perdre les sauts de ligne dans les zones de texte,
</li>
	<li>
Pour les éléments de type "sélection" et "cases à cocher" les différents choix sont encadrés de '|'.
	</li>
</ul>
</p>
</div>
