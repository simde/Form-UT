<div>
	<h2>
		Administration du formulaire n°<?php echo $formId; ?>.
	</h2>
	<h3>
		<?php echo htmlentities($title); ?>
	</h3>
<div class="text-center">
	<form action="/Administrate/showResults/<?php echo $formId; ?>">
		<button class="btn btn-primary" type="submit"/>
Voir les résultats associés au formulaire <i class="fa fa-table" aria-hidden="true"></i>
	</button>
</form>
<br>
<p>Lien pour répondre au formulaire (valide uniquement si le formulaire n'est pas clos) : <a href="<?php echo $shareUrl; ?>"> <?php echo $shareUrl; ?></a></p>
<hr>
</div>

</div>

<div id="mainForm"> </div>



<div id="mainNavigationDiv">
	<div id="mainPreviousStepButton"></div>
	<div id="mainSaveButton"></div>
	<div id="mainNextSetpButton"></div>
</div>
