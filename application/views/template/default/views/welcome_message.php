
		 <h1>Bonjour !</h1>
		 <p>Form-UT est un système permettant de créer simplement des formulaires à l'attention de la communauté UTCéenne.</p>
		 <p>Vous avez la possibilité de restreindre l'accès de ces formulaires à certains groupes d'utilisateurs⋅rices : </p>
			 <ul>
				 <li>
					 Soit en fonction des groupes "administratifs" déduits du CAS,
				 </li>
			 	<li>
					Soit en fonction des groupes que vous aurez vous-même <a href="<?php echo base_url('/Groups/initGroup');?>">créés</a>.
				</li>
			 </ul>
		 <br>
		 <h3>Profitez-bien de la plateforme !</h3>
