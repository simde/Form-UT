<?php

/**
 * This is an adaptation of the standard CI_Model class of codeigniter.
 *  BLABKA
 */
class My_Model extends CI_Model
{

		/**
		 * Variable for storing the results of a request
		 * @var array of std...
		 */
    public $result; // we store the results of queries here

		/**
		 * Store the user error code
		 * @var [type]
		 */
    public $userErrorCode;


    /**
     * Constructor of the class.
     */
    public function __construct()
    {
        parent::__construct();

        $this->userErrorCode = $this->session->userErrorCode;
    }

		/**
		 * Function for logging the incorrects sql request
		 * @param  string $sqlString
		 */
    protected function errorLogging($sqlString)
    {
        $classe = get_class();
        $message = "
				<H3> Erreur dans la classe $classe.</H3>
				<br>
				<code> $sqlString </code>
				";

        $this->session->set_userdata("SQLerror", $message);
    }


		/**
		 * Function for making a simple sql query
		 * @param  string $queryString query to make
		 * @return bool | errorcode
		 */
    protected function makeQuery($queryString)
    {
        $this->load->database();
        $query = $this->db->query($queryString);

        if ($query === false) {
            // sql error handling
                    self::errorLogging($queryString);
            return false;
        } elseif ($query === true) {
            return true;
        } else {
            $this->result = $query->result();
            return "tout est bon !";
        }
    }


		/**
		 * Function for making an sql transaction
		 * @param  string $queries queries to make
		 * @return bool | errorcode
		 */
    protected function makeTransaction($queries)
    {
        $this->load->database();

        $this->db->trans_start();

        foreach ($queries as $query) {
            $this->db->query($query);
        }
        $this->db->trans_complete();


        if ($this->db->trans_status() === false) {
            self::errorLogging($query);
            return false;
        }

        return true;
    }
}
