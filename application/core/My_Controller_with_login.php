<?php

/**
 * This is an adaptation of the standard CI_Controller class of codeigniter.
 * It enables Verifying if the user is logged in or not.
 *
 * And it provides an ajax interface
 */
class  My_Controller_with_login  extends  CI_Controller  {
	/**
	 * Holds name oh the class
	 * @var string
	 */
	protected $className = 'You should have setted the class name, dev !';

		/**
		 * Constructor of the class.
		 */
    function __construct()
		{
        parent::__construct();

        // Verify logged in status.
        if ( ! $this->session->userdata('loggedIn') ){
					$pageRedirect = uri_string();
					// Save location in the site
					$this->session->set_userdata('pageRedirect',$pageRedirect);
					redirect('Users/login','location');
				} else {

					if (!is_int($this->session->userIDinDB)){
							redirect('Welcome/errorTempDataMissing','location');
							return;
					}

				}
    }



		/**
		 * Easy redirection system (or not for production)
		 * @param  string $location Where we want to be redirected
		 * @return void
		 */
		protected function errorRedirector($location)
		{
			redirect($location,'location');
			return;
		}


		/**
		 * Generates a random string
		 *
		 * @param integer $length length of the string ti generate

		 * @return string
		 */
		protected function generateRandomString($length = 10)
		{
				$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
				$charactersLength = strlen($characters);
				$randomString = '';
				for ($i = 0; $i < $length; $i++) {
						$randomString .= $characters[random_int(0, $charactersLength - 1)];
				}
				return $randomString;
		}


		/**
		 * Function for automaticly getting the correct ajax location
		 * @return string url
		 */
		protected function ajaxLocation()
		{
			return base_url($this->className."/ajaxInterface");
		}


		/**
		 * Method providing an interface for ajax.
		 * @return none errors or call an other function.
		 */
		public function ajaxInterface()
		{
			// First we get the data
			$token = $this->input->post('token');
			$redirect = $this->input->post('redirect');
			$data = $this->input->post('data');

			// second we check the ajaX token
			$tokenValid = self::checkAjaxToken($token);
			if ($tokenValid===false){
				return self::JSONerror("The connection with the server has expired, please reload the page.",false);
			}

			// third we check if the inputs are correct
			if (self::checkPostInputs(array($token,$redirect,$data))===false){
				return self::JSONerror("There has been an error during JSON interception, please try again.");
			}



			// Fourth we convert the data to an Array
			/*
			var_dump($data);
			$data = json_decode($data,true);
			if($data === NULL){
				return self::JSONerror("Error in JSON decoding...");
			}
			*/

			// Five we redirect to the corect class function
			return $this->ajaxRedirector($redirect,$data);
		}

		/**
		 * Method just to check if the dev has a little bit of memory
		 */
		protected function ajaxRedirector($redirect,$data)
		{
			return self::JSONerror("The developer forgot to overload the ajaxRedirector function in the controller !!!!!");
		}




		/**
		 * Metho to check if the POST inputs are not empty
		 * @param  array | string $inputs an array of inputs or a string corresponding to one input
		 * @return bool     true if correct, false if not
		 */
		private function checkPostInputs($inputs)
		{
			if (!is_array($inputs))
			{
				if (!$inputs) return false;
			} else {
				foreach ($inputs as $input) {
					if (!$input) return false;
				}
			}
			return true;
		}


		/**
		 * Create, store and return a token for enabling post data transfers
		 *
		 * @return string        The new token
		 */
		protected function generateAjaxToken()
		{
			if ( ! defined('BASEPATH')) exit('No direct script access allowed');

			if ($this->session->userdata('ajaxTokens')==NULL){
				$this->session->set_userdata('ajaxTokens',array());
			}

			// We retreive the previous tokens
			$mainArray = $this->session->ajaxTokens;
			$this->session->unset_userdata('ajaxTokens');

			$newAjaxTokens = self::generateRandomString(10);
			while (in_array($newAjaxTokens,$mainArray)){
				$newAjaxTokens = self::generateRandomString(10);
			}

			$mainArray[$newAjaxTokens] = $newAjaxTokens;
			$this->session->set_userdata('ajaxTokens',$mainArray);

			return $newAjaxTokens;
		}

		/**
		 * check if an aJax token is valid or not
		 * Depending on the value of remove, we might remove it from the session
		 *
		 * @param  string $token the token to check
		 * @param  bool   $remove	Do we remove it or not if we have found the token
		 * @return bool   Has the token been found ?
		 */
		protected function checkAjaxToken($token,$remove = true)
		{
			if ( ! defined('BASEPATH')) exit('No direct script access allowed');

			$mainArray = $this->session->ajaxTokens;
			$this->session->unset_userdata('ajaxTokens');

			if (!is_array($mainArray)) return false;

			if(array_key_exists($token,$mainArray)){

				if ($remove === true){
					unset($mainArray[$token]);
					$this->session->set_userdata('ajaxTokens',$mainArray);
				}
				return true;
			} else {
				return false;
			}
		}


		/**
		 * Method to return an error via ajax
		 * @param string  $message    the Message going with error
		 * @param boolean $regenToken Should a knew ajax token be regenerated ?
		 */
		protected function JSONerror($message = "the dev forgot to mention what the error is...",$regenToken = true)
		{
			if ($regenToken === true){
				$newAjaxToken = self::generateAjaxToken();
				$result = array('success' => 'ERROR','message'=> $message,'newAjaxToken' => $newAjaxToken);

			} else {
				$result = array('success' => 'ERROR','message'=> $message);
			}

			echo json_encode($result);
			return;
		}

		/**
		 * Method to return an error via ajax
		 * @param array   $data  the data to return
		 * @param boolean $regenToken Should a knew ajax token be regenerated ?
		 */
		protected function JSONsuccess($data = array("empty" => "oui"),$regenToken = true)
		{
			if ($regenToken === true){
				$newAjaxToken = self::generateAjaxToken();
				$result = array('success' => 'SUCCESS','data'=> $data,'newAjaxToken' => $newAjaxToken);
			} else {
				$result = array('success' => 'SUCCESS','data'=> $data);
			}
			echo json_encode($result);
			return;
		}


		/**
		 * Function for validating the results send trough ajax
		 * @param  array $data the results
		 * @return bool | string
		 */
		protected function validateReturnedFormData($data){
			$dataFromForm = $data["dataFromForm"];
			$results = $dataFromForm["results"];
			$tokenForFormValidation = $data['tokenForFormValidation'];

			$originalForm = $this->session->userdata($tokenForFormValidation);

			if ( $originalForm === NULL){
				return "Token for form validation couldn't be authentificated, please reload the page";
			}

			$this->load->library('Validator');
			return $this->validator->validate($results,$originalForm);
		}



		/**
		 * Function for cleaning groups results
		 * @param string the string resutls
		 * @return array
		 */
		protected function cleanGroupResults($string){
			$this->load->library('Validator');
			return $this->validator->separateResultVariant($string);
		}




}
